// DirectMain.h: interface for the CDirectMain class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRECTMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
#define AFX_DIRECTMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "DirectSettings.h"
#include "DirectData.h"

// global message function so that dialogs etc (external objects) can write to the log etc.
//void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations);

// global main thread - this is the business right here!
void DirectMainThread(void* pvArgs);

// cortex http thread - we pass in the main cortex object, do al the cgi programming and security checking.
// void DirectHTTPThread(void* pvArgs);


class CDirectMain  
{
public:
	CDirectMain();
	virtual ~CDirectMain();

	CMessager					m_msgr;				// main messager // the one and only messager object
	CDirectSettings		m_settings;		// main mem storage for settings.  settings file objects are local to the DirectMainThread.
	CDirectData				m_data;				// internal variables
	CNetUtil					m_net;				// have a separate one for the status and command services.
	CDirUtil					m_dir;				// watch folder utilities.
	CDBUtil						m_db;					// SQL database
	COmniComm					m_omni;				// Omnibus automation
	CIS2Comm					m_ox;					// Oxtel graphics boxes

	// core net functions
	int		SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd=DIRECT_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);  // cortex initiates a request to an object server
	int		SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd=DIRECT_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex replies to an object server after receiving data. (usually ack or nak)
	int		SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd=DIRECT_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex answers a request from an object client

	// metadata functions	
	int DoAutoDeletion();

	// searches watch folder.
	int FindFile(char* pszFilename, char* pszBasePath, char** ppszFoundPath );

	// errors and messages
	int SendError(char* pszSender, char* pszError, ...);
	int SendMsg(char* pszSender, char* pszError, ...);

	// device specific functions
	int Miranda_DealWithFile(char* pszFilename, char* pszDirAlias, int nDest, char** ppszFoundPath=NULL);
	int Miranda_CheckFileExists(char* pszFilename, char* pszDirAlias, int nDest);
	int Miranda_PushFile(char* pszFullSourcePath, char* pszFilename, char* pszDirAlias, int nDest);
};

#endif // !defined(AFX_DIRECTMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
