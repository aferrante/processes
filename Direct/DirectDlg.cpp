// DirectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Direct.h"
#include "DirectDlg.h"
#include "DirectMain.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CDirectApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
// CAbout requires MFC.

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CAboutDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
  OnLogo(nFlags, point);

	CDialog::OnLButtonUp(nFlags, point);
}

BOOL CAboutDlg::OnInitDialog() 
{
  CDialog::OnInitDialog();

  
  CFont* pFont;
	CFont* pSetFont;
	CFont newFont;
  LOGFONT lfLogFont;
//  pFont = GetDlgItem(IDC_STATIC_URL)->GetFont();
  pFont = GetFont();
  COLORREF color;
  CDC* pdc;

  // set font to underline
	pSetFont =pFont;

  if(pFont->GetLogFont(&lfLogFont))
	{
		lfLogFont.lfUnderline = (BYTE) TRUE;
		if(newFont.CreateFontIndirect(&lfLogFont))
			pSetFont = &newFont;
	}

  // set color to blue...
  pdc = GetDlgItem(IDC_STATIC_URL)->GetDC( );
  color=RGB(0,0,255);
	if(pdc!=NULL)
	{
		CBmpUtil bmpu;
		HDC hdc= pdc->GetSafeHdc();
		((CStatic*)GetDlgItem(IDC_STATIC_URL))->SetBitmap(
			bmpu.TextOutBitmap(
				hdc, 
				"http://www.VideoDesignSoftware.com", 
				pSetFont, 
				color, 
				GetSysColor(COLOR_3DFACE), 
				0
				)
			);
		//center window
		CRect rcCtrl, rcWnd;
		GetClientRect(&rcWnd);
		GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcCtrl);
		ScreenToClient(&rcCtrl);
		GetDlgItem(IDC_STATIC_URL)->SetWindowPos(NULL, (rcWnd.Width()-rcCtrl.Width())/2, rcCtrl.top ,0,0,SWP_NOSIZE|SWP_NOZORDER);

	}
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATICTEXT_URL)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_URL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_URL)->EnableWindow(FALSE);
	}

  CString szTemp;
  szTemp.Format("Build date: %s %s", __DATE__, __TIME__);
  GetDlgItem(IDC_STATIC_BUILD)->SetWindowText(szTemp);

#ifdef CX_CURRENT_VERSION
  szTemp.Format("Direct Application\nVersion %s", CX_CURRENT_VERSION);
  GetDlgItem(IDC_STATICTEXT_TITLE)->SetWindowText(szTemp);
#endif


  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CAboutDlg::OnLogo(UINT nFlags, CPoint point)
{
	CRect rcLogo;
  GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
    // launch browser
    HINSTANCE hi;
    hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignInteractive.com", NULL, NULL, SW_HIDE);
  }
  GetDlgItem(IDC_STATIC_LOGO)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
    // launch browser
    HINSTANCE hi;
    hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignInteractive.com", NULL, NULL, SW_HIDE);
  }

}




/////////////////////////////////////////////////////////////////////////////
// CDirectDlg dialog


CDirectDlg::CDirectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDirectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDirectDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_nDxEye = 1;
	m_hbmp[BMP_PROG] = NULL;
}


void CDirectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDirectDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDirectDlg, CDialog)
	//{{AFX_MSG_MAP(CDirectDlg)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON_SETTINGS, OnButtonSettings)
	ON_WM_GETMINMAXINFO()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_SYSCOMMAND()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDirectDlg message handlers

void CDirectDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	ShowWindow(SW_HIDE);
	// CDialog::OnCancel();
}

void CDirectDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CDirectDlg::SetProgressColor(COLORREF cr)
{
	CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
	if(pwnd)
	{
//		CPaintDC dc(pwnd);
//		dc.SetBkColor( cr );
		CRect rcProg;
		pwnd->GetClientRect(&rcProg);
//		dc.FillSolidRect( rcProg, cr );
		
		if(m_hbmp[BMP_PROG]!=NULL) DeleteObject(m_hbmp[BMP_PROG]);
		m_hbmp[BMP_PROG] = m_bmpu.ColorBitmap(GetDC()->GetSafeHdc(), rcProg.Width(), rcProg.Height(), cr);
		((CStatic*)pwnd)->SetBitmap(m_hbmp[BMP_PROG]);
	}
}

void CDirectDlg::SetProgress(unsigned long ulFlags)
{
//#define CXDLG_CLEAR		0x00000000
//#define CXDLG_WAITING 0x10000000

	if(ulFlags&CXDLG_WAITING)
	{
		// start the timer to get the "waiting" look.
		if(ulFlags&0xffff)
			SetTimer(1, (ulFlags&0xffff), NULL);
		else
			SetTimer(1, 30, NULL);
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left, rcProg.top,
				1, rcProg.Height(),
				SWP_NOZORDER);
		}
	}
	else
	if(ulFlags&CXDLG_EVILEYE)  //like waiting, only no timer, needs actual calls from an outside thread to show that its working!
	{
		// kil the timer if it's on, to get the "evil eye" look.
		KillTimer(1);

		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			//width is expressed as a percentage.
			int nWidth;
			if(((ulFlags&0x7f)>0)&&((ulFlags&0x7f)<100))
				nWidth = (ulFlags&0x7f);
			else
				nWidth = 10;

			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);

			//recalc width to pixels;
			nWidth = (rcFull.Width()*nWidth/100);


			if(rcProg.Width() != nWidth)//starting.
			{
				if((rcProg.left+nWidth)>= rcFull.right)
				{
					rcProg.left = rcFull.right-nWidth-1;
				}
				if( rcProg.left <= rcFull.left)
				{
					rcProg.left = rcFull.left+1;
				}
				pwnd->SetWindowPos(
					&wndTop,
					rcProg.left, rcProg.top,
					nWidth, rcProg.Height(),
					SWP_NOZORDER);
				//speed is expressed as a pixel value.
				if((((ulFlags&0xff00)>>8)>0)&&((int)((ulFlags&0xff00)>>8)<nWidth))  // max one full step per call
					m_nDxEye = ((ulFlags&0xff00)>>8);
				else
					m_nDxEye = 1;
			}
			else
			{
				if((rcProg.right+m_nDxEye>rcFull.right)||(rcProg.left+m_nDxEye<rcFull.left))
				{
					m_nDxEye=-m_nDxEye;
				}
				pwnd->SetWindowPos(
					&wndTop,
					rcProg.left+m_nDxEye, rcProg.top,
					nWidth, rcProg.Height(),
					SWP_NOZORDER|SWP_NOSIZE);
			}
		}
	}
	else 
	if(ulFlags==CXDLG_CLEAR)
	{
		// kill the timer and clear
		KillTimer(1);
		GetDlgItem(IDC_STATIC_PROGBAR)->ShowWindow(SW_HIDE);
	}
	else
	if((ulFlags&0x7F)<=100)
	{// set a percentage
		// do the progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// right edge moves to the right
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left, rcProg.top,
				rcFull.Width()*(ulFlags&0x7F)/100, rcProg.Height(),
				SWP_NOZORDER);
		}
	}
	else
	if(((ulFlags&0xff)>100)&&((ulFlags&0xff)<200)) // range 1 to 99. described by 101 to 199.
	{// set a percentage
		// do the left side progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// left edge moves to the right
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left+(rcFull.Width()*((ulFlags&0xff)-100)/100), rcProg.top,
				rcFull.Width()-(rcFull.Width()*((ulFlags&0xff)-100)/100), rcProg.Height(),  //width done this way because of int truncate
				SWP_NOZORDER);
		}
	}
}

void CDirectDlg::ShellExecuteSettings()
{
	if((theApp.m_pszSettingsURL!=NULL)&&(strlen(theApp.m_pszSettingsURL)>0))
	{
		//shell execute a browser to the Direct settings URL, shich can be found in 
		//one of the ini files. it gets initialized once the webserver is up.
		// launch browser
		HINSTANCE hi;
		hi=ShellExecute((HWND) NULL, "open", theApp.m_pszSettingsURL, NULL, NULL, SW_SHOW);

	}
	else
	{
		AfxMessageBox("The settings interface has not yet been initialized.");
	}
}

void CDirectDlg::OnButtonSettings() 
{
	ShellExecuteSettings();
//	g_settings.DoModal();
}

BOOL CDirectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
//	g_settings.Settings(READ);

	m_lce.SubclassDlgItem(IDC_LIST1, this);
	m_lce.SetHeight(16);
	m_lce.SetImages(IDB_BITMAP_STDICONS, 16, RGB(254,254,254)); 

	CRect rc; 
	m_lce.GetWindowRect(&rc);
	m_lce.InsertColumn(0, "", LVCFMT_LEFT, rc.Width()-16, 0 );

//	int nRegisterCode=0;

	CString szParams=_T("");//, szReport=_T("");

	// main Dlg Title
	szParams.Format("Direct powered by VDS");
	SetWindowText(szParams);

/*
	// set up default MB title
//	szParams.Format("Direct");
	nRegisterCode = g_md.RegisterDestination(MD_DEST_UI_MB, MD_DESTTYPE_UI_MB,	szParams, (void*)(NULL));
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register MB Title\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up logging list control
	szParams.Format("50000|T-iM-F-S-D"); // if this changes, modify dest in OnButtonPause should match
	nRegisterCode = g_md.RegisterDestination(MD_DEST_UI_LIST1, MD_DESTTYPE_UI_LISTCTRLEX,	szParams, (void*)(&m_lce));
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register ListCtrlEx\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}
*/
	// set up logfile
//	szParams.Format("Direct|YM");
//	nRegisterCode = m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", char* pszParams, char* pszInfo);
//	if (nRegisterCode != MSG_SUCCESS) {szReport.Format("failed to register log file\n code: %d", nRegisterCode); AfxMessageBox(szReport);}

/*
	// set up e-mail group 1 -> make this general alert
	szParams.Format("%s|Direct|%s|Default subject",g_settings.m_szEmailTo,g_settings.m_szEmailIP);
	nRegisterCode = g_md.RegisterDestination(MD_DEST_EMAIL_GP1, MD_DESTTYPE_EMAIL, szParams);
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register e-mail1\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up e-mail group 2 -> make this just VDI
	szParams.Format("kenji@videodesigninteractive.com,gregg@videodesigninteractive.com|Direct|%s|Default Subj",g_settings.m_szEmailIP);
	nRegisterCode = g_md.RegisterDestination(MD_DEST_EMAIL_GP2, MD_DESTTYPE_EMAIL, szParams);
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register e-mail2\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}
	
	
	szParams.Format("-------------- Direct start --------------");
	CString szLine = _T("");
	for(int l=0; l<szParams.GetLength(); l++) szLine+="-"; // make it pretty!
	
	g_md.DM(0,MD_FMT_UI_ICONINFO,0,
		"%s\n%s|OnInitDialog",
		szLine,szParams
		); //(Dispatch message)
// 
*/

	// Load all bitmaps, store handles
	CBitmap bmp;

  bmp.LoadBitmap(IDB_SETTINGS);
	m_hbmp[BMP_SETTINGS] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
/*
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BITMAP_PAUSELIST);
	m_hbmp[BMP_PAUSE] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BITMAP_PLAYLIST);
	m_hbmp[BMP_PLAY] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
*/
	bmp.DeleteObject();

	// uncomment below to set up buttons
	((CButton*)GetDlgItem(IDC_BUTTON_SETTINGS))->SetBitmap(m_hbmp[BMP_SETTINGS]);
//	((CButton*)GetDlgItem(IDC_BUTTON_PAUSE))->SetBitmap(m_hbmp[BMP_PAUSE]);

	GetDlgItem(IDC_STATIC_PROGBAR)->ShowWindow(SW_HIDE);

	// uncomment below for tooltips
	EnableToolTips(TRUE);
	

	//change the progress bar style...
//	GetDlgItem(IDC_STATIC_PROGBAR)->ModifyStyleEx( SS_GRAYRECT, SS_OWNERDRAW );  //interesting look, because cant modify cstatic, apparently.
	SetProgressColor(GetSysColor(COLOR_ACTIVECAPTION));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDirectDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}
void CDirectDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDirectDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDirectDlg::OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI ) 
{
	lpMMI->ptMinTrackSize.x=DIRECTDLG_MINSIZEX;
	lpMMI->ptMinTrackSize.y=DIRECTDLG_MINSIZEY;
}

void CDirectDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	int nCtrlID;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_STATIC_STATUSTEXT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_STSTATUS].left,  // goes with right edge
			m_rcCtrl[ID_STSTATUS].top-dy,  // goes with bottom edge
			m_rcCtrl[ID_STSTATUS].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_STSTATUS].Height(), 
			SWP_NOZORDER
			);
/*
		pWnd=GetDlgItem(IDC_STATIC_PROG);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_STPROG].left,  // goes with right edge
			m_rcCtrl[ID_STPROG].top-dy,  // goes with bottom edge
			m_rcCtrl[ID_STPROG].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_STPROG].Height(), 
			SWP_NOZORDER
			);
*/

		pWnd=GetDlgItem(IDC_STATIC_PROGBAR);
		if(pWnd!=NULL)
		{
			CRect rcProg;
			pWnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			pWnd->SetWindowPos( 
			&wndTop,
			rcProg.left,  
			m_rcCtrl[ID_STSTATUS].top-dy+(m_rcCtrl[ID_STPROG].top-m_rcCtrl[ID_STSTATUS].top),  // goes with bottom edge
			rcProg.Width(), // leave the width alone, it's a progress bar!
			rcProg.Height(), 
			SWP_NOZORDER
			);
		}

		pWnd=GetDlgItem(IDC_BUTTON_SETTINGS);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[ID_BNSETTINGS].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[ID_BNSETTINGS].top-dy,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);
/*
		pWnd=GetDlgItem(IDC_BUTTON_PAUSE);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNPAUSE].left-(dx/2),  // centered
			m_rcCtrl[ID_BNPAUSE].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNPAUSE].top,  // goes with top edge
			m_rcCtrl[ID_BNPAUSE].top-dy,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);
*/
		pWnd=GetDlgItem(IDC_LIST1);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			m_rcCtrl[ID_LIST].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_LIST].Height()-dy,  // goes with bottom edge
			SWP_NOZORDER|SWP_NOMOVE
			);

		if(m_lce.GetColumnWidth(0)<(m_rcCtrl[ID_LIST].Width()-dx-16)) // only make this get larger
		{
			m_lce.SetColumnWidth(0, (m_rcCtrl[ID_LIST].Width()-dx-16) ); 
		}



		for (int i=0;i<DIRECTDLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			case ID_BNSETTINGS: nCtrlID=IDC_BUTTON_SETTINGS;break;
//			case ID_BNPAUSE: nCtrlID=IDC_BUTTON_PAUSE;break;
			case ID_STSTATUS: nCtrlID=IDC_STATIC_STATUSTEXT;break;
			case ID_STPROG: nCtrlID=IDC_STATIC_PROGBAR;break;
			}
			GetDlgItem(nCtrlID)->Invalidate();
		}
	}		
}

void CDirectDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);

//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<DIRECTDLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			case ID_BNSETTINGS: nCtrlID=IDC_BUTTON_SETTINGS;break;
//			case ID_BNPAUSE: nCtrlID=IDC_BUTTON_PAUSE;break;
			case ID_STSTATUS: nCtrlID=IDC_STATIC_STATUSTEXT;break;
			case ID_STPROG: nCtrlID=IDC_STATIC_PROGBAR;break;
			}
			GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
			ScreenToClient(&m_rcCtrl[i]);
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
}

BOOL CDirectDlg::Create() 
{
	return CDialog::Create(IDD);
}

BOOL CDirectDlg::OnToolTipNotify( UINT id, NMHDR* pNMHDR, LRESULT* pResult )
{
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID =pNMHDR->idFrom;
	if (pTTT->uFlags & TTF_IDISHWND)
	{
		// idFrom is actually the HWND of the tool
		nID = ::GetDlgCtrlID((HWND)nID);
		if(nID)
		{
			switch(nID)
			{
			case IDC_BUTTON_SETTINGS:
				{
					sprintf(pTTT->szText, "Display or edit settings.");
				} break;
			}
			return TRUE;
		}
	}
	return FALSE;
}

void CDirectDlg::OnExit()
{

	CDialog::OnCancel();
}



void CDirectDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 1)
	{
		// do the progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// right edge moving to the right
			if(rcProg.left<=rcFull.left)
			{
				if(rcProg.right>=rcFull.right) // too far, so start with the left edge now.
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.left+1, rcProg.top,
						rcFull.Width()-1, rcProg.Height(),
						SWP_NOZORDER);
				}
				else  //ok just make right edge move to the right
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.left, rcProg.top,
						min(rcProg.Width()+1, rcFull.Width()), rcProg.Height(),
						SWP_NOZORDER|SWP_NOMOVE);
				}
			}
			else  // left edge moving to the right, so check
			{
				if(rcProg.left>=rcFull.right)  // made it, so lets start over.
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.left, rcProg.top,
						1, rcProg.Height(),
						SWP_NOZORDER);
				}
				else  //ok just make left edge move to the right
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.right-min(rcProg.Width()-1,rcFull.Width()-1), rcProg.top,
						min(rcProg.Width()-1,rcFull.Width()-1), rcProg.Height(),
						SWP_NOZORDER);
				}
			}

		}

	}
	else
	CDialog::OnTimer(nIDEvent);
}

void CDirectDlg::SetStatus(void* pObj)
{
	if(pObj==NULL) return;
	CDirectMain* pdirect = (CDirectMain*)pObj;
	// fill the status display with the current status.
}
