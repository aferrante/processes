// DirectSettings.cpp: implementation of the CDirectSettings.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Direct.h"
#include "DirectSettings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectSettings::CDirectSettings()
{
	m_pszName = NULL;
	m_ulMainMode = DIRECT_MODE_DEFAULT;

	// ports
//	m_usFilePort		= DIRECT_PORT_FILE;
	m_usCommandPort	= DIRECT_PORT_CMD;
	m_usStatusPort	= DIRECT_PORT_STATUS;

	// port ranges
//	m_usResourcePortMin		= DIRECT_PORT_RESMIN;
//	m_usResourcePortMax		= DIRECT_PORT_RESMAX;

//	m_usProcessPortMin		= DIRECT_PORT_PRCMIN;
//	m_usProcessPortMax		= DIRECT_PORT_PRCMAX;

	// messaging for Direct
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host

	// messaging for remote objects
//	m_bUseLogRemote = false;	// write a separate log file for each object

	//admin
//	m_bUseAuthentication = true;	// user/password verification, otherwise totally open to all clients
//	m_pszUserName = NULL;  // cortex master username
//	m_pszPassword = NULL;		// cortex master password
//	m_pszSecurityFile1 = NULL;
//	m_pszSecurityFile2 = NULL;

	// backup
	m_bUseClone = false;			// spark a clone (unless a clone itself)
	m_bInitClone = false;    // Be a clone when starting up.
	m_pszCloneIP = NULL;  // IP of clone
	m_usCloneCommandPort = DIRECT_PORT_CMD;  //port that clone is listnening on


	// Database settings
	m_bUseBackupDB = true;					// make calls to both pri and backup DBs
	m_pszPrimaryDSN=NULL;						// DSN
	m_pszPrimaryDBUser=NULL;				// user
	m_pszPrimaryDBPassword=NULL;		// pw
	m_pszBackupDSN=NULL;						// DSN
	m_pszBackupDBUser=NULL;					// user
	m_pszBackupDBPassword=NULL;			// pw
	m_pszTableDest=NULL;				// destination table name - info about graphics boxes and mapping
	m_pszTableMeta=NULL;				// metadata table name - info about graphics
	m_pszTableExchange=NULL;		// exchange flag table - application info
	m_pszTableChannels=NULL;		// channels table name - info about channel options
	m_pszTableEvents = NULL;    	// events table name - info about event status
	m_pszTableMessages = NULL;    	// errors table name


	// Automation settings
	m_usNumColossus=0;   // number of Colossus Adaptors
	m_ppszServerAddress=NULL;				// array of Colossus Adaptor IP (port is 10540)
	m_ppszServerName=NULL;					// array of Friendly name of connection (optional).
	m_ppszDebugFile=NULL;						//  array of debug filenames
	m_ppszCommFile=NULL;						//  array of comm log filenames

	// watchfolder settings.
	m_pszWatchFolderPath=NULL;									// the path of the watchfolder
	m_pszPrimaryDestinationFolderPath=NULL;		// the path of the primary destination folder
	m_pszBackupDestinationFolderPath=NULL;			// the path of the backup destination folder

	//system folder
	m_pszSystemFolderPath=NULL;			// the path of the folder used for parse files etc.

	//imagestore specific 
	m_pszNullFile=NULL;			// the filename of the nullfile - must be in the system folder
	m_bUseNullFile = true;

	// App settings (process specific)
	m_ulProcessIntervalMS = 60000;		// interval on which to run the process, in milliseconds
	m_ulClonePingIntervalMS = 15000;  // interval on which the clone pings, or which to expect the clone to ping, in milliseconds
	m_ulWatchIntervalMS = 60000;			// interval on which to check the watch folder, in milliseconds
	m_ucWarnPercentageDiskFull = 75;   // send a warning when this % of storage is exceeded
	m_ucDeletePercentageDiskFull = 95;   // start deleting files if this % is exceeded

}

CDirectSettings::~CDirectSettings()
{
// AfxMessageBox("wait settings");
	if(m_pszName) free(m_pszName); // must use malloc to allocate
//	if(m_pszUserName) free(m_pszUserName); // must use malloc to allocate
//	if(m_pszPassword) free(m_pszPassword); // must use malloc to allocate
//	if(m_pszSecurityFile1) free(m_pszSecurityFile1); // must use malloc to allocate
//	if(m_pszSecurityFile2) free(m_pszSecurityFile2); // must use malloc to allocate
	if(m_pszCloneIP) free(m_pszCloneIP); // must use malloc to allocate

	// Database settings
	if(m_pszPrimaryDSN) free(m_pszPrimaryDSN); 		// DSN
	if(m_pszPrimaryDBUser) free(m_pszPrimaryDBUser);			// user
	if(m_pszPrimaryDBPassword) free(m_pszPrimaryDBPassword);		// pw
	if(m_pszBackupDSN) free(m_pszBackupDSN);					// DSN
	if(m_pszBackupDBUser) free(m_pszBackupDBUser);			// user
	if(m_pszBackupDBPassword) free(m_pszBackupDBPassword);		// pw
	if(m_pszTableDest) free(m_pszTableDest);				// destination table name - info about graphics boxes and mapping
	if(m_pszTableMeta) free(m_pszTableMeta);			// metadata table name - info about graphics
	if(m_pszTableExchange) free(m_pszTableExchange);		// exchange flag table - application info
	if(m_pszTableChannels) free(m_pszTableChannels);		// channels table name - info about channel options
	if(m_pszTableEvents) free(m_pszTableEvents);				// events table name - info about event status
	if(m_pszTableMessages) free(m_pszTableMessages);				// errors/messages table name

	// Automation settings
	m_usNumColossus=0;   // number of Colossus Adaptors
	unsigned short i=0;
	while (i<m_usNumColossus)
	{
		if((m_ppszServerAddress)&&(m_ppszServerAddress[i])) free(m_ppszServerAddress[i]);				// array of Colossus Adaptor IP (port is 10540)
		if((m_ppszServerName)&&(m_ppszServerName[i])) free(m_ppszServerName[i]);					// array of Friendly name of connection (optional).
		if((m_ppszDebugFile)&&(m_ppszDebugFile[i])) free(m_ppszDebugFile[i]);						//  array of debug filenames
		if((m_ppszCommFile)&&(m_ppszCommFile[i])) free(m_ppszCommFile[i]);						//  array of comm log filenames

		i++;
	}

	if(m_ppszServerAddress) delete [] m_ppszServerAddress;
	if(m_ppszServerName) delete [] m_ppszServerName;
	if(m_ppszDebugFile) delete [] m_ppszDebugFile;
	if(m_ppszCommFile) delete [] m_ppszCommFile;

	// watchfolder settings.
	if(m_pszWatchFolderPath) free(m_pszWatchFolderPath);									// the path of the watchfolder
	if(m_pszPrimaryDestinationFolderPath) free(m_pszPrimaryDestinationFolderPath);		// the path of the primary destination folder
	if(m_pszBackupDestinationFolderPath) free(m_pszBackupDestinationFolderPath);			// the path of the backup destination folder

	//imagestore specific 
	if(m_pszNullFile) free(m_pszNullFile);			// the filename of the nullfile - must be in the system folder

	//system folder
	if(m_pszSystemFolderPath) free(m_pszSystemFolderPath);			// the path of the folder used for parse files etc.
}
