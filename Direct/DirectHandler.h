#if !defined(AFX_DIRECTHANDLER_H__7C5F5C8B_0838_4B9A_8AE3_48681BAC19CF__INCLUDED_)
#define AFX_DIRECTHANDLER_H__7C5F5C8B_0838_4B9A_8AE3_48681BAC19CF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DirectHandler.h : header file
//

//#define ICON_BARS 0
//#define ICON_BLK  1
//#define ICON_BLU  2
#define ICON_LOGO  0
#define ICON_GRN  1
#define ICON_RED  2
#define ICON_YEL  3
//#define ICON_CLR  6 

#define MAX_ICONS 4
/////////////////////////////////////////////////////////////////////////////
// CDirectHandler window

class CDirectHandler : public CWnd
{
// Construction
public:
	CDirectHandler();

// Attributes
public:
	int m_nLogoCounter;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDirectHandler)
	public:
	virtual BOOL Create();
	protected:
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDirectHandler();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDirectHandler)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnCmdAbout();
	afx_msg void OnCmdExit();
	afx_msg void OnCmdSettings();
	afx_msg void OnCmdShowwnd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	LONG OnTrayNotify(UINT wParam, LONG lParam);
	BOOL NotifyIcon(DWORD dwMessage, HICON hIcon, LPCTSTR pszToolTip = NULL);
	BOOL NotifyIcon(DWORD dwMessage, HICON hIcon, UINT nStringResource);

	void OnRightClick();
	void OnRightDoubleClick();
	void OnLeftClick();
	void OnLeftDoubleClick();

	BOOL m_bLeftFireDoubleClick;
	BOOL m_bRightFireDoubleClick;
	BOOL m_bLeft;
	

	// icons
	HICON m_hIcon[MAX_ICONS];  // defined above
	int m_nCurrentIcon;

public:
	CWnd* m_pMainDlg;
	
	void OnExternalCmdExit();

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIRECTHANDLER_H__7C5F5C8B_0838_4B9A_8AE3_48681BAC19CF__INCLUDED_)
