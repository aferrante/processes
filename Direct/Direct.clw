; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDirectDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Direct.h"
LastPage=0

ClassCount=5
Class1=CAboutDlg
Class2=CDirectApp
Class3=CDirectHandler

ResourceCount=4
Resource1=IDD_CORTEX_DIALOG
Resource2=IDD_DIRECT_DIALOG
Class4=CDirectDlg
Class5=CDirectSettings
Resource3=IDR_MENU1
Resource4=IDD_ABOUTBOX

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=About.cpp
ImplementationFile=About.cpp
LastObject=CAboutDlg

[CLS:CDirectApp]
Type=0
BaseClass=CWinApp
HeaderFile=Direct.h
ImplementationFile=Direct.cpp
Filter=N
VirtualFilter=AC
LastObject=CDirectApp

[CLS:CDirectHandler]
Type=0
BaseClass=CWnd
HeaderFile=DirectHandler.h
ImplementationFile=DirectHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CDirectHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SETTINGS
Command3=ID_CMD_SHOWWND
Command4=ID_CMD_EXIT
Command5=ID_CMD_ABOUT
Command6=ID_CMD_SETTINGS
Command7=ID_CMD_SHOWWND
Command8=ID_CMD_EXIT
CommandCount=8

[CLS:CDirectDlg]
Type=0
HeaderFile=DirectDlg.h
ImplementationFile=DirectDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_STATIC_PROGBAR

[CLS:CDirectSettings]
Type=0
HeaderFile=DirectSettings.h
ImplementationFile=DirectSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDirectSettings

[DLG:IDD_CORTEX_DIALOG]
Type=1
Class=CDirectDlg
ControlCount=6
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1342242944
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294

[DLG:IDD_DIRECT_DIALOG]
Type=1
Class=?
ControlCount=6
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1342242944
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294

