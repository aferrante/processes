// DirectSettings.h: interface for the CDirectSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRECTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_DIRECTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//#include "DirectDefines.h"

class CDirectSettings  
{
public:
	CDirectSettings();
	virtual ~CDirectSettings();

	char* m_pszName;  // familiar name of this instance.
	unsigned long m_ulMainMode;

	// ports
//	unsigned short m_usFilePort;
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// port ranges
//	unsigned short m_usResourcePortMin;
//	unsigned short m_usResourcePortMax;

//	unsigned short m_usProcessPortMin;
//	unsigned short m_usProcessPortMax;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Direct
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host

	// messaging for remote objects
//	bool m_bUseLogRemote;	// write a separate log file for each object

	//admin
//	bool m_bUseAuthentication;	// user/password verification, otherwise totally open to all clients
//	char* m_pszUserName;  // cortex master username
//	char* m_pszPassword;	// cortex master password
//	char* m_pszSecurityFile1;
//	char* m_pszSecurityFile2;

	// backup
	bool m_bUseClone;			// spark a clone (unless a clone itself)
	bool m_bInitClone;    // Be a clone when starting up.
	char* m_pszCloneIP;  // IP of clone
	unsigned short m_usCloneCommandPort;  //port that clone is listening on

	// Database settings
	bool m_bUseBackupDB;						// make calls to both pri and backup DBs
	char* m_pszPrimaryDSN;					// DSN
	char* m_pszPrimaryDBUser;				// user
	char* m_pszPrimaryDBPassword;		// pw
	char* m_pszBackupDSN;						// DSN
	char* m_pszBackupDBUser;				// user
	char* m_pszBackupDBPassword;		// pw
	char* m_pszTableDest;   // destination table name - info about graphics boxes and mapping
	char* m_pszTableMeta;   // metadata table name - info about graphics
	char* m_pszTableExchange;		// exchange flag table - application info
	char* m_pszTableChannels;		// channels table name - info about channel options
	char* m_pszTableEvents;		// events table name, metadata
	char* m_pszTableMessages;		// errors/messages table name - errors!

	// Automation settings
	unsigned short m_usNumColossus;   // number of Colossus Adaptors
	char** m_ppszServerAddress;				// array of Colossus Adaptor IP (port is 10540)
	char** m_ppszServerName;					// array of Friendly name of connection (optional).
	char** m_ppszDebugFile;						//  array of debug filenames
	char** m_ppszCommFile;						//  array of comm log filenames

	// watchfolder settings.
	char* m_pszWatchFolderPath;									// the path of the watchfolder
	char* m_pszPrimaryDestinationFolderPath;		// the path of the primary destination folder
	char* m_pszBackupDestinationFolderPath;			// the path of the backup destination folder

	//system folder
	char* m_pszSystemFolderPath;			// the path of the folder used for parse files etc.

	//imagestore specific 
	char* m_pszNullFile;			// the filename of the nullfile - must be in the system folder
	bool	m_bUseNullFile;			// use the nullfile

	// App settings (process specific)
	unsigned long m_ulProcessIntervalMS;		// interval on which to run the process, in milliseconds
	unsigned long m_ulClonePingIntervalMS;  // interval on which the clone pings, or which to expect the clone to ping, in milliseconds
	unsigned long m_ulWatchIntervalMS;			// interval on which to check the watch folder, in milliseconds
	unsigned char m_ucWarnPercentageDiskFull;   // send a warning when this % of storage is exceeded
	unsigned char m_ucDeletePercentageDiskFull;   // start deleting files if this % is exceeded

};

#endif // !defined(AFX_DIRECTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
