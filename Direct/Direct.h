// Direct.h : main header file for the DIRECT application
//

#if !defined(AFX_DIRECT_H__82614209_3A12_4A52_8D88_49B5FD42C967__INCLUDED_)
#define AFX_DIRECT_H__82614209_3A12_4A52_8D88_49B5FD42C967__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

// These are damned useful:
#ifndef max
#define max(a,b)            ((a > b) ? a : b)
#endif

#ifndef min
#define min(a,b)            ((a < b) ? a : b)
#endif                    

// cortex window flags
#define CXWF_DEFAULT			0x00000000
#define CXWF_QUIET_MODE		0x00000001

#include "resource.h"		// main symbols

// various includes 
#include "../../Common/IMG/BMP/CBmpUtil_MFC.h" 
//#include "../../Common/MFC/ListCtrlEx/ListCtrlEx.h"
// core components
#include "../../Common/MSG/msg.h"
#include "../../Common/MSG/Messager.h"
#include "../../Common/TXT/FileUtil.h"
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/LAN/NetUtil.h"
//#include "../../Common/HTTP/HTTP10.h"  // includes CMessagingObject
#include "../../Common/FILE/DirUtil.h"

// "device" components
#include "../../Common/MFC/ODBC/DBUtil.h"  // DB

// API components
#include "../../Common/API/Omnibus/OmniComm.h"  // automation
#include "../../Common/API/Miranda/IS2Comm.h"   // imagestore and intuition file xfer

#include "DirectHandler.h"  // just included to have access to windowing environment
#include "DirectDefines.h"  // all the other defines

#include <process.h>
#include <direct.h>

/////////////////////////////////////////////////////////////////////////////
// CDirectApp:
// See Direct.cpp for the implementation of this class
//

class CDirectApp : public CWinApp
{
public:
	CDirectApp();

//	unsigned long m_ulFlags;
//	char* m_pszSettingsURL;

	CRITICAL_SECTION m_crit;  // critical section to manage dir change array

	int m_nAppState;
	char m_pszAppStateText[81];
//	CDirectMain m_direct;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDirectApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	void SetAppStateText(char* pszText, ...);
// Implementation

	//{{AFX_MSG(CDirectApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
//	BOOL m_bAutostart;

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIRECT_H__82614209_3A12_4A52_8D88_49B5FD42C967__INCLUDED_)
