// IngestClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IngestClient.h"
#include "IngestClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



extern CIngestClientApp			theApp;
extern CIngestClientCore			g_core;
extern CIngestClientData			g_data;
extern CIngestClientDlg*			g_pdlg;
extern CIngestClientSettings	g_settings;
extern CMessager*					g_pmsgr;

/////////////////////////////////////////////////////////////////////////////
// CIngestClientDlg dialog


CIngestClientDlg::CIngestClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIngestClientDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIngestClientDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
}


void CIngestClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIngestClientDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIngestClientDlg, CDialog)
	//{{AFX_MSG_MAP(CIngestClientDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, OnButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	ON_NOTIFY(HDN_ITEMCLICK, IDC_LIST1, OnItemclickList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIngestClientDlg message handlers

void CIngestClientDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CIngestClientDlg::OnOK() 
{
//CDialog::OnOK();
}

BOOL CIngestClientDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}

void CIngestClientDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID = 0;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			case ID_BNREF: nCtrlID=IDC_BUTTON_REFRESH;break;
			case ID_BNADD: nCtrlID=IDC_BUTTON_ADD;break;
			case ID_BNED: nCtrlID=IDC_BUTTON_EDIT;break;
			case ID_BNDEL: nCtrlID=IDC_BUTTON_DEL;break;
			default: nCtrlID=0;break;
			}
			if(nCtrlID)
			{
				GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
				ScreenToClient(&m_rcCtrl[i]);
			}
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
}

void CIngestClientDlg::OnSize(UINT nType, int cx, int cy) 
{
	int nCtrlID = 0;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_LIST1);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0,0,
			m_rcCtrl[ID_LIST].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_LIST].Height()-dy,  // goes with bottom edge, 
			SWP_NOZORDER|SWP_NOMOVE
			);
		pWnd=GetDlgItem(IDC_BUTTON_REFRESH);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BNREF].left-dx,  // goes with right edge
			m_rcCtrl[ID_BNREF].top,      // goes with top edge, 
			0,0,
			SWP_NOZORDER|SWP_NOSIZE
			);
		pWnd=GetDlgItem(IDC_BUTTON_ADD);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BNADD].left-dx,  // goes with right edge
			m_rcCtrl[ID_BNADD].top,      // goes with top edge, 
			0,0,
			SWP_NOZORDER|SWP_NOSIZE
			);
		pWnd=GetDlgItem(IDC_BUTTON_EDIT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BNED].left-dx,  // goes with right edge
			m_rcCtrl[ID_BNED].top,      // goes with top edge, 
			0,0,
			SWP_NOZORDER|SWP_NOSIZE
			);
		pWnd=GetDlgItem(IDC_BUTTON_DEL);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BNDEL].left-dx,  // goes with right edge
			m_rcCtrl[ID_BNDEL].top,      // goes with top edge, 
			0,0,
			SWP_NOZORDER|SWP_NOSIZE
			);

		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			case ID_BNREF: nCtrlID=IDC_BUTTON_REFRESH;break;
			case ID_BNADD: nCtrlID=IDC_BUTTON_ADD;break;
			case ID_BNED: nCtrlID=IDC_BUTTON_EDIT;break;
			case ID_BNDEL: nCtrlID=IDC_BUTTON_DEL;break;
			default: nCtrlID=0;break;
			}
			if(nCtrlID) GetDlgItem(nCtrlID)->Invalidate();
		}
	}		
}

BOOL CIngestClientDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	g_settings.Settings(true);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CIngestClientDlg::OnExit()
{
	// add cleanup here if necesary;

	// save settings;
	g_settings.Settings(false);
	CDialog::OnCancel();
}

void CIngestClientDlg::OnButtonAdd() 
{
	// TODO: Add your control notification handler code here
	
}

void CIngestClientDlg::OnButtonDel() 
{
	// TODO: Add your control notification handler code here
	
}

void CIngestClientDlg::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here
	
}

void CIngestClientDlg::OnButtonRefresh() 
{
	// TODO: Add your control notification handler code here
	
}

void CIngestClientDlg::OnItemclickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;
	// TODO: Add your control notification handler code here
	// deal with the sort-bys...

	/*
HD_NOTIFY
    NMHDR         hdr; 
    int           iItem; 
    int           iButton; 
    HD_ITEM FAR*  pitem; 

HD_ITEM
    UINT     mask; 
    int      cxy; 
    LPTSTR   pszText; 
    HBITMAP  hbm; 
    int      cchTextMax; 
    int      fmt; 
    LPARAM   lParam; 
*/
	
	*pResult = 0;
}
