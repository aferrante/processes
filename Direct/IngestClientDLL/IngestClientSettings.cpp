// IngestClientSettings.cpp : implementation file
//

#include "stdafx.h"
#include "IngestClient.h"
#include "IngestClientSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SETTINGS_FILENAME "IngestClient.ini"

/////////////////////////////////////////////////////////////////////////////
// CIngestClientSettings dialog


CIngestClientSettings::CIngestClientSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CIngestClientSettings::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIngestClientSettings)
	m_szDesc = _T("");
	//}}AFX_DATA_INIT
	m_bHasDlg = true;  //set to false to disable DoModal;

	m_pszAppName = NULL;
	m_pszAboutText = NULL;
	m_pszSettingsText = NULL;
}

CIngestClientSettings::~CIngestClientSettings()
{
	if(m_pszAppName) free(m_pszAppName);
	if(m_pszAboutText) free(m_pszAboutText);
	if(m_pszSettingsText) free(m_pszSettingsText);
}


void CIngestClientSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIngestClientSettings)
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIngestClientSettings, CDialog)
	//{{AFX_MSG_MAP(CIngestClientSettings)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIngestClientSettings message handlers

int CIngestClientSettings::Settings(bool bRead)
{
	CFileUtil fu;
	if(fu.GetSettings(SETTINGS_FILENAME, false) ==	FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			char* pch;
			pch = fu.GetIniString("Global", "AppName", "IngestClient");
			if(pch)
			{
				if(m_pszAppName) free(m_pszAppName);
				m_pszAppName = pch;
			}

			pch = (char*)malloc(strlen(m_pszAppName)+strlen(VERSION_STRING)+strlen(", version ")+1);
			if(pch)
			{
				if(m_pszAboutText) free(m_pszAboutText);
				m_pszAboutText = pch;
				sprintf(m_pszAboutText, "%s, version %s", m_pszAppName, VERSION_STRING);
			}

			pch = fu.GetIniString("Global", "SettingsText", "IngestClient settings...");
			if(pch)
			{
				if(m_pszSettingsText) free(m_pszSettingsText);
				m_pszSettingsText = pch;
			}


//			int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);
		}
		else
		{
			if(m_pszAppName) fu.SetIniString("Global", "AppName", m_pszAppName);
//			if(m_pszAboutText) fu.SetIniString("Global", "AboutText", m_pszAboutText);  // dont save
			if(m_pszSettingsText) fu.SetIniString("Global", "SettingsText", m_pszSettingsText);
//			int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);

			if(!(fu.SetSettings(SETTINGS_FILENAME, false)&FILEUTIL_MALLOC_OK)) return CLIENT_ERROR;
		}

		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}
