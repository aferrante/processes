; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=3
Class1=CIngestClientApp
LastClass=CIngestClientDlg
NewFileInclude2=#include "IngestClient.h"
ResourceCount=2
NewFileInclude1=#include "stdafx.h"
Resource1=IDD_DIALOG_SETTINGS
Class2=CIngestClientSettings
LastTemplate=CDialog
Class3=CIngestClientDlg
Resource2=IDD_DIALOG_MAIN

[CLS:CIngestClientApp]
Type=0
HeaderFile=IngestClient.h
ImplementationFile=IngestClient.cpp
Filter=N
LastObject=CIngestClientApp

[DLG:IDD_DIALOG_MAIN]
Type=1
Class=CIngestClientDlg
ControlCount=8
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_LIST1,SysListView32,1350631437
Control4=IDC_BUTTON_ADD,button,1342242816
Control5=IDC_BUTTON_EDIT,button,1342242816
Control6=IDC_BUTTON_DEL,button,1342242816
Control7=IDC_STATIC,static,1342308352
Control8=IDC_BUTTON_REFRESH,button,1342242816

[DLG:IDD_DIALOG_SETTINGS]
Type=1
Class=CIngestClientSettings
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT_DESC,edit,1350631552

[CLS:CIngestClientSettings]
Type=0
HeaderFile=IngestClientSettings.h
ImplementationFile=IngestClientSettings.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_EDIT_DESC
VirtualFilter=dWC

[CLS:CIngestClientDlg]
Type=0
HeaderFile=IngestClientDlg.h
ImplementationFile=IngestClientDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_LIST1

