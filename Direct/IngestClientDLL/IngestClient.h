// IngestClient.h : main header file for the CLIENTDLL DLL
//

#if !defined(AFX_CLIENTDLL_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_CLIENTDLL_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "C:/Source/Applications/Generic/Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"

#include "IngestClientCore.h"
#include "IngestClientData.h"
#include "IngestClientDlg.h"
#include "IngestClientSettings.h"

#define VERSION_STRING "1.0.0.1"

/////////////////////////////////////////////////////////////////////////////
// CIngestClientApp
// See IngestClient.cpp for the implementation of this class
//

class CIngestClientApp : public CWinApp
{
public:
	CIngestClientApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIngestClientApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CIngestClientApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENTDLL_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
