// DirectData.cpp: implementation of the CDirectData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DirectData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// CDirectData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectData::CDirectData()
{
	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = DIRECT_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
	m_pszInfo = NULL;		// human readable info string

	m_pdbConnPrimary = NULL;
	m_pdbConnBackup = NULL;

	m_bCloneMode = false;
	m_bCloneInitReadSuccess  = false;
	m_ulLastCloneTime = 0;  

	m_ppdbDest = NULL;
	m_ulNumDest = 0;
	m_ulNextPing=0;

	m_ppdbChannels = NULL;
	m_ulNumChannels = 0;

	m_ppdbCriteria = NULL;  // file type lookups etc.
	m_ulNumCriteria = 0; // number of criteria


	m_bDiskWarningSent = false;
	m_bDiskWarning = false;
	m_bDiskDeletionSent = false;
	m_bDiskDeletion = false;

	m_bWatchfolderInitialized=false;

}

CDirectData::~CDirectData()
{
// AfxMessageBox("wait data");
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	if((m_ppdbDest)&&(m_ulNumDest))
	{
		unsigned long i=0;
		while (i<m_ulNumDest)
		{
			if(m_ppdbDest[i])
			{
				if(m_ppdbDest[i]->pszIP) free(m_ppdbDest[i]->pszIP);
				if(m_ppdbDest[i]->pszExtensions) free(m_ppdbDest[i]->pszExtensions);
				delete m_ppdbDest[i];
			}
			i++;
		}

		if(m_ppdbDest) delete [] m_ppdbDest;
	}	
	if((m_ppdbChannels)&&(m_ulNumChannels))
	{
		unsigned long i=0;
		while (i<m_ulNumChannels)
		{
			if(m_ppdbChannels[i])
			{
				delete m_ppdbChannels[i];
			}
			i++;
		}

		if(m_ppdbChannels) delete [] m_ppdbChannels;
	}
	if((m_ppdbCriteria)&&(m_ulNumCriteria))
	{
		unsigned long i=0;
		while (i<m_ulNumCriteria)
		{
			if(m_ppdbCriteria[i])
			{
				if(m_ppdbCriteria[i]->pszCriterion) free(m_ppdbCriteria[i]->pszCriterion);
				if(m_ppdbCriteria[i]->pszFlag) free(m_ppdbCriteria[i]->pszFlag);
				delete m_ppdbCriteria[i];
			}
			i++;
		}

		if(m_ppdbCriteria) delete [] m_ppdbCriteria;
	}
}

//case insensitive
unsigned long CDirectData::ReturnExchangeType(char* szCriterion)
{
	if((m_ppdbCriteria)&&(m_ulNumCriteria)&&(szCriterion)&&(strlen(szCriterion)>0))
	{
		unsigned long i=0;
		while (i<m_ulNumCriteria)
		{
			if(m_ppdbCriteria[i])
			{
				if(stricmp(m_ppdbCriteria[i]->pszCriterion, szCriterion)==0)
				{
					return m_ppdbCriteria[i]->ulMod;
				}
			}
			i++;
		}
	}
	return 0; // unknown type
}

int CDirectData::GetDestination(char* szIP)
{
	if((m_ppdbDest)&&(m_ulNumDest)&&(szIP)&&(strlen(szIP)>0))
	{
		unsigned long i=0;
		while (i<m_ulNumDest)
		{
			if(m_ppdbDest[i])
			{
				if(strcmp(m_ppdbDest[i]->pszIP, szIP)==0)
				{
					return i;
				}
			}
			i++;
		}
	}
	return -1;
}

int CDirectData::GetDestination(unsigned short usChannelID, unsigned char ucKeyerLayer )
{
	if((m_ppdbDest)&&(m_ulNumDest)&&(usChannelID>0)&&(ucKeyerLayer>0))
	{
		unsigned long i=0;
		while (i<m_ulNumDest)
		{
			if(m_ppdbDest[i])
			{
				if(m_ppdbDest[i]->usChannelID == usChannelID)
				{
					if( (1<<(ucKeyerLayer-1)) & m_ppdbDest[i]->ulKeyLayers )
						return i;
				}
			}
			i++;
		}
	}
	return -1;
}

unsigned long CDirectData::GetChannelFlag(unsigned short usChannelID)
{
	if((m_ppdbChannels)&&(m_ulNumChannels>0)&&(usChannelID>0))
	{
		unsigned long i=0;
		while (i<m_ulNumChannels)
		{
			if(m_ppdbChannels[i])
			{
				if(m_ppdbChannels[i]->usChannelID == usChannelID)
				{
						return m_ppdbChannels[i]->ulFlags;
				}
			}
			i++;
		}
	}
	return 0x00000000;
}

