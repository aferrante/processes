// ChannelUtilData.h: interface for the CChannelUtilData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTDLLDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
#define AFX_CLIENTDLLDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CChannelUtilData  
{
public:
	CChannelUtilData();
	virtual ~CChannelUtilData();

	bool m_bDialogStarted;
};

#endif // !defined(AFX_CLIENTDLLDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
