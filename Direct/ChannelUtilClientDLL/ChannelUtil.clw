; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=3
Class1=CChannelUtilApp
LastClass=CChannelUtilDlg
NewFileInclude2=#include "ChannelUtil.h"
ResourceCount=2
NewFileInclude1=#include "stdafx.h"
Resource1=IDD_DIALOG_SETTINGS
Class2=CChannelUtilSettings
LastTemplate=CDialog
Class3=CChannelUtilDlg
Resource2=IDD_DIALOG_MAIN

[CLS:CChannelUtilApp]
Type=0
HeaderFile=ChannelUtil.h
ImplementationFile=ChannelUtil.cpp
Filter=N
LastObject=CChannelUtilApp

[DLG:IDD_DIALOG_MAIN]
Type=1
Class=CChannelUtilDlg
ControlCount=5
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_STATIC,static,1342308352
Control4=IDC_COMBO_VIEW,combobox,1344339971
Control5=IDC_LIST1,SysListView32,1350631437

[DLG:IDD_DIALOG_SETTINGS]
Type=1
Class=CChannelUtilSettings
ControlCount=17
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT_DSN,edit,1350631552
Control5=IDC_RADIO_MODE1,button,1342308361
Control6=IDC_RADIO_MODE2,button,1342177289
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC_SQL,button,1342177287
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT_USERID,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_EDIT_PW,edit,1350631584
Control13=IDC_STATIC_TCP,button,1342177287
Control14=IDC_STATIC,static,1342308352
Control15=IDC_EDIT_IP,edit,1350631552
Control16=IDC_STATIC,static,1342308352
Control17=IDC_EDIT_PORT,edit,1350639744

[CLS:CChannelUtilSettings]
Type=0
HeaderFile=ChannelUtilSettings.h
ImplementationFile=ChannelUtilSettings.cpp
BaseClass=CDialog
Filter=D
LastObject=CChannelUtilSettings
VirtualFilter=dWC

[CLS:CChannelUtilDlg]
Type=0
HeaderFile=ChannelUtilDlg.h
ImplementationFile=ChannelUtilDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CChannelUtilDlg

