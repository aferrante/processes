// ChannelUtil.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "ChannelUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

#define CLIENTDLLDLG_MINSIZEX 200
#define CLIENTDLLDLG_MINSIZEY 150

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilApp

BEGIN_MESSAGE_MAP(CChannelUtilApp, CWinApp)
	//{{AFX_MSG_MAP(CChannelUtilApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilApp construction

CChannelUtilApp::CChannelUtilApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CChannelUtilApp object

CChannelUtilApp				theApp;
CChannelUtilCore			g_core;
CChannelUtilData			g_data;
CChannelUtilDlg*			g_pdlg = NULL;
CChannelUtilSettings	g_settings;
CMessager*					g_pmsgr = NULL;

int  CChannelUtilApp::DLLCtrl(void** ppvoid, UINT nType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	int nReturn = CLIENT_SUCCESS;
	switch(nType)
	{
	case DLLCMD_MAINDLG_BEGIN://					0x00
		{
			//gets the address of a Dlg pointer.
			if(!g_data.m_bDialogStarted)
			{
				g_pdlg = new CChannelUtilDlg;

				if(*ppvoid) // we should be passing in the parent Wnd.
				{
					if(g_pdlg->Create((CWnd*)(*ppvoid))) 
					{
						g_data.m_bDialogStarted = true;
						if (g_pdlg)	*ppvoid = g_pdlg;
						else *ppvoid=NULL;

						g_pdlg->ShowWindow(SW_SHOW);
						g_pdlg->ShowWindow(SW_HIDE);
					}
					else
					{
						*ppvoid=NULL;
						g_data.m_bDialogStarted = false;
						nReturn = CLIENT_ERROR; //error
					}
				} 
				else 
				{
					*ppvoid=NULL;
					g_data.m_bDialogStarted = false;
					nReturn = CLIENT_ERROR; //error, need the parent wnd
				}
			}

		} break;
	case DLLCMD_MAINDLG_END://						0x01
		{
			if(g_data.m_bDialogStarted)
			{
				g_data.m_bDialogStarted = false;
				if(g_pdlg) g_pdlg->OnExit();
			}
		} break;
	case DLLCMD_SETTINGSTXT://						0x02
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszSettingsText)&&(strlen(g_settings.m_pszSettingsText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszSettingsText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszSettingsText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_DOSETTINGSDLG://					0x03
		{
			// takes a NULL pointer.
			if(g_settings.m_bHasDlg)
			{
				g_settings.DoModal();
			}
			else
			{
				// just refresh
				if(g_settings.Settings(true)==CLIENT_SUCCESS)
					nReturn = CLIENT_REFRESH;
				else nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETABOUTTXT://						0x04
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAboutText)&&(strlen(g_settings.m_pszAboutText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAboutText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAboutText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETAPPNAME://							0x05
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAppName)&&(strlen(g_settings.m_pszAppName)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAppName)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAppName);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
			
		} break;
	case DLLCMD_SETDISPATCHER://					0x06
		{
			if (*ppvoid)
			{
				g_pmsgr = (CMessager*)*ppvoid;
//				CString szMsg; szMsg.Format("%08x dispatcher",g_pmd);
//				AfxMessageBox(szMsg);
//				AfxMessageBox(AfxGetAppName());
			}
			else
			{
				nReturn = CLIENT_ERROR;
			}  

		} break;
	case DLLCMD_GETMINX://							0x07
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)CLIENTDLLDLG_MINSIZEX;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETMINY://							0x08
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)CLIENTDLLDLG_MINSIZEY;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	default: nReturn = CLIENT_UNKNOWN; break;
	}
	return nReturn;
}
