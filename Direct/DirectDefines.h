// DirectDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(DIRECTDEFINES_H_INCLUDED)
#define DIRECTDEFINES_H_INCLUDED

#include <stdlib.h>


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define DIRECT_CURRENT_VERSION		"1.0.1.3"
//#define DIRECT_AUTH_USER					"cortex"  // for cortex to tell an object that it is cortex that is sending commands
//#define DIRECT_AUTH_PWD						"medulla" // cortex's password


// modes
#define DIRECT_MODE_DEFAULT			0x00000000  // exclusive
#define DIRECT_MODE_LISTENER		0x00000001  // exclusive
#define DIRECT_MODE_CLONE				0x00000002  // exclusive
#define DIRECT_MODE_QUIET				0x00000004  // ORable - means, no statup UI and no message boxes.
#define DIRECT_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override

// default port values.
#define DIRECT_PORT_FILE				80		
#define DIRECT_PORT_CMD					10660		
#define DIRECT_PORT_STATUS			10661		

#define DIRECT_PORT_INVALID			0	

// types
#define DIRECT_TYPE_UNDEF				0x0000  // undefined	
#define DIRECT_TYPE_RESOURCE		0x8000  // resource	
#define DIRECT_TYPE_PROCESS			0x4000  // process	

// resource types
#define DIRECT_TYPE_DB					0x0001  // database	
#define DIRECT_TYPE_AUTOMATION	0x0002  // automation	
#define DIRECT_TYPE_CG					0x0003  // CG	
#define DIRECT_TYPE_AUDIO				0x0004  // audio	
#define DIRECT_TYPE_MIXER				0x0005  // mixer	
#define DIRECT_TYPE_FEED				0x0006  // feed	
#define DIRECT_TYPE_POLL				0x0007  // polling data manager
#define DIRECT_TYPE_USER				0x000f  // user defined	

// process types
#define DIRECT_TYPE_PERIODIC		0x0010  // periodic process, like a show	
#define DIRECT_TYPE_ETERNAL			0x0020  // eternal process.	
#define DIRECT_TYPE_INFO				0x0100  // process that acts as an information resource for other processes.	
#define DIRECT_TYPE_META				0x0200  // process that keeps tabs on other processes and resources, without grabbing ownership.	


// status
#define DIRECT_STATUS_UNINIT				0x00000000  // uninitialized	

// owners are zero-based enumerated, but we reserve the following values
#define DIRECT_OWNER_INVALID				0xffffffff  // not owned, or owner unknown
#define DIRECT_OWNER_DIRECT					0xfffffffe  // owned by this instance of cortex (not another cortex somewhere)

// default intervals
#define DIRECT_TIME_PING				5000		// get device status every 5 seconds
#define DIRECT_TIME_FAIL				60000		// object failure after 1 minute timeout


//return values
#define DIRECT_SUCCESS   0
#define DIRECT_ERROR	   -1


// default filenames
#define DIRECT_SETTINGS_FILE_DEFAULT	  "direct.csf"		// csf = cortex settings file
//#define DIRECT_SETTINGS_FILE_LISTENER	  "listen.csf"		// csf = cortex settings file
//#define DIRECT_SETTINGS_FILE_CLONE		  "clone.csf"			// csf = cortex settings file
//#define DIRECT_SETTINGS_FILE_SPARK			"cortex.csl"		// csl = cortex spark list
//#define DIRECT_SETTINGS_FILE_ENCRYPT		"cortex.esf"		// esf = encrypted settings file
//#define DIRECT_SETTINGS_FILE_SECURE1		"cortex.epa"		// epa = ecrypted password archive
//#define DIRECT_SETTINGS_FILE_SECURE2		"cortex.epb"		// epb = ecrypted password backup


// object status states:


// common commands.  objects must be careful not to override these.
#define DIRECT_CMD_NULL					0x00  // null or empty command
#define DIRECT_CMD_ACK					0x06  // same as ascii, used as response only
#define DIRECT_CMD_NAK					0x15  // same as ascii, used as response only

#define DIRECT_CMD_HASAUTH			0x80  // usually a subcommand, ORable, menas, has auth info in data.

// commands used by cortex to command cortex listeners
#define DIRECT_CMD_SPARK				0x31  // run the spark list. main cortex hostname:commandport in data.
#define DIRECT_CMD_KILL					0x32  // send kill commands to the exes on the spark list.
#define DIRECT_CMD_KILLSELF			0x33  // end process, possibly for upgrade. (upgrading process should shell execute after upgrade)

// communications params
// commands used by cortex to command resources and processes
#define DIRECT_CMD_CHGCOMM			0x01  // change communication parameters.
// following are ORable subs.
#define DIRECT_CMD_HOST					0x01  // change the host (can be used for switching to backup)
#define DIRECT_CMD_PORT					0x02  // change the ports // has data "commandport:statusport"


// familiar name labels
#define DIRECT_CMD_GETNAME			0x02  // get the familiar names associated with a process or resource.
#define DIRECT_CMD_CHGNAME			0x03  // change the familiar name for a process or resource (if we have one thats identical, must append a suffix to make unique)  new name given in data
// following are exclusive subs.
#define DIRECT_CMD_OBJ					0x01  // default, get the familiar name for the object
#define DIRECT_CMD_DEST					0x02  // get the familiar name for all destinations in a resource.


// other object information
#define DIRECT_CMD_GETINFO			0x04  // get the information associated with a cortex object
#define DIRECT_CMD_ASSIGN				0x05  // set ownership and other info
// following are exclusive subs.
#define DIRECT_CMD_TYPE					0x01  // get the enumerated type value of the object.
#define DIRECT_CMD_STATUS				0x02  // get current status.
#define DIRECT_CMD_OWNER				0x03  // get/set the name of the current owner.
#define DIRECT_CMD_STATUSINT		0x04  // get/set status interval.
#define DIRECT_CMD_FAILINT			0x05  // get/set failure interval.
#define DIRECT_CMD_SETTING			0x06  // get/set setting value.
#define DIRECT_CMD_AUTHCRED			0x07  // set cortex authorization credentials to identify cortex to a module

// files
#define DIRECT_CMD_GETFILE			0x06	// get files associated with module.
// following are exclusive subs.
#define DIRECT_CMD_LIST_ADMIN   0x01  // retrieves a list of files and templates necessary for admin
#define DIRECT_CMD_LIST_AUTH	  0x02  // retrieves a list of files and templates necessary for auth
#define DIRECT_CMD_LIST_STATUS  0x03  // retrieves a list of files and templates necessary for status
#define DIRECT_CMD_LIST_LOGS	  0x04  // retrieves a list of files and templates necessary for logs
#define DIRECT_CMD_LIST_HELP	  0x05  // retrieves a list of files and templates necessary for help
#define DIRECT_CMD_LIST_QUERY	  0x06  // retrieves a list of files that match a query
#define DIRECT_CMD_FILE					0x08  // retrieves a single file. (used after receiving lists, retrieves deficiencies)


// client module commands
#define DIRECT_CMD_BYE				  0x0f  // command module to shut down.
#define DIRECT_CMD_PING				  0xaa  // just check that its there..

#define DIRECT_CMD_GETCHANNELS  0x20  // get list of channels
#define DIRECT_CMD_GETDESTS		  0x21  // get list of destinations
#define DIRECT_CMD_GETMETA		  0x22  // get list of metadata
#define DIRECT_CMD_GETSETTINGS  0x23  // get list of settings

#define DIRECT_CMD_SETCHANNELS  0x30  // set list of channels
#define DIRECT_CMD_SETDESTS		  0x31  // set list of destinations
#define DIRECT_CMD_SETMETA		  0x32  // set list of metadata
#define DIRECT_CMD_SETSETTINGS  0x33  // set list of settings


// commands used by resources and processes to request things from cortex
#define DIRECT_REQ_HELLO				0x01  // startup... has data "host:commandport:statusport"
#define DIRECT_REQ_GIVESTATUS		0x02  // gives formatted status of module and destinations, etc
#define DIRECT_REQ_REQOWN		    0x03  // requests ownership of a destination
#define DIRECT_REQ_RELOWN		    0x04  // releases ownership of a destination
#define DIRECT_REQ_AUTHCHK		  0x05  // check credentials against the cortex main security files

// commands used by cortex to answer resources and processes. (main cmd should be ACK or NAK plus flags)
#define DIRECT_REPLY_OK					0x00  // was able to comply
#define DIRECT_REPLY_NOT				0x01  // was not able to comply.

// channel flags
#define DIRECT_CHANNEL_NA						0x000000  // channel not available - default!
#define DIRECT_CHANNEL_USE					0x000001  // use this channel
#define DIRECT_CHANNEL_TYPEMASK			0x000030  // bit mask for frame basis
#define DIRECT_CHANNEL_DF						0x000000  // use NTSC drop frame (default)
#define DIRECT_CHANNEL_NDF					0x000010  // use NTSC no drop frame (black and white!)
#define DIRECT_CHANNEL_PAL					0x000020  // use PAL

// destination type codes
#define DIRECT_DESTTYPE_UNK				0x00  // undefined
#define DIRECT_DESTTYPE_IS2				0x01  // Miranda imagestore 2
#define DIRECT_DESTTYPE_INT				0x02  // Miranda intuition
#define DIRECT_DESTTYPE_IS300			0x03  // Miranda imagestore 300.
#define DIRECT_DESTTYPE_ISHD			0x04  // Miranda imagestore HD.

// asset mgmt live event status codes
#define DIRECT_LESTATUS_MASK					0x00000f00  // no status - a new event
#define DIRECT_LESTATUS_NONE					0x00000000  // no status - a new event
#define DIRECT_LESTATUS_CHK						0x00000100  // checking - if a tem file is needed from machine, means xferring that one back to controller
#define DIRECT_LESTATUS_XFER					0x00000200  // xferring a file to a machine (ORed with 0-255 ordinal of which file is being transferred) for IS2, 0=oxa, 1=oxt, 2=oxw.  for INT 0=tem, then enum'ed components as listed by tem.
#define DIRECT_LESTATUS_SET						0x00000300  // all set to go, file is there.
#define DIRECT_LESTATUS_NOFILE				0x00000400  // error - controller doesnt have the file.
#define DIRECT_LESTATUS_ERR						0x00001000  // sent the error notification
#define DIRECT_LESTATUS_UNSUPPORTED		0x00002000  // the event exists, but we arent going to deal with it, because we dont know how, or it is disallowed.
// colossus live event status codes
#define DIRECT_LESTATUS_PLAYED				0x01000000  // event has played (we need to count)
#define DIRECT_LESTATUS_EXISTS				0x02000000  // event exists.

// live event fields.
#define DIRECT_LEFIELD_ID							0
#define DIRECT_LEFIELD_CHANNEL				1
#define DIRECT_LEFIELD_DATA						2
#define DIRECT_LEFIELD_ONAIRTIME			3
#define DIRECT_LEFIELD_STATUS					4
#define DIRECT_LEFIELD_DURATION				5
#define DIRECT_LEFIELD_CLIPID					6
#define DIRECT_LEFIELD_CLIPTITLE			7
#define DIRECT_LEFIELD_FILES					8
#define DIRECT_LEFIELD_IP							9

#define DIRECT_LEFIELD_FILESWIDTH			1024

// metadata fields
#define DIRECT_MFIELD_FILE						0
#define DIRECT_MFIELD_PATH						1
#define DIRECT_MFIELD_LINKED					2
#define DIRECT_MFIELD_DESC						3
#define DIRECT_MFIELD_OP							4
#define DIRECT_MFIELD_TYPE						5
#define DIRECT_MFIELD_DUR							6
#define DIRECT_MFIELD_VALID						7
#define DIRECT_MFIELD_EXPIRE					8
#define DIRECT_MFIELD_INGEST					9
#define DIRECT_MFIELD_SYS_LAST				10
#define DIRECT_MFIELD_SYS_TIMES				11
#define DIRECT_MFIELD_SYS_FLAGS				12

// messages fields
#define DIRECT_MSGFIELD_MSG							0
#define DIRECT_MSGFIELD_SENDER					1
#define DIRECT_MSGFIELD_FLAGS						2
#define DIRECT_MSGFIELD_SYSTIME					3
#define DIRECT_MSGFIELD_ID							4

// system file flags
#define DIRECT_FILEFLAG_RECORDONLY			0x00000000
#define DIRECT_FILEFLAG_FILEXFER				0x00000001
#define DIRECT_FILEFLAG_FILECOPIED			0x00000002


#define DIRECT_RULE_TYPE_PRESMASTER 1 
#define DIRECT_RULE_TYPE_XML        2 

#define DIRECT_RULE_SEARCH_EXPLICIT 0 
#define DIRECT_RULE_SEARCH_AUDIO    1 
#define DIRECT_RULE_SEARCH_VIDEO    2 
#define DIRECT_RULE_SEARCH_AUDIOVIDEO  DIRECT_RULE_SEARCH_AUDIO|DIRECT_RULE_SEARCH_VIDEO

#define DIRECT_RULE_ACTION_NORM      0
#define DIRECT_RULE_ACTION_NULLOXT   1


// database structs
typedef struct db_dest_t
{
	char* pszIP;
	char* pszExtensions;
	unsigned short usChannelID;
	unsigned long ulKeyLayers;  //bit mask
	unsigned char ucDiskPercentage;  // after this is exceeded, we start deleting files we have backed up.
	unsigned char ucType;  // type of machine.
	unsigned long ulKBFree;  //KB free on device
	unsigned long ulKBTotal;  //KB total on device
	unsigned char ucChecksum;
	bool bDiskChecked;
} db_dest_t;

typedef struct db_channel_t
{
	unsigned short	usChannelID;
	unsigned long		ulFlags;  //bit mask
	unsigned char		ucChecksum;
} db_channel_t;

typedef struct db_exchange_t
{
	char* pszCriterion;
	char* pszFlag;
	unsigned long		ulMod;  //mod time or other value
	unsigned char ucChecksum;
} db_exchange_t;


#endif // !defined(DIRECTDEFINES_H_INCLUDED)
