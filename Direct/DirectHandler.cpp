// DirectHandler.cpp : implementation file
//

#include "stdafx.h"
#include "Direct.h"
//#include "DirectDlg.h"
#include "DirectHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ICONID 100
#define LEFTCLICK_TIMER 69
#define RIGHTCLICK_TIMER 99
#define MONITOR_TIMER_ID 27
#define KILL_TIMER_ID 42
#define MONITOR_TIMER_INT 1200 // update tray icon every x ms

#define TRAY_NOTIFYICON WM_USER+500

extern CDirectApp theApp;
extern bool g_bKillThread;
extern bool g_bThreadStarted;


/////////////////////////////////////////////////////////////////////////////
// CDirectHandler

CDirectHandler::CDirectHandler()
{
	m_bLeftFireDoubleClick=FALSE;
	m_bRightFireDoubleClick=FALSE;
	m_bLeft=TRUE;
	m_nLogoCounter=0;
}

CDirectHandler::~CDirectHandler()
{
//	AfxMessageBox("wait handler");
}


BEGIN_MESSAGE_MAP(CDirectHandler, CWnd)
	//{{AFX_MSG_MAP(CDirectHandler)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_CMD_ABOUT, OnCmdAbout)
	ON_COMMAND(ID_CMD_EXIT, OnCmdExit)
	ON_COMMAND(ID_CMD_SETTINGS, OnCmdSettings)
	ON_COMMAND(ID_CMD_SHOWWND, OnCmdShowwnd)
	//}}AFX_MSG_MAP
	ON_MESSAGE(TRAY_NOTIFYICON, OnTrayNotify)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDirectHandler message handlers

BOOL CDirectHandler::Create()
{
	return CWnd::CreateEx(WS_EX_TOOLWINDOW, AfxRegisterWndClass(0), "DirectHandler",
		WS_OVERLAPPED,0,0,0,0,NULL,NULL);
}

void CDirectHandler::PostNcDestroy() 
{
	CWnd::PostNcDestroy();
	delete this;
}

void CDirectHandler::OnLButtonUp(UINT nFlags, CPoint point) 
{
	OnLeftClick();
}

void CDirectHandler::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	OnLeftDoubleClick();
}

void CDirectHandler::OnRButtonUp(UINT nFlags, CPoint point) 
{
	OnRightClick();
}

void CDirectHandler::OnRButtonDblClk(UINT nFlags, CPoint point) 
{
	OnRightDoubleClick();
}

void CDirectHandler::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == LEFTCLICK_TIMER)	
	{
		KillTimer(LEFTCLICK_TIMER);
		PostMessage(WM_LBUTTONUP);
	}
	else
	if(nIDEvent == RIGHTCLICK_TIMER)	
	{
		KillTimer(RIGHTCLICK_TIMER);
		PostMessage(WM_RBUTTONUP);
	}
	else
	if(nIDEvent == KILL_TIMER_ID)	
	{
		if(!g_bThreadStarted)
		{
//			if(m_pMainDlg) ((CDirectDlg*)m_pMainDlg)->OnExit();

			KillTimer(KILL_TIMER_ID);
			KillTimer(MONITOR_TIMER_ID);
			PostMessage(WM_CLOSE);
		}
	}
	else
	if(nIDEvent == MONITOR_TIMER_ID)	
	{
		static DWORD dwAction = NIM_MODIFY;
		CString szToolTip = _T("");
		HICON hIcon;

		// must have some kind of conditional here to set the tooltip and icon
		EnterCriticalSection(&theApp.m_crit);

		m_nLogoCounter++;
		if(m_nLogoCounter>3) m_nLogoCounter=0;

		if( m_nLogoCounter==0 ) // logo icon
		{
			m_nCurrentIcon=ICON_LOGO;
			hIcon = m_hIcon[m_nCurrentIcon];
			if(theApp.m_nAppState<=1)
			{
		//		szToolTip.Format("This is the Direct tooltip, icon %d", m_nCurrentIcon);
				szToolTip.Format("Direct is in progress.");
			}
			else
			{
//				szToolTip.Format("Direct has errors.\nUse client for more info.\n\n%s", theApp.m_pszAppStateText);
				szToolTip.Format("Direct:\n%s", theApp.m_pszAppStateText);  // must not exceed 80 chars, m_pszAppStateText limited to 64
			}
			NotifyIcon(dwAction, hIcon, szToolTip);
		}
		else 
		if( m_nLogoCounter==1 ) // status icon
		{
			if(theApp.m_nAppState<=1)
			{
				m_nCurrentIcon=ICON_GRN;
				hIcon = m_hIcon[m_nCurrentIcon];
		//		szToolTip.Format("This is the Direct tooltip, icon %d", m_nCurrentIcon);
				szToolTip.Format("Direct is in progress.");
			}
			else
			{
				if(theApp.m_nAppState<=ICON_YEL)
					m_nCurrentIcon=theApp.m_nAppState;
				else
					m_nCurrentIcon=ICON_RED;
				hIcon = m_hIcon[m_nCurrentIcon];
		//		szToolTip.Format("This is the Direct tooltip, icon %d", m_nCurrentIcon);
//				szToolTip.Format("Direct has errors.\nUse client for more info.\n\n%s", theApp.m_pszAppStateText);
				szToolTip.Format("Direct:\n%s", theApp.m_pszAppStateText);  // must not exceed 80 chars, m_pszAppStateText limited to 64
			}
			NotifyIcon(dwAction, hIcon, szToolTip);
		} // else dont update.
		LeaveCriticalSection(&theApp.m_crit);

	}
	else
	CWnd::OnTimer(nIDEvent);
}

LONG CDirectHandler::OnTrayNotify(UINT wParam, LONG lParam)
{
	UINT uIconID = (UINT)wParam;
	UINT uMouseMsg = (UINT)lParam;

	if(uIconID != ICONID) return 0;
	switch (uMouseMsg)
	{
	case WM_LBUTTONUP:
		{
			m_bLeft=TRUE;
			if(m_bLeftFireDoubleClick) PostMessage(WM_LBUTTONDBLCLK);
		}
		break;
	case WM_RBUTTONUP:
		{
			m_bLeft=FALSE;
			if(m_bRightFireDoubleClick) PostMessage(WM_RBUTTONDBLCLK);
		}
		break;
	case WM_LBUTTONDOWN:
		{
			m_bLeft=TRUE;
			m_bLeftFireDoubleClick = FALSE;
			SetTimer(LEFTCLICK_TIMER, GetDoubleClickTime(), NULL);
		}
		break;
	case WM_RBUTTONDOWN:
		{
			m_bLeft=FALSE;
			m_bRightFireDoubleClick = FALSE;
			SetTimer(RIGHTCLICK_TIMER, GetDoubleClickTime(), NULL);
		}
		break;
	case WM_LBUTTONDBLCLK:
		{
			m_bLeftFireDoubleClick = TRUE;
			KillTimer(LEFTCLICK_TIMER);
		}
		break;
	case WM_RBUTTONDBLCLK:
		{
			m_bRightFireDoubleClick = TRUE;
			SetTimer(RIGHTCLICK_TIMER, GetDoubleClickTime(), NULL);
		}
		break;
	}
	return 0;

}

BOOL CDirectHandler::NotifyIcon(DWORD dwMessage, HICON hIcon, LPCTSTR pszToolTip)
{
	ASSERT(dwMessage == NIM_ADD ||dwMessage == NIM_DELETE ||dwMessage == NIM_MODIFY);
	static HICON hCurrentIcon = NULL;
	NOTIFYICONDATA nid;
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = GetSafeHwnd();
	nid.uID = ICONID;
	nid.uCallbackMessage = TRAY_NOTIFYICON;
	nid.uFlags = NIF_MESSAGE;

	if((hIcon!= NULL)&&(hIcon!=hCurrentIcon))
	{
		nid.uFlags |= NIF_ICON;
		nid.hIcon = hIcon;
		hCurrentIcon=hIcon;
	}
	if(pszToolTip!=NULL)
	{
		nid.uFlags |= NIF_TIP;
		lstrcpy(nid.szTip,pszToolTip);
	}

//	if(dwMessage&NIM_ADD) AfxMessageBox("adding");
	return Shell_NotifyIcon(dwMessage, &nid);
}

BOOL CDirectHandler::NotifyIcon(DWORD dwMessage, HICON hIcon, UINT nStringResource)
{
	CString szToolTip;
	VERIFY(szToolTip.LoadString(nStringResource));
	return NotifyIcon(dwMessage, hIcon, szToolTip);
}

void CDirectHandler::OnRightClick()
{
/*
	CPoint pt;
	GetCursorPos(&pt);
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU1));
	int nMenu = 0;
	CMenu* pMenu; 

// use alternate menu instead of changing text
	if(m_pMainDlg->IsWindowVisible()) nMenu=1;
	pMenu = menu.GetSubMenu(nMenu);

	SetForegroundWindow();
	pMenu->TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y, this);
	PostMessage(WM_USER);
*/
}

void CDirectHandler::OnRightDoubleClick()
{
}

void CDirectHandler::OnLeftClick()
{
/*
	if(m_pMainDlg->IsWindowVisible())
		m_pMainDlg->ShowWindow(SW_HIDE);
	else
		m_pMainDlg->ShowWindow(SW_SHOW);
*/
}

void CDirectHandler::OnLeftDoubleClick()
{
/*	
	if(m_pMainDlg->IsWindowVisible())
		m_pMainDlg->ShowWindow(SW_HIDE);
	else
		m_pMainDlg->ShowWindow(SW_SHOW);
*/
}

int CDirectHandler::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
/*
	for (int i=0; i<MAX_ICONS; i++)
	{
		switch(i)
		{
		case ICON_BARS: m_hIcon[ICON_BARS] =	AfxGetApp()->LoadIcon(IDI_ICON_COMP_BARS); break;
		case ICON_BLK:  m_hIcon[ICON_BLK]  =	AfxGetApp()->LoadIcon(IDI_ICON_COMP_BLK); break;
		case ICON_BLU:  m_hIcon[ICON_BLU]  =	AfxGetApp()->LoadIcon(IDI_ICON_COMP_BLU); break;
		case ICON_GRN:  m_hIcon[ICON_GRN]  =	AfxGetApp()->LoadIcon(IDI_ICON_COMP_GRN); break;
		case ICON_RED:  m_hIcon[ICON_RED]  =	AfxGetApp()->LoadIcon(IDI_ICON_COMP_RED); break;
		case ICON_YEL:  m_hIcon[ICON_YEL]  =	AfxGetApp()->LoadIcon(IDI_ICON_COMP_YEL); break;
		case ICON_CLR:  m_hIcon[ICON_CLR]  =	AfxGetApp()->LoadIcon(IDI_ICON_QUAD_CLR); break;
		}
	}
*/
// *

	for (int i=0; i<MAX_ICONS; i++)
	{
		switch(i)
		{
		case ICON_LOGO: m_hIcon[ICON_LOGO] =	AfxGetApp()->LoadIcon(IDR_MAINFRAME); break;
		case ICON_GRN:  m_hIcon[ICON_GRN]  =	AfxGetApp()->LoadIcon(IDI_ICON_GRN); break;
		case ICON_RED:  m_hIcon[ICON_RED]  =	AfxGetApp()->LoadIcon(IDI_ICON_X); break;
		case ICON_YEL:  m_hIcon[ICON_YEL]  =	AfxGetApp()->LoadIcon(IDI_ICON_YEL); break;
		}
	}
// * /

	m_nCurrentIcon=ICON_LOGO;
	NotifyIcon(NIM_ADD, m_hIcon[ICON_LOGO], "VDS Direct");

	// for updating the tray icon
	SetTimer(MONITOR_TIMER_ID, MONITOR_TIMER_INT, NULL);
	OnTimer(MONITOR_TIMER_ID); // force immediate update

//	if(m_pMainDlg) m_pMainDlg->ShowWindow(SW_HIDE);  // start invisible
//	if(m_pMainDlg) m_pMainDlg->ShowWindow(SW_SHOW);  // start visible
	

	return 0;
}

void CDirectHandler::OnDestroy() 
{
	CWnd::OnDestroy();

	// removes icon from tray on destroy
	NotifyIcon(NIM_DELETE, NULL);
}

void CDirectHandler::OnCmdAbout() 
{
//	CAboutDlg dlgAbout;
//	dlgAbout.DoModal();
}


void CDirectHandler::OnExternalCmdExit() 
{
	OnCmdExit();
}

void CDirectHandler::OnCmdExit() 
{
	// if you want confirmation, uncomment the AfxMessageBox
//	if(AfxMessageBox("Are you sure you want to exit?",MB_ICONQUESTION|MB_YESNO)!=IDYES) return;

//	if(m_pMainDlg) m_pMainDlg->GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText("shutting down...");

	g_bKillThread=true;

	SetTimer(KILL_TIMER_ID, 50, NULL); // for cleanup to occur before destruction of app.
}

void CDirectHandler::OnCmdSettings() 
{
//	((CDirectDlg*)m_pMainDlg)->ShellExecuteSettings();

//	g_settings.DoModal();
}

void CDirectHandler::OnCmdShowwnd() 
{
/*
	if(m_pMainDlg) 
	{
		if(m_pMainDlg->IsWindowVisible())
			m_pMainDlg->ShowWindow(SW_HIDE);
		else
			m_pMainDlg->ShowWindow(SW_SHOW);
	}
*/
}

