// ChannelUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ChannelUtil.h"
#include "ChannelUtilDlg.h"
#include "../DirectDefines.h"
#include "../../../Common/MFC/ODBC/DBUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



extern CChannelUtilApp			theApp;
extern CChannelUtilCore			g_core;
extern CChannelUtilData			g_data;
extern CChannelUtilDlg*			g_pdlg;
extern CChannelUtilSettings	g_settings;
extern CMessager*					g_pmsgr;

#define  LB_SCROLLEXCLUDE_CX 18

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilDlg dialog


CChannelUtilDlg::CChannelUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChannelUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChannelUtilDlg)
	//}}AFX_DATA_INIT
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
}


void CChannelUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChannelUtilDlg)
	DDX_Control(pDX, IDC_COMBO_VIEW, m_cb);
	DDX_Control(pDX, IDC_LIST1, m_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChannelUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CChannelUtilDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO_VIEW, OnSelchangeComboView)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilDlg message handlers

void CChannelUtilDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CChannelUtilDlg::OnOK() 
{
//CDialog::OnOK();
}

BOOL CChannelUtilDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}

void CChannelUtilDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID = 0;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			default: nCtrlID=0;break;
			}
			if(nCtrlID)
			{
				GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
				ScreenToClient(&m_rcCtrl[i]);
			}
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
}

void CChannelUtilDlg::OnSize(UINT nType, int cx, int cy) 
{
	int nCtrlID = 0;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_LIST1);
		if(pWnd!=NULL)
		{
			pWnd->SetWindowPos( 
			&wndTop,
			0,0,
			m_rcCtrl[ID_LIST].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_LIST].Height()-dy,  // goes with bottom edge, 
			SWP_NOZORDER|SWP_NOMOVE
			);

			for(int i=0; i<m_nNumCols; i++)
			{
				m_list.SetColumnWidth(i, ((m_rcCtrl[ID_LIST].Width()-dx-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100 );
			}


		}
		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			default: nCtrlID=0;break;
			}
			if(nCtrlID) GetDlgItem(nCtrlID)->Invalidate();
		}
	}		
}

BOOL CChannelUtilDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	g_settings.Settings(true);

	m_cb.SetCurSel(0);
	m_nMode = 0;
	OnSelchangeComboView();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChannelUtilDlg::OnExit()
{
	// add cleanup here if necesary;

	// save settings;
	g_settings.Settings(false);
	CDialog::OnCancel();
}

void CChannelUtilDlg::OnSelchangeComboView() 
{
	KillTimer(m_nMode);

	m_nMode = m_cb.GetCurSel(); 
	if((m_nMode<0)||(m_nMode>4))
	{
		m_cb.SetCurSel(0);//CB_ERR
		m_nMode=0;
	}

	/*
Channels
Live Events
Graphics Devices
Registered Types
System Messages
	*/

// clear list box;
	CRect rc; 
	m_list.GetWindowRect(&rc);
	char pszColName[16][64]; // dont need this many.
	LV_COLUMN lvCol[16]; // dont need this many.
	m_nNumCols = 0;
	int i=0;

	for(i=0; i<16; i++)	lvCol[16].mask = LVCF_FMT|LVCF_TEXT|LVCF_WIDTH;


	m_list.DeleteAllItems( );  // clear list

	// prep the column information.
	switch(m_nMode)
	{
		case 0: //Channels
			{
				// there are 3 columns.
				m_nNumCols = 3;
				i=0;

				strcpy(pszColName[i], "Channel ID");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 10;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Description");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 60;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Attributes");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 30;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;

			} break;
		case 1: //Live Events
			{
				// there are 8 columns.
				m_nNumCols = 8;
				i=0;

				strcpy(pszColName[i], "Channel ID");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 6;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Clip ID");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 10;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "ClipTitle");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 20;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Status");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 14;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "On Air Time");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 10;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Duration");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 10;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Event Data");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 15;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Device");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 15;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;

			} break;
		case 2: //Graphics Devices
			{
				// there are 6 columns.
				m_nNumCols = 6;
				i=0;

				strcpy(pszColName[i], "Device address");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 15;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Type");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 15;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Description");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 25;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Disk space limit");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 15;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Current disk usage");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 15;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Extension data");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 15;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

			} break;
		case 3: //Registered Types
			{
				// there are 3 columns.
				m_nNumCols = 3;
				i=0;

				strcpy(pszColName[i], "File extension");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 20;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Description");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 60;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Type ID");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 20;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;

			} break;
		case 4: //System Messages
			{
				// there are 3 columns.

				m_nNumCols = 4;
				i=0;

				strcpy(pszColName[i], "Timestamp");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 20;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Message");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 60;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;
				i++;

				strcpy(pszColName[i], "Sender");
				lvCol[i].pszText = pszColName[i];

				m_nColWidthPercent[i] = 20;
				lvCol[i].cx = ((rc.Width()-LB_SCROLLEXCLUDE_CX)*m_nColWidthPercent[i])/100;

				lvCol[i].fmt = LVCFMT_LEFT;


			} break;
	}

	while(m_list.DeleteColumn(0))
	{
		; // just keep going until all columns are deleted.
	}

	for(i=0; i<m_nNumCols; i++)
	{
		m_list.InsertColumn(i, lvCol[i].pszText, lvCol[i].fmt, lvCol[i].cx, 0 );
	}

	SetTimer(m_nMode, 2000, NULL);  // two second refresh of data.
	OnTimer(m_nMode);  //call it immeditaly tho.


}

// all the DB display calls are in OnTimer. (updates handled separately)
void CChannelUtilDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	switch(nIDEvent)
	{
	case 0: //Channels
		{
			KillTimer(m_nMode);

		} break;
	case 1: //Live Events
		{

		} break;
	case 2: //Graphics Devices
		{
			KillTimer(m_nMode);
		} break;
	case 3: //Registered Types
		{
			KillTimer(m_nMode);
		} break;
	case 4: //System Messages
		{
		} break;
	default: 	CDialog::OnTimer(nIDEvent); break;
	}
	
}
