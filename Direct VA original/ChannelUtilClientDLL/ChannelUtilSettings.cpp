// ChannelUtilSettings.cpp : implementation file
//

#include "stdafx.h"
#include "ChannelUtil.h"
#include "ChannelUtilSettings.h"
#include "../../../Common/TXT/BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SETTINGS_FILENAME "ChannelUtil.ini"

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilSettings dialog


CChannelUtilSettings::CChannelUtilSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CChannelUtilSettings::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChannelUtilSettings)
	m_nConnMode = -1;
	//}}AFX_DATA_INIT
	m_bHasDlg = true;  //set to false to disable DoModal;

	m_pszAppName = NULL;
	m_pszAboutText = NULL;
	m_pszSettingsText = NULL;

	// TCPIP connection
	m_pszModuleIP = NULL;
	m_nModulePort = 10660;

	// ODBC connection
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

}

CChannelUtilSettings::~CChannelUtilSettings()
{
	if(m_pszAppName) free(m_pszAppName);
	if(m_pszAboutText) free(m_pszAboutText);
	if(m_pszSettingsText) free(m_pszSettingsText);

	if(m_pszModuleIP) free(m_pszModuleIP);

	if(m_pszDSN) free(m_pszDSN);
	if(m_pszUser) free(m_pszUser);
	if(m_pszPW) free(m_pszPW);

}


void CChannelUtilSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChannelUtilSettings)
	DDX_Radio(pDX, IDC_RADIO_MODE1, m_nConnMode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChannelUtilSettings, CDialog)
	//{{AFX_MSG_MAP(CChannelUtilSettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilSettings message handlers

int CChannelUtilSettings::Settings(bool bRead)
{
	CFileUtil fu;
	if(fu.GetSettings(SETTINGS_FILENAME, false) ==	FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			char* pch;
			pch = fu.GetIniString("Global", "AppName", "ChannelUtil");
			if(pch)
			{
				if(m_pszAppName) free(m_pszAppName);
				m_pszAppName = pch;
			}

			pch = (char*)malloc(strlen(m_pszAppName)+strlen(VERSION_STRING)+strlen(", version ")+1);
			if(pch)
			{
				if(m_pszAboutText) free(m_pszAboutText);
				m_pszAboutText = pch;
				sprintf(m_pszAboutText, "%s, version %s", m_pszAppName, VERSION_STRING);
			}

			pch = fu.GetIniString("Global", "SettingsText", "ChannelUtil settings...");
			if(pch)
			{
				if(m_pszSettingsText) free(m_pszSettingsText);
				m_pszSettingsText = pch;
			}

			pch = fu.GetIniString("Client", "ModuleIP", "127.0.0.1");
			if(pch)
			{
				if(m_pszModuleIP) free(m_pszModuleIP);
				m_pszModuleIP = pch;
			}

			m_nModulePort = fu.GetIniInt("Client", "ModulePort", 10660);

			pch = fu.GetIniString("Client", "DSN", "DataSourceName");
			if(pch)
			{
				if(m_pszDSN) free(m_pszDSN);
				m_pszDSN = pch;
			}
			pch = fu.GetIniString("Client", "User", "user");
			if(pch)
			{
				if(m_pszUser) free(m_pszUser);
				m_pszUser = pch;
			}
			CBufferUtil bu;

			pch = fu.GetIniString("Client", "Password", "password");
			if(pch)
			{
				if(m_pszModuleIP) free(m_pszModuleIP);
				m_pszModuleIP = pch;
			}

//			int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);
		}
		else
		{
			if(m_pszAppName) fu.SetIniString("Global", "AppName", m_pszAppName);
//			if(m_pszAboutText) fu.SetIniString("Global", "AboutText", m_pszAboutText);  // dont save
			if(m_pszSettingsText) fu.SetIniString("Global", "SettingsText", m_pszSettingsText);
//			int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);
			if(m_pszModuleIP) fu.SetIniString("Client", "ModuleIP", m_pszModuleIP);

			if(!(fu.SetSettings(SETTINGS_FILENAME, false)&FILEUTIL_MALLOC_OK)) return CLIENT_ERROR;
		}

		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}

void CChannelUtilSettings::OnOK() 
{
	// TODO: Add extra validation here
	Settings(false);
	CDialog::OnOK();
}

BOOL CChannelUtilSettings::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	Settings(true);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
