// ChannelUtilData.cpp: implementation of the CChannelUtilData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ChannelUtil.h"
#include "ChannelUtilData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CChannelUtilData::CChannelUtilData()
{
	m_bDialogStarted = false;
}

CChannelUtilData::~CChannelUtilData()
{

}
