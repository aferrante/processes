#if !defined(AFX_CLIENTDLLSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
#define AFX_CLIENTDLLSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ChannelUtilSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilSettings dialog

class CChannelUtilSettings : public CDialog
{
// Construction
public:
	CChannelUtilSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChannelUtilSettings();   // standard destructor

// Dialog Data
	//{{AFX_DATA(CChannelUtilSettings)
	enum { IDD = IDD_DIALOG_SETTINGS };
	int		m_nConnMode;
	//}}AFX_DATA

	// internal
	bool m_bHasDlg;

	//exposed
	char* m_pszAppName;
	char* m_pszAboutText;
	char* m_pszSettingsText;

	// TCPIP connection
	char* m_pszModuleIP;
	int   m_nModulePort;

	// ODBC connection
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;
	
	int Settings(bool bRead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChannelUtilSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChannelUtilSettings)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENTDLLSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
