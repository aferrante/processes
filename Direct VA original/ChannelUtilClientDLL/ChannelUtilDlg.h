#if !defined(AFX_CLIENTDLLDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
#define AFX_CLIENTDLLDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ChannelUtilDlg.h : header file
//
#define ID_LIST 0
#define DLG_NUM_MOVING_CONTROLS 1

/////////////////////////////////////////////////////////////////////////////
// CChannelUtilDlg dialog

class CChannelUtilDlg : public CDialog
{
// Construction
public:
	CChannelUtilDlg(CWnd* pParent = NULL);   // standard constructor
//	virtual ~CChannelUtilDlg();   // standard destructor

// Dialog Data
	//{{AFX_DATA(CChannelUtilDlg)
	enum { IDD = IDD_DIALOG_MAIN };
	CComboBox	m_cb;
	CListCtrl	m_list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChannelUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[DLG_NUM_MOVING_CONTROLS];

	int m_nColWidthPercent[16];
	int m_nNumCols;
	int m_nMode;

public:
	void OnExit();

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChannelUtilDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboView();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENTDLLDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
