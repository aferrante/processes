//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ChannelUtil.rc
//
#define IDD_DIALOG_MAIN                 129
#define IDD_DIALOG_SETTINGS             130
#define IDC_EDIT_DESC                   1000
#define IDC_EDIT_DSN                    1000
#define IDC_STATIC_TEXT                 1001
#define IDC_EDIT_USERID                 1001
#define IDC_COMBO_VIEW                  1002
#define IDC_EDIT_PW                     1002
#define IDC_LIST1                       1003
#define IDC_EDIT_IP                     1003
#define IDC_RADIO_MODE1                 1004
#define IDC_RADIO_MODE2                 1005
#define IDC_STATIC_SQL                  1006
#define IDC_STATIC_TCP                  1007
#define IDC_EDIT_PORT                   1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
