# Microsoft Developer Studio Project File - Name="Direct" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Direct - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Direct.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Direct.mak" CFG="Direct - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Direct - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Direct - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Direct - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Direct - Win32 Release"
# Name "Direct - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\Common\TXT\BufferUtil.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\IMG\BMP\CBmpUtil_MFC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\MFC\ODBC\DBUtil.cpp
# End Source File
# Begin Source File

SOURCE=.\Direct.cpp
# End Source File
# Begin Source File

SOURCE=.\Direct.rc

!IF  "$(CFG)" == "Direct - Win32 Release"

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DirectData.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DirectHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\DirectMain.cpp
# End Source File
# Begin Source File

SOURCE=.\DirectSettings.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\FILE\DirUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\FileUtil.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\API\Miranda\IS2Comm.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\API\Miranda\IS2Comm.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\LogUtil.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\Messager.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\MessagingObject.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\LAN\NetUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\API\Omnibus\OmnibusDefs.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\API\Omnibus\OmnibusDefs.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\API\Omnibus\OmniComm.cpp

!IF  "$(CFG)" == "Direct - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Direct - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\API\Omnibus\OmniComm.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\Common\TXT\BufferUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\IMG\BMP\CBmpUtil_MFC.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MFC\ODBC\DBUtil.h
# End Source File
# Begin Source File

SOURCE=.\Direct.h
# End Source File
# Begin Source File

SOURCE=.\DirectData.h
# End Source File
# Begin Source File

SOURCE=.\DirectDefines.h
# End Source File
# Begin Source File

SOURCE=.\DirectHandler.h
# End Source File
# Begin Source File

SOURCE=.\DirectMain.h
# End Source File
# Begin Source File

SOURCE=.\DirectSettings.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\FILE\DirUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\FileUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\LogUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\Messager.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\MessagingObject.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\msg.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\LAN\NetDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\LAN\NetUtil.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Direct.ico
# End Source File
# Begin Source File

SOURCE=.\res\Direct.rc2
# End Source File
# Begin Source File

SOURCE=.\res\favicon.ico
# End Source File
# Begin Source File

SOURCE=.\res\hpbvdsw.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00005.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00006.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00007.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00008.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00009.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00010.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_com.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_qua.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_x1.ico
# End Source File
# Begin Source File

SOURCE=.\res\jewelIcon.ico
# End Source File
# Begin Source File

SOURCE=.\res\pauselist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\playlist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\settings.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Status-Blu.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Grn.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Red.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Yel.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\stdicons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Titling_Branding_Banner_Harris.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Titling_Branding_Banner_VDS.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\VDS_logo.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\todo.txt
# End Source File
# End Target
# End Project
