// DirectData.h: interface for the CDirectData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRECTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_DIRECTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "Direct.h"


class CDirectData  
{
public:
	CDirectData();
	virtual ~CDirectData();

	_timeb m_timebLastStatus; // the time of the last status given
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)
	char* m_pszStatus;	// parseable string
	char* m_pszInfo;		// human readable info string

	bool m_bCloneMode;
	bool m_bCloneInitReadSuccess;
	unsigned long m_ulLastCloneTime;  
	unsigned long m_ulNumCloneTimeSame;  

	// database data
	CDBconn* m_pdbConnPrimary;
	CDBconn* m_pdbConnBackup;

	db_dest_t** m_ppdbDest;
	unsigned long m_ulNumDest; // number of destination IPs
	unsigned long m_ulNextPing;  // next OXsox ping.  not the clone ping

	db_channel_t** m_ppdbChannels;
	unsigned long m_ulNumChannels; // number of registered channel IDs

	db_exchange_t** m_ppdbCriteria;  // file type lookups etc.
	unsigned long m_ulNumCriteria; // number of criteria

	bool m_bDiskWarningSent;
	bool m_bDiskWarning;
	bool m_bDiskDeletionSent;
	bool m_bDiskDeletion;

	bool m_bWatchfolderInitialized;

	int GetDestination(unsigned short usChannelID, unsigned char ucKeyerLayer );
	int GetDestination(char* szIP);
	unsigned long GetChannelFlag(unsigned short usChannelID);

	unsigned long ReturnExchangeType(char* szCriterion);

	ULARGE_INTEGER m_uliBytesAvail;
	ULARGE_INTEGER m_uliBytesTotal;
	ULARGE_INTEGER m_uliBytesFree;

	double m_dblAvail;
	double m_dblTotal;
	double m_dblFree;

};

#endif // !defined(AFX_DIRECTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
