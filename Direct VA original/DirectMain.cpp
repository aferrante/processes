// DirectMain.cpp: implementation of the CDirectMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "DirectMain.h"  
//#include "DirectDlg.h"  // just included to have access to windowing environment

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;

//extern CMessager* g_pmsgr;  // from Messager.cpp
extern CDirectApp theApp;

void DirectServerHandlerThread(void* pvArgs);



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectMain::CDirectMain()
{
}

CDirectMain::~CDirectMain()
{
//	AfxMessageBox("wait main");
}

int		CDirectMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // direct initiates a request to an object server
{
	return DIRECT_SUCCESS;
}

int		CDirectMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// direct replies to an object server after receiving data. (usually ack or nak)
{
	return DIRECT_SUCCESS;
}

int		CDirectMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// direct answers a request from an object client
{
	return DIRECT_SUCCESS;
}

int CDirectMain::DoAutoDeletion()
{
	// need to go out to the database and file cataloged files and delete by parameter:
	// prioritize and keep most recent last used date, most times used, smallest
	// delete least used, oldest, biggest until enough space

//gaaaa!

	//TODO


	return DIRECT_SUCCESS;
}

int CDirectMain::SendError(char* pszSender, char* pszError, ...)
{
 	if((m_data.m_pdbConnPrimary)&&(pszError)&&(strlen(pszError)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		int nMessageEventTableIndex = m_db.GetTableIndex(m_data.m_pdbConnPrimary, m_settings.m_pszTableMessages);
		if(nMessageEventTableIndex<0) return  DIRECT_ERROR;

		unsigned long ulID = 1;
		// get the highest ID#;
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY id DESC", 
			m_settings.m_pszTableMessages			);

		//Sleep(500);
		CRecordset* prs = m_db.Retrieve(m_data.m_pdbConnPrimary, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("id", szValue);
				unsigned long ulLastID = atol(szValue);
				ulID = ulLastID+1;
			}
			prs->Close();
			delete prs;

		}


		CDBCriterion* pdbcCriteria = new CDBCriterion(m_data.m_pdbConnPrimary, nMessageEventTableIndex);

		// load up the values.
		for(int i=0; i<m_data.m_pdbConnPrimary->m_pdbTable[nMessageEventTableIndex].m_usNumFields; i++)
		{
			switch(i)
			{
			case DIRECT_MSGFIELD_MSG://							0
				{
					va_list marker;
					// create the formatted output string
					va_start(marker, pszError); // Initialize variable arguments.
					_vsnprintf(szSQL, 511, pszError, (va_list) marker);
					va_end( marker );             // Reset variable arguments.
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(min(strlen(szSQL), 512)); // varchar 512
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, szSQL);
					}
					else
						pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_IGNORE;
				} break;
			case DIRECT_MSGFIELD_SENDER://					1
				{
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_IGNORE;
					if((pszSender)&&(strlen(pszSender)))
					{
						pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(64); // varchar 64
						if(pdbcCriteria->m_pdbField[i].m_pszData)
						{
							_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 63, "%s", pszSender);
							pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
						}
					}
				} break;
			case DIRECT_MSGFIELD_FLAGS://						2
				{
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", 1 ); // 1 is error, 0 is message
					}
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
				} break;
			case DIRECT_MSGFIELD_SYSTIME://					3
				{
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						_timeb timestamp;
						_ftime( &timestamp );
						_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", (unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)));
					}
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
				} break;
			case DIRECT_MSGFIELD_ID://							4
				{
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", ulID );
					}
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
				} break;
			} // switch
		} //for

		if(m_db.Insert(pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
		{
			m_msgr.DM(MSG_ICONERROR, NULL, "Direct:InsertError", "Insert returned an error.\n%s", dberrorstring);  //(Dispatch message)
			if(pdbcCriteria) delete pdbcCriteria;
			return DIRECT_ERROR;
		}
		else
		{
			// update the exchange database
			if(m_data.m_pdbConnPrimary)
			{

				// get the status #
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE criterion LIKE 'status_error'", 
					m_settings.m_pszTableExchange	);

				//Sleep(500);
				ulID = 0;
				CRecordset* prs = m_db.Retrieve(m_data.m_pdbConnPrimary, szSQL, dberrorstring);
				if(prs != NULL) 
				{
					if(!prs->IsEOF())
					{
						CString szValue;
						prs->GetFieldValue("mod", szValue);
						ulID = atol(szValue);
					}
					prs->Close();
					delete prs;
				}

				if(ulID == 0)
				{
					ulID++;

					// have to insert
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"INSERT INTO %s (criterion, flag, mod) VALUES ( 'status_error', 'Error status', %d);", 
						m_settings.m_pszTableExchange,
						ulID
						);
				}
				else
				{
					//update
					ulID++;

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s SET mod = %d WHERE criterion LIKE 'status_error'", 
						m_settings.m_pszTableExchange,
						ulID
						);
				}


				if(m_db.ExecuteSQL(m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
				{
		//			m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
				}
			}
		}
		if(pdbcCriteria) delete pdbcCriteria;
		return DIRECT_SUCCESS;
	}
	return DIRECT_ERROR;
}

int CDirectMain::SendMsg(char* pszSender, char* pszError, ...)
{
 	if((m_data.m_pdbConnPrimary)&&(pszError)&&(strlen(pszError)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		int nMessageEventTableIndex = m_db.GetTableIndex(m_data.m_pdbConnPrimary, m_settings.m_pszTableMessages);
		if(nMessageEventTableIndex<0) return  DIRECT_ERROR;

		unsigned long ulID = 1;
		// get the highest ID#;
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY id DESC", 
			m_settings.m_pszTableMessages			);

		//Sleep(500);
		CRecordset* prs = m_db.Retrieve(m_data.m_pdbConnPrimary, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("id", szValue);
				unsigned long ulLastID = atol(szValue);
				ulID = ulLastID+1;
			}
			prs->Close();
			delete prs;

		}


		CDBCriterion* pdbcCriteria = new CDBCriterion(m_data.m_pdbConnPrimary, nMessageEventTableIndex);

		// load up the values.
		for(int i=0; i<m_data.m_pdbConnPrimary->m_pdbTable[nMessageEventTableIndex].m_usNumFields; i++)
		{
			switch(i)
			{
			case DIRECT_MSGFIELD_MSG://							0
				{
					va_list marker;
					// create the formatted output string
					va_start(marker, pszError); // Initialize variable arguments.
					_vsnprintf(szSQL, 511, pszError, (va_list) marker);
					va_end( marker );             // Reset variable arguments.
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(min(strlen(szSQL), 512)); // varchar 512
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, szSQL);
					}
					else
						pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_IGNORE;
				} break;
			case DIRECT_MSGFIELD_SENDER://					1
				{
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_IGNORE;
					if((pszSender)&&(strlen(pszSender)))
					{
						pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(64); // varchar 64
						if(pdbcCriteria->m_pdbField[i].m_pszData)
						{
							_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 63, "%s", pszSender);
							pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
						}
					}
				} break;
			case DIRECT_MSGFIELD_FLAGS://						2
				{
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", 0 ); // 1 is error, 0 is message
					}
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
				} break;
			case DIRECT_MSGFIELD_SYSTIME://					3
				{
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						_timeb timestamp;
						_ftime( &timestamp );
						_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", (unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)));
					}
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
				} break;
			case DIRECT_MSGFIELD_ID://							4
				{
					pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
					if(pdbcCriteria->m_pdbField[i].m_pszData)
					{
						_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", ulID );
					}
					pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
				} break;
			} // switch
		} //for

		if(m_db.Insert(pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
		{
			m_msgr.DM(MSG_ICONERROR, NULL, "Direct:InsertMsg", "Insert returned an error.\n%s", dberrorstring);  //(Dispatch message)
			if(pdbcCriteria) delete pdbcCriteria;
			return DIRECT_ERROR;
		}
		else
		{
			// update the exchange database
			if(m_data.m_pdbConnPrimary)
			{

				// get the status #
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE criterion LIKE 'status_error'", 
					m_settings.m_pszTableExchange	);

				//Sleep(500);
				ulID = 0;
				CRecordset* prs = m_db.Retrieve(m_data.m_pdbConnPrimary, szSQL, dberrorstring);
				if(prs != NULL) 
				{
					if(!prs->IsEOF())
					{
						CString szValue;
						prs->GetFieldValue("mod", szValue);
						ulID = atol(szValue);
					}
					prs->Close();
					delete prs;
				}

				if(ulID == 0)
				{
					ulID++;

					// have to insert
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"INSERT INTO %s (criterion, flag, mod) VALUES ( 'status_error', 'Error status', %d);", 
						m_settings.m_pszTableExchange,
						ulID
						);
				}
				else
				{
					//update
					ulID++;

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s SET mod = %d WHERE criterion LIKE 'status_error'", 
						m_settings.m_pszTableExchange,
						ulID
						);
				}


				if(m_db.ExecuteSQL(m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
				{
		//			m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
				}
			}

		}

		if(pdbcCriteria) delete pdbcCriteria;
		return DIRECT_SUCCESS;
	}
	return DIRECT_ERROR;}



// means, find a file somewhere in the storage area
int CDirectMain::FindFile(char* pszFilename, char* pszBasePath, char** ppszFoundPath )
{
	WIN32_FIND_DATA wfd;
	HANDLE hfile = NULL;
	char dberrorstring[DB_ERRORSTRING_LEN];
	char szSQL[DB_SQLSTRING_MAXLEN];

	if((ppszFoundPath)&&(pszFilename)&&(strlen(pszFilename)))
	{
		// first, check the DB for the entry.
		// if it's in the DB, check it against the location.
		// if it is there, return the found path.
		// if not, search the base path (watchfolder passed in)


		if(m_data.m_pdbConnPrimary)
		{
			CDBRecord* pdbrData=NULL;
			unsigned long ulNumRecords=0;

			int nMetadataTableIndex = m_db.GetTableIndex(m_data.m_pdbConnPrimary, m_settings.m_pszTableMeta);
			if(nMetadataTableIndex>=0)
			{
				CDBCriterion* pdbcCriteria = new CDBCriterion(m_data.m_pdbConnPrimary, nMetadataTableIndex);

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE filename LIKE '%s'", 
					m_settings.m_pszTableMeta,
					pszFilename
					);

				//Sleep(500);
				CRecordset* prs = m_db.Retrieve(m_data.m_pdbConnPrimary, szSQL, dberrorstring);
				if(prs == NULL) 
				{
					m_msgr.DM(MSG_ICONERROR, NULL, "Direct:FindFile", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
				}
				else
				{
					// check the items.
//				AfxMessageBox("here");
					int x = m_db.Convert(prs, pdbcCriteria, &pdbrData, &ulNumRecords, dberrorstring);
//				AfxMessageBox("here2");
					if(x != DB_SUCCESS) 
					{
						if(prs)
						{
							prs->Close();
							delete prs;
						}
						m_msgr.DM(MSG_ICONERROR, NULL, "Direct:FindFile", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
					}
				}
				if(pdbcCriteria) delete pdbcCriteria;

			}

			if(pdbrData)
			{
				int nNumRecords = (int)ulNumRecords;
				if(nNumRecords>0)
				{
					// we found a record with this filename, check if it's there.
					int nrec=0;

					if(
						  (pdbrData[nrec].m_ppszData)
						&&(pdbrData[nrec].m_ppszData[DIRECT_MFIELD_FILE])  // 0 is filename
						&&(pdbrData[nrec].m_ppszData[DIRECT_MFIELD_PATH])  // 1 is path.
						)
					{
						char pszSearchPath[MAX_PATH+1];
						_snprintf(pszSearchPath, MAX_PATH,
							"%s\\%s", pdbrData[nrec].m_ppszData[DIRECT_MFIELD_PATH], pdbrData[nrec].m_ppszData[DIRECT_MFIELD_FILE]);

						EnterCriticalSection(&m_dir.m_critFileOp);

						hfile = FindFirstFile( pszSearchPath,  // pointer to name of file to search for 
							&wfd  // pointer to returned information 
						); 

						// note: have to recurse dirs if set in pdu->m_bUseSubDirs
						if(hfile)
						{
							if(!(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
							{
								char* pchReturn = (char*)malloc(strlen(pszSearchPath)+1);
								if(pchReturn) 
								{
									strcpy(pchReturn, pszSearchPath);
									*ppszFoundPath = pchReturn;
									FindClose( hfile); 
									LeaveCriticalSection(&m_dir.m_critFileOp);

									return DIRECT_SUCCESS;
								}
							}
						}
						LeaveCriticalSection(&m_dir.m_critFileOp);

					}

					// now clear out memory... 
					while(nrec<nNumRecords)
					{
						pdbrData[nrec].Free();
						nrec++;  // next record
					}
				}
				delete [] pdbrData;
			}
		}

		// else it comes here if file not found at coordinates.


		if((pszBasePath)&&(strlen(pszBasePath)))
		{

			char pszSearchPath[MAX_PATH+1];
			while(pszBasePath[strlen(pszBasePath)-1] == '\\') pszBasePath[strlen(pszBasePath)-1]=0; // remove trailing '\' if any
			sprintf(pszSearchPath, "%s\\*.*", pszBasePath);

			EnterCriticalSection(&m_dir.m_critFileOp);

			hfile = FindFirstFile( pszSearchPath,  // pointer to name of file to search for 
				&wfd  // pointer to returned information 
			); 

			// note: have to recurse dirs if set in pdu->m_bUseSubDirs
			if(hfile)
			{

				if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file or dir
				{
					if(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)
					{
						char pszSubSearchPath[MAX_PATH+1];
						_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszBasePath, wfd.cFileName);
						int nRV = FindFile(pszFilename, pszSubSearchPath, ppszFoundPath); //recursion
						if(nRV == DIRECT_SUCCESS)
						{
							FindClose( hfile); 
							LeaveCriticalSection(&m_dir.m_critFileOp);

							return nRV;
						}
					}
					else
					{
						//check it for the file
						char* pch = strrchr(wfd.cFileName, '\\');
						if(pch) pch++;
						else pch = wfd.cFileName;
						if(stricmp(pszFilename, pch)==0)
						{
							char* pchReturn = (char*)malloc(strlen(pszBasePath)+2+strlen(pch));
							if(pchReturn) 
							{
								sprintf(pchReturn, "%s\\%s", pszBasePath, pch);
								*ppszFoundPath = pchReturn;
								FindClose( hfile); 
								LeaveCriticalSection(&m_dir.m_critFileOp);
								return DIRECT_SUCCESS;
							}
						}
					}
				}

				while(FindNextFile( hfile,  // handle to search 
				&wfd  // pointer to structure for data on found file 
				)) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
				{

					if((strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file or dir
					{
						if(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)
						{
							char pszSubSearchPath[MAX_PATH+1];
							_snprintf(pszSubSearchPath, MAX_PATH,  "%s\\%s", pszBasePath, wfd.cFileName);
							int nRV = FindFile(pszFilename, pszSubSearchPath, ppszFoundPath); //recursion
							if(nRV == DIRECT_SUCCESS)
							{
								FindClose( hfile); 
								LeaveCriticalSection(&m_dir.m_critFileOp);

								return nRV;
							}
						}
						else
						{
							//check it for the file
							char* pch = strrchr(wfd.cFileName, '\\');
							if(pch) pch++;
							else pch = wfd.cFileName;
							if(stricmp(pszFilename, pch)==0)
							{
								char* pchReturn = (char*)malloc(strlen(pszBasePath)+2+strlen(pch));
								if(pchReturn) 
								{
									sprintf(pchReturn, "%s\\%s", pszBasePath, pch);
									*ppszFoundPath = pchReturn;
									FindClose( hfile); 
									LeaveCriticalSection(&m_dir.m_critFileOp);
									return DIRECT_SUCCESS;
								}
							}
						}
					}
				}
				FindClose( hfile); 
				LeaveCriticalSection(&m_dir.m_critFileOp);
			}
		}
	}
	return DIRECT_ERROR;

}


// device specific functions
int CDirectMain::Miranda_CheckFileExists(char* pszFilename, char* pszDirAlias, int nDest)
{
	FileInfo_ file_info;
	int nReturn = m_ox.OxSoxGetFileInfo(pszFilename, pszDirAlias, &file_info, NULL, OX_FILEINFO);
	if(nReturn<OX_SUCCESS)
	{
		// bad call
		//TODO report
		m_msgr.DM(MSG_ICONERROR, NULL, "Direct:Miranda_CheckFileExists", "OxSoxGetFileInfo returned %d for %s on directory alias %s, at IP address %s.", 
			nReturn, 
			pszFilename?pszFilename:"(null)",
			pszDirAlias?pszDirAlias:"(null)",
			m_data.m_ppdbDest[nDest]->pszIP?m_data.m_ppdbDest[nDest]->pszIP:"(null)"
			);  //(Dispatch message)
		return DIRECT_ERROR;
	}
	else
	if(nReturn&OX_NOFILE)
	{
		// file isnt there!
		return DIRECT_ERROR;
	}  // else it is!
	return DIRECT_SUCCESS;
}

int CDirectMain::Miranda_PushFile(char* pszFullSourcePath, char* pszFilename, char* pszDirAlias, int nDest)
{
	// Get the file size:
	WIN32_FIND_DATA wfd;
	EnterCriticalSection(&m_dir.m_critFileOp);
	if(FindFirstFile( pszFullSourcePath, &wfd)!=INVALID_HANDLE_VALUE)
	{
		LeaveCriticalSection(&m_dir.m_critFileOp);
		double dblFileSizeKB = ((double)wfd.nFileSizeLow/1024.0);
		if(wfd.nFileSizeHigh) dblFileSizeKB += (((double)wfd.nFileSizeHigh*(double)MAXDWORD)/1024.0);
		// check the disk space on IS2.

		bool bDone = false;
		do
		{
			DiskInfo_ disk_info;
			int nReturn = m_ox.OxSoxGetDriveInfo(pszDirAlias, &disk_info);
			int nDelMode = 0;
					
			if(nReturn<OX_SUCCESS)
			{
				// bad call
				//TODO report
				m_msgr.DM(MSG_ICONERROR, NULL, "Direct:Miranda_PushFile", "Error: OxSoxGetDriveInfo returned %d", nReturn);  //(Dispatch message)

				bDone = true; //  have to exit
			}
			else
			{
				double dblDiskPercentageUsed // will be used, after this file transfer.
					= (((double)(disk_info.KBytes_Total-disk_info.KBytes_Free)+dblFileSizeKB)*100.0)/(double)disk_info.KBytes_Total;
				if( dblDiskPercentageUsed > (double)m_data.m_ppdbDest[nDest]->ucDiskPercentage )
				{
					m_msgr.DM(MSG_ICONERROR, NULL, "Direct:Miranda_PushFile", "Percentage of disk space used after transfer will be %.02f, exceeding the limit (%.02f).",dblDiskPercentageUsed, (double)m_data.m_ppdbDest[nDest]->ucDiskPercentage);  //(Dispatch message)
					
					char dberrorstring[DB_ERRORSTRING_LEN];
					char szSQL[DB_SQLSTRING_MAXLEN];
					// delete oldest file
					// TODO - check DB and delete least popular!

					bool bFileFound = false;
					char FileName[IS2_MAX_FILENAME];

 					if(m_data.m_pdbConnPrimary)
					{
						//first check for expired ones.
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
							"SELECT * FROM [device_%s] WHERE (expires_after > 0) ORDER BY expires_after", 
							m_data.m_ppdbDest[nDest]->pszIP	);

						//Sleep(500);
						CRecordset* prs = m_db.Retrieve(m_data.m_pdbConnPrimary, szSQL, dberrorstring);
						if(prs == NULL) 
						{
							// this is OK, maybe we have nothing in the table.
							// m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CheckDestination", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
						}
						else
						{
							// check the items.
							_timeb timestamp;
							_ftime( &timestamp );
																	
							unsigned long ulNow =  timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0);

							while ((!bFileFound)&&(!prs->IsEOF()))
							{
								CString szValue;
								prs->GetFieldValue("filename", szValue);
								sprintf(FileName, "%s", szValue);
								prs->GetFieldValue("expires_after", szValue);
								unsigned long ulExp = atol(szValue);

								if((ulExp>0)&&(ulNow>ulExp))  // this is it.
								{
									bFileFound = true;
									nDelMode = 1;
								}
								prs->MoveNext();
							}
							prs->Close();
							delete prs;
						}

						// if we didnt find any that are expired, we have to try a different strategy, go statistical

						if(!bFileFound)
						{
							// cant use transfer date first, it might be an old but oft used file.
							// we go by times used first, then by last used (oldest).
							// we will delete the least used file with the oldest xfer date, if it has no
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
								"SELECT * FROM [device_%s] WHERE (expires_after > 0) ORDER BY sys_times_used, sys_last_used, transfer_date", 
								m_data.m_ppdbDest[nDest]->pszIP	);

							//Sleep(500);
							prs = m_db.Retrieve(m_data.m_pdbConnPrimary, szSQL, dberrorstring);
							if(prs == NULL) 
							{
								m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CheckDestination", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
							}
							else
							{
								// check the items.
								_timeb timestamp;
								_ftime( &timestamp );
																		
								unsigned long ulNow =  timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0);
								if(prs)
								{
									while ((!bFileFound)&&(!prs->IsEOF()))
									{
										CString szValue;
										prs->GetFieldValue("filename", szValue);
										sprintf(FileName, "%s", szValue);
										prs->GetFieldValue("sys_last_used", szValue);
										unsigned long ulLastUsed = atol(szValue);
										prs->GetFieldValue("sys_times_used", szValue);
										unsigned long ulTimesUsed = atol(szValue);

										if((ulLastUsed>0)&&(ulTimesUsed>0))  // this is it.
										{
											bFileFound = true;
											nDelMode = 2;
										}
										prs->MoveNext();
									}
									prs->Close();

									if(!bFileFound)
									{
										// nothing has been used!  have to go by oldest transfer date.
										// reopen the results
										prs->Open();
										while ((!bFileFound)&&(!prs->IsEOF()))
										{
											CString szValue;
											prs->GetFieldValue("filename", szValue);
											sprintf(FileName, "%s", szValue);
											prs->GetFieldValue("sys_last_used", szValue);
											unsigned long ulTransfer = atol(szValue);

											if(ulTransfer>0)  // this is it.
											{
												bFileFound = true;
												nDelMode = 3;
											}
											prs->MoveNext();
										}
									}

									prs->Close();
									delete prs;
								}
							}
						}
					}

					if(!bFileFound) // we do have to delete something, so let's delete the oldest file.
					{
						PV3DirEntry_ pv3DirEntry = NULL;
						int nNumEntries =0;
						nReturn = m_ox.OxSoxGetDirInfo(pszDirAlias, &pv3DirEntry, &nNumEntries);
						if(nReturn<OX_SUCCESS) 
						{
							bDone = true; // break on error
						}
						else
						{
							// sort for oldest file.
							if(nNumEntries)
							{
								bFileFound = true;
								nDelMode = 4;
							}
							int x=0;
							UINT32 FileTimeStamp = UINT_MAX;

							strcpy(FileName, "");

							while(x<nNumEntries)
							{
								if(pv3DirEntry[x].FileTimeStamp<FileTimeStamp)
								{
									FileTimeStamp = pv3DirEntry[x].FileTimeStamp;
									strcpy(FileName, pv3DirEntry[x].FileName);
								}
								x++;
							}

							free(pv3DirEntry);
						}
					}

					if(bFileFound)
					{
						nReturn = m_ox.OxSoxDelFile(FileName, pszDirAlias);
						if(nReturn<OX_SUCCESS) bDone = true; // break on error
						else
						{
							switch(nDelMode)
							{
							case 1: m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxDelFile", "OxSoxDelFile: deleted %s on %s on %s based on oldest expiry date.", FileName, pszDirAlias, m_ox.m_pszHost?m_ox.m_pszHost:"(null)"); break; //(Dispatch message) 
							case 2: m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxDelFile", "OxSoxDelFile: deleted %s on %s on %s based on least number of times used.", FileName, pszDirAlias, m_ox.m_pszHost?m_ox.m_pszHost:"(null)"); break; //(Dispatch message) 
							case 3: m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxDelFile", "OxSoxDelFile: deleted %s on %s on %s based on oldest transfer date.", FileName, pszDirAlias, m_ox.m_pszHost?m_ox.m_pszHost:"(null)"); break; //(Dispatch message) 
							case 4: m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxDelFile", "OxSoxDelFile: deleted %s on %s on %s based on oldest file timestamp.", FileName, pszDirAlias, m_ox.m_pszHost?m_ox.m_pszHost:"(null)"); break; //(Dispatch message) 
							}
							if(m_data.m_pdbConnPrimary)
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
									"DELETE FROM [device_%s] WHERE (filename LIKE '%s');", 
									m_data.m_ppdbDest[nDest]->pszIP,
									FileName
									);

								//Sleep(500);
								if(m_db.ExecuteSQL(m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
								{
									// report DB error TODO...  if we want to later.
								}
							}
						}
					}
					else bDone = true; // break on error

				}
				else
					bDone = true;
			}
		} while(!bDone);

		// try to transfer
		int nReturn = m_ox.OxSoxPutFile(pszFullSourcePath, pszFilename, pszDirAlias);
		if(nReturn!=OX_SUCCESS)
		{
			// error
			//TODO report
			EnterCriticalSection(&theApp.m_crit);
			theApp.m_nAppState=ICON_RED;
			theApp.SetAppStateText("oxsox error.");
			LeaveCriticalSection(&theApp.m_crit);
			m_msgr.DM(MSG_ICONERROR, NULL, "Direct:OxSoxPutFile", "OxSoxPutFile returned %d for %s", nReturn, m_ox.m_pszHost?m_ox.m_pszHost:"(null)");  //(Dispatch message)

			return DIRECT_ERROR;
		}
		else
		{
			//TODO report
			EnterCriticalSection(&theApp.m_crit);
			theApp.m_nAppState=ICON_GRN;
			theApp.SetAppStateText("Direct is in progress.");
			LeaveCriticalSection(&theApp.m_crit);
			m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxPutFile", "OxSoxPutFile: transferring %s to %s on %s", (pszFullSourcePath)?(pszFullSourcePath):"(null)", pszDirAlias?pszDirAlias:"(null)", m_ox.m_pszHost?m_ox.m_pszHost:"(null)");  //(Dispatch message)
			return DIRECT_SUCCESS;
		}
	}
	LeaveCriticalSection(&m_dir.m_critFileOp);

	return DIRECT_ERROR;

}


int CDirectMain::Miranda_DealWithFile(char* pszFilename, char* pszDirAlias, int nDest, char** ppszFoundPath)
{
	if(ppszFoundPath) *ppszFoundPath = NULL;
	FileInfo_ file_info;

	//TODO - check that if no file, it errors
	int nReturn = m_ox.OxSoxGetFileInfo(pszFilename, pszDirAlias, &file_info, NULL, OX_FILEINFO);
	if(nReturn<OX_SUCCESS)
	{
		// bad call
		//TODO report
		m_msgr.DM(MSG_ICONERROR, NULL, "Direct:OxSoxGetFileInfo", "OxSoxGetFileInfo returned %d for %s on directory alias %s, at IP address %s.", 
			nReturn, 
			pszFilename?pszFilename:"(null)",
			pszDirAlias?pszDirAlias:"(null)",
			m_data.m_ppdbDest[nDest]->pszIP?m_data.m_ppdbDest[nDest]->pszIP:"(null)"
			);  //(Dispatch message)
	}
	else
	if(nReturn&OX_NOFILE)
	{
		// file isnt there!
		// check storage, if found move it.  if not, report

		*ppszFoundPath = NULL;
		nReturn = FindFile(pszFilename, 
			m_settings.m_pszPrimaryDestinationFolderPath, 
			 ppszFoundPath);
		if(nReturn == DIRECT_SUCCESS)
		{
			// pszFoundPath has the file
			m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "The file %s does not exist on directory alias %s, at IP address %s.", 
				pszFilename?pszFilename:"(null)",
				pszDirAlias?pszDirAlias:"(null)",
				m_data.m_ppdbDest[nDest]->pszIP?m_data.m_ppdbDest[nDest]->pszIP:"(null)"
				);  //(Dispatch message)


			// Get the file size:
			WIN32_FIND_DATA wfd;
			if(FindFirstFile( *ppszFoundPath, &wfd)!=INVALID_HANDLE_VALUE)
			{

				unsigned long ulFileSizeKB = (wfd.nFileSizeLow/1024);
				if(wfd.nFileSizeHigh) ulFileSizeKB += ((wfd.nFileSizeHigh*MAXDWORD)/1024);
				// check the disk space on IS2.

				bool bDone = false;
				do
				{
					DiskInfo_ disk_info;
					nReturn = m_ox.OxSoxGetDriveInfo(pszDirAlias, &disk_info);
					
					if(nReturn<OX_SUCCESS)
					{
						// bad call
						//TODO report

					}
					else
					{
						if( ((disk_info.KBytes_Free+ulFileSizeKB)/(disk_info.KBytes_Total/100)) > m_data.m_ppdbDest[nDest]->ucDiskPercentage)
						{
							// delete oldest file
							// TODO - check DB and delete least popular!

							PV3DirEntry_ pv3DirEntry = NULL;
							int nNumEntries =0;
							nReturn = m_ox.OxSoxGetDirInfo(pszDirAlias, &pv3DirEntry, &nNumEntries);
							if(nReturn<OX_SUCCESS) 
							{
								bDone = true; // break on error
							}
							else
							{
								// sort for oldest file.
								int x=0;
								char FileName[IS2_MAX_FILENAME];
								UINT32 FileTimeStamp = UINT_MAX;

								strcpy(FileName, "");

								while(x<nNumEntries)
								{
									if(pv3DirEntry[x].FileTimeStamp<FileTimeStamp)
									{
										FileTimeStamp = pv3DirEntry[x].FileTimeStamp;
										strcpy(FileName, pv3DirEntry[x].FileName);
									}
									x++;
								}

								free(pv3DirEntry);

								nReturn = m_ox.OxSoxDelFile(FileName, pszDirAlias);
								if(nReturn<OX_SUCCESS) bDone = true; // break on error
								else
								{
									m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxDelFile", "OxSoxDelFile: deleted %s on %s on %s", FileName, pszDirAlias, m_ox.m_pszHost?m_ox.m_pszHost:"(null)");  //(Dispatch message)
								}

							}
						}
						else
							bDone = true;
					}
				} while(!bDone);

				// try to transfer
				nReturn = m_ox.OxSoxPutFile(*ppszFoundPath, pszFilename, pszDirAlias);
				if(nReturn!=OX_SUCCESS)
				{
					// error
					//TODO report
			EnterCriticalSection(&theApp.m_crit);
					theApp.m_nAppState=ICON_RED;
					theApp.SetAppStateText("oxsox error.");
			LeaveCriticalSection(&theApp.m_crit);
					m_msgr.DM(MSG_ICONERROR, NULL, "Direct:OxSoxPutFile", "OxSoxPutFile returned %d for %s", nReturn, m_ox.m_pszHost?m_ox.m_pszHost:"(null)");  //(Dispatch message)

				}
				else
				{
					//TODO report
			EnterCriticalSection(&theApp.m_crit);
					theApp.m_nAppState=ICON_GRN;
					theApp.SetAppStateText( "Direct is in progress.");
			LeaveCriticalSection(&theApp.m_crit);
					m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxPutFile", "OxSoxPutFile: transferring %s to %s on %s", (*ppszFoundPath)?(*ppszFoundPath):"(null)", pszDirAlias?pszDirAlias:"(null)", m_ox.m_pszHost?m_ox.m_pszHost:"(null)");  //(Dispatch message)
					while( (m_ox.m_bTransferring)||(m_ox.m_bTransferSuppress))
					{
						Sleep(50); // wait for tem file to transfer, should be fairly quick...
					}
					Sleep(50); // added measure

				}

			}
			else
			{
				//. error, but should never get here.
				//TODO - deal and report.
				// couldnt find the file we just found!  huh?
			EnterCriticalSection(&theApp.m_crit);
					theApp.m_nAppState=ICON_RED;
					theApp.SetAppStateText( "file error.");
			LeaveCriticalSection(&theApp.m_crit);
					m_msgr.DM(MSG_ICONERROR, NULL, "Direct", "FindFirstFile failed to find %s.", (*ppszFoundPath)?(*ppszFoundPath):"(null)");  //(Dispatch message)
			}
		}
		else
		{
			// couldn't find it, have to report!
			EnterCriticalSection(&theApp.m_crit);
			theApp.m_nAppState=ICON_RED;
			theApp.SetAppStateText("file not found.");
			LeaveCriticalSection(&theApp.m_crit);
			m_msgr.DM(MSG_ICONINFO, NULL, "Direct:OxSoxGetFileInfo", "The file %s does not exist on directory alias %s, at IP address %s.\nThe file could also not be found in any subdirectory of %s.", 
				pszFilename?pszFilename:"(null)",
				pszDirAlias?pszDirAlias:"(null)",
				m_data.m_ppdbDest[nDest]->pszIP?m_data.m_ppdbDest[nDest]->pszIP:"(null)",
				m_settings.m_pszPrimaryDestinationFolderPath?m_settings.m_pszPrimaryDestinationFolderPath:"(null)"
				);  //(Dispatch message)
		}

		// 

	}
/*	else
	{
		// success, it's on the drive.
		// we didnt do a file find so we just NULL out the ppszFoundPath 
	}
*/
	return DIRECT_SUCCESS;
}


void DirectMainThread(void* pvArgs)
{
	CDirectApp* pApp = (CDirectApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment

	//startup.
	g_bThreadStarted = true;

	// windows stuff
//	CDirectDlg* pdlg = ((CDirectDlg*)(((CDirectHandler*)pApp->m_pMainWnd)->m_pMainDlg));
//	CWnd* pwndStatus = ((CDirectHandler*)pApp->m_pMainWnd)->m_pMainDlg->GetDlgItem(IDC_STATIC_STATUSTEXT);
//	CListCtrlEx* plist = (CListCtrlEx*)(&(pdlg->m_lce));
	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Direct\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}

//	pdlg->SetProgress(CXDLG_WAITING);

//	if(pwndStatus) pwndStatus->SetWindowText("Initializing...");
//	StatusMessage(plist, "Initializing..."); 

	//create the main object.
	CDirectMain direct;

	//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// modes.
			if(stricmp(pch, "/c")==0) // clone mode  (check for this first, it invalidates some of the rest)
			{
				//TODO create clone mode!
				direct.m_settings.m_ulMainMode |= DIRECT_MODE_CLONE;  // trumps listener
			}
			else
			if(stricmp(pch, "/l")==0) // listener mode
			{
				//TODO create listener mode
				direct.m_settings.m_ulMainMode |= DIRECT_MODE_LISTENER;  // trumped by clone
			}
			else
			if(stricmp(pch, "/q")==0) // quiet mode (works in listener or default, clone is by def quiet)
			{
				direct.m_settings.m_ulMainMode |= DIRECT_MODE_QUIET;	
			}
			else
			if(stricmp(pch, "/v")==0) // volatile mode settings used this session are not retained by default - does not override explicit saves via http interface
			{
				direct.m_settings.m_ulMainMode |= DIRECT_MODE_VOLATILE;	
			}
			else

			// params
			if(strnicmp(pch, "/s:", 3)==0) // server listen port(s) override
			{
			}
			else
			if(strnicmp(pch, "/r:", 3)==0) // resource port range override
			{
			}
			else
			if(strnicmp(pch, "/p:", 3)==0) // process port range override
			{
			}

			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	char pszFilename[MAX_PATH];

/*	if(direct.m_settings.m_ulMainMode&DIRECT_MODE_CLONE)
		strcpy(pszFilename, DIRECT_SETTINGS_FILE_CLONE);  // direct settings file
	else
	if(direct.m_settings.m_ulMainMode&DIRECT_MODE_LISTENER)
		strcpy(pszFilename, DIRECT_SETTINGS_FILE_LISTENER);  // direct settings file
	else  // default
*/
		strcpy(pszFilename, DIRECT_SETTINGS_FILE_DEFAULT);  // direct settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// get settings
		direct.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		if(direct.m_settings.m_bUseLog)
		{
			// for logfiles, we need params, and they must be in this format:
			//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
			pszParams = file.GetIniString("Messager", "LogFileIni", "Direct|YM||1");
			int nRegisterCode=0;

			nRegisterCode = direct.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", pszParams, errorstring);
			if (nRegisterCode != MSG_SUCCESS) 
			{
				// inform the windowing environment
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
				AfxMessageBox(errorstring);
//				pwndStatus->SetWindowText(errorstring);
			}

			free(pszParams); pszParams=NULL;
		}
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
		EnterCriticalSection(&theApp.m_crit);
		theApp.m_nAppState=ICON_RED;
		theApp.SetAppStateText("Log file error.");
		LeaveCriticalSection(&theApp.m_crit);
	}

	direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "--------------------------------------------------\n\
-------------- Direct %s start --------------", DIRECT_CURRENT_VERSION);  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	direct.m_http.InitializeMessaging(&direct.m_msgr);
	direct.m_net.InitializeMessaging(&direct.m_msgr);
	direct.m_ox.InitializeMessaging(&direct.m_msgr);
	direct.m_db.InitializeMessaging(&direct.m_msgr);
	direct.m_omni.InitializeMessaging(&direct.m_msgr);




// load up the rest of the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		direct.m_settings.m_pszName = file.GetIniString("Main", "Name", "Direct");
//		direct.m_settings.m_usFilePort = file.GetIniInt("FileServer", "ListenPort", DIRECT_PORT_FILE);
		direct.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", DIRECT_PORT_CMD);
		direct.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", DIRECT_PORT_STATUS);

//		direct.m_settings.m_usResourcePortMin = file.GetIniInt("Resources", "MinPort", DIRECT_PORT_RESMIN);
//		direct.m_settings.m_usResourcePortMax = file.GetIniInt("Resources", "MaxPort", DIRECT_PORT_RESMAX);

//		direct.m_settings.m_usProcessPortMin = file.GetIniInt("Processes", "MinPort", DIRECT_PORT_PRCMIN);
//		direct.m_settings.m_usResourcePortMin = file.GetIniInt("Processes", "MaxPort", DIRECT_PORT_PRCMAX);

		direct.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		direct.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
//		direct.m_settings.m_bUseLogRemote = file.GetIniInt("Messager", "LogRemote", 0)?true:false;

		// backup
		direct.m_settings.m_bUseClone = file.GetIniInt("Mode", "UseClone", 0)?true:false;  // default to not using a clone
		direct.m_settings.m_bInitClone = file.GetIniInt("Mode", "InitClone", 0)?true:false;  // default to not being a clone
		direct.m_settings.m_pszCloneIP = file.GetIniString("Clone", "Address", "127.0.0.1");
		direct.m_settings.m_usCloneCommandPort = file.GetIniInt("Clone", "CloneListenPort", DIRECT_PORT_CMD);

		// Database settings
		direct.m_settings.m_bUseBackupDB					= file.GetIniInt("Database", "UseBackup", 0)?true:false;	 // make calls to both pri and backup DBs
		direct.m_settings.m_pszPrimaryDSN					= file.GetIniString("Database", "PrimaryDSN", "DirectPrimary");						// DSN
		direct.m_settings.m_pszPrimaryDBUser			= file.GetIniString("Database", "PrimaryDBUser", "sa");				// user
		direct.m_settings.m_pszPrimaryDBPassword	= file.GetIniString("Database", "PrimaryDBPassword", "");		// pw
		direct.m_settings.m_pszBackupDSN					= file.GetIniString("Database", "BackupDSN", "DirectBackup");						// DSN
		direct.m_settings.m_pszBackupDBUser				= file.GetIniString("Database", "BackupDBUser", "sa");					// user
		direct.m_settings.m_pszBackupDBPassword		= file.GetIniString("Database", "BackupDBPassword", "");			// pw
		direct.m_settings.m_pszTableDest					= file.GetIniString("Database", "DestinationsTable", "Destinations");		// destination table name - info about graphics boxes and mapping
		direct.m_settings.m_pszTableMeta					= file.GetIniString("Database", "MetadataTable", "Metadata");		// metadata table name - info about graphics
		direct.m_settings.m_pszTableExchange			= file.GetIniString("Database", "ExchangeTable", "Exchange");		// exchange flag table - application info
		direct.m_settings.m_pszTableChannels			= file.GetIniString("Database", "ChannelsTable", "Channels");		// channels table - application info
		direct.m_settings.m_pszTableEvents				= file.GetIniString("Database", "EventsTable", "Events");		// events table name - info about event status
		direct.m_settings.m_pszTableMessages			= file.GetIniString("Database", "MessagesTable", "Messages");		// errors/messages table name

		// Automation settings
		direct.m_settings.m_usNumColossus = file.GetIniInt("Automation", "NumServers", 0);   // number of Colossus Adaptors

		direct.m_settings.m_ppszServerAddress = new char*[direct.m_settings.m_usNumColossus];	// array of Colossus Adaptor IP (port is 10540)
		direct.m_settings.m_ppszServerName		= new char*[direct.m_settings.m_usNumColossus];	// array of Friendly name of connection (optional).
		direct.m_settings.m_ppszDebugFile			= new char*[direct.m_settings.m_usNumColossus];	//  array of debug filenames
		direct.m_settings.m_ppszCommFile			= new char*[direct.m_settings.m_usNumColossus];	//  array of comm log filenames

		if(pszParams) {	free(pszParams);	pszParams=NULL;	}

		char buffer[64];

		unsigned short i=0;
		while(i<direct.m_settings.m_usNumColossus)
		{
			if(direct.m_settings.m_ppszServerAddress)
			{
				sprintf(buffer, "Server_%03d_Address", i);
				pszParams = file.GetIniString("Automation", buffer, "");
				if(pszParams)
				{ if(strlen(pszParams)) { direct.m_settings.m_ppszServerAddress[i] = pszParams; }
					else { free(pszParams); pszParams=NULL; direct.m_settings.m_ppszServerAddress[i] = NULL; }
				}
			}

			if(direct.m_settings.m_ppszServerName)
			{
				sprintf(buffer, "Server_%03d_Description", i);
				pszParams = file.GetIniString("Automation", buffer, "");
				if(pszParams)
				{ if(strlen(pszParams)) { direct.m_settings.m_ppszServerName[i] = pszParams; }
					else { free(pszParams); pszParams=NULL; direct.m_settings.m_ppszServerName[i] = NULL; }
				}
			}

			if(direct.m_settings.m_ppszDebugFile)
			{
				sprintf(buffer, "Server_%03d_DebugFilename", i);
				pszParams = file.GetIniString("Automation", buffer, "");
				if(pszParams)
				{ if(strlen(pszParams)) { direct.m_settings.m_ppszDebugFile[i] = pszParams; }
					else { free(pszParams); pszParams=NULL; direct.m_settings.m_ppszDebugFile[i] = NULL; }
				}
			}

			if(direct.m_settings.m_ppszCommFile)
			{
				sprintf(buffer, "Server_%03d_CommLogFilename", i);
				pszParams = file.GetIniString("Automation", buffer, "");
				if(pszParams)
				{ if(strlen(pszParams)) { direct.m_settings.m_ppszCommFile[i] = pszParams; }
					else { free(pszParams); pszParams=NULL; direct.m_settings.m_ppszCommFile[i] = NULL; }
				}
			}

			i++;
		}


	//imagestore specific 
		direct.m_settings.m_pszNullFile = file.GetIniString("Miranda_Imagestore", "NullFile", "null.oxt");		// the filename of the nullfile - must be in the system folder
		direct.m_settings.m_bUseNullFile = file.GetIniInt("Miranda_Imagestore", "UseNullFile", 1)?true:false;

		// App settings (process specific)
		direct.m_settings.m_ulProcessIntervalMS = file.GetIniInt("Application", "ProcessIntervalMS", 60000);		// interval on which to run the process, in milliseconds
		direct.m_settings.m_ulClonePingIntervalMS = file.GetIniInt("Application", "ClonePingIntervalMS", 15000);  // interval on which the clone pings, or which to expect the clone to ping, in milliseconds
		direct.m_settings.m_ulWatchIntervalMS = file.GetIniInt("Application", "WatchIntervalMS", 60000);			// interval on which to check the watch folder, in milliseconds
		direct.m_settings.m_ucWarnPercentageDiskFull  = file.GetIniInt("Application", "WarnPercentageDiskFull", 75);   // send a warning when this % of storage is exceeded
		direct.m_settings.m_ucDeletePercentageDiskFull = file.GetIniInt("Application", "DeletePercentageDiskFull", 95);   // start deleting files if this % is exceeded

		// deal with possible invalid values
		if(direct.m_settings.m_ucWarnPercentageDiskFull<10) direct.m_settings.m_ucWarnPercentageDiskFull=75;
		if(direct.m_settings.m_ucWarnPercentageDiskFull>95) direct.m_settings.m_ucWarnPercentageDiskFull=75;

		if(direct.m_settings.m_ucDeletePercentageDiskFull<10) direct.m_settings.m_ucWarnPercentageDiskFull=95;
		if(direct.m_settings.m_ucDeletePercentageDiskFull>99) direct.m_settings.m_ucWarnPercentageDiskFull=99;

	// watchfolder settings.
		direct.m_settings.m_pszWatchFolderPath = file.GetIniString("WatchFolder", "WatchPath", "X:\\watchfolder");									// the path of the watchfolder // a mapped drive
		direct.m_settings.m_pszPrimaryDestinationFolderPath = file.GetIniString("WatchFolder", "PrimaryDestinationPath", "C:\\assets");		// the path of the primary destination folder // local
		direct.m_settings.m_pszBackupDestinationFolderPath = file.GetIniString("WatchFolder", "BackupDestinationPath", "D:\\assets");			// the path of the backup destination folder // usually, a mapped drive.

	//system folder
		direct.m_settings.m_pszSystemFolderPath = file.GetIniString("SystemFolder", "SystemFolderPath", "C:\\system");			// the path of the folder used for parse files etc.


		// have to make the sys folder if it isnt there.
		// mkdir is fine if dir is there already
		char path_buffer[MAX_PATH+1];
		char dir_buffer[MAX_PATH+1];
		sprintf(path_buffer, direct.m_settings.m_pszSystemFolderPath);

		//_mkdir(pszPath);
		char* pchRoot = strstr(path_buffer, ":\\");  //drive delim.
		int nBegin=0;
		if(pchRoot) nBegin = pchRoot-path_buffer+2;

		// have to make dir.
		// if dir already exists, no problem, _mkdir just returns and we continue
		// so, we can always just call mkdir

		strcpy(dir_buffer, path_buffer);

		for (int q=nBegin; q<(int)strlen(path_buffer); q++)
		{
			if((path_buffer[q]=='/')||(path_buffer[q]=='\\'))
			{
				dir_buffer[q] = 0;
				if(strlen(dir_buffer)>0)
				{
					_mkdir(dir_buffer);
				}

				dir_buffer[q] = '\\';
			}
			else
			if(q==(int)strlen(path_buffer)-1)
			{
				if(strlen(dir_buffer)>0)
				{
					_mkdir(dir_buffer);
				}
			}
			else
				dir_buffer[q] = path_buffer[q];
		}


		//SAVE HERE ON INIT
	// save settings, creates keys if not exist

		// these explicts arent necessary - uncomment to write out a full file to edit...
//		file.SetIniInt("FileServer", "ListenPort", direct.m_settings.m_usFilePort);
		file.SetIniInt("CommandServer", "ListenPort", direct.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", direct.m_settings.m_usStatusPort);

//		file.SetIniInt("Resources", "MinPort", direct.m_settings.m_usResourcePortMin);
//		file.SetIniInt("Resources", "MaxPort", direct.m_settings.m_usResourcePortMax);

//		file.SetIniInt("Processes", "MinPort", direct.m_settings.m_usProcessPortMin);
//		file.SetIniInt("Processes", "MaxPort", direct.m_settings.m_usResourcePortMin);

		file.SetIniInt("Messager", "UseEmail", direct.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", direct.m_settings.m_bUseNetwork?1:0);
//		file.SetIniInt("Messager", "LogRemote", direct.m_settings.m_bUseLogRemote?1:0);

		// backup
		file.SetIniInt("Mode", "UseClone", direct.m_settings.m_bUseClone?1:0);
		file.SetIniInt("Mode", "InitClone", direct.m_settings.m_bInitClone?1:0); 
		file.SetIniString("Clone", "Address", direct.m_settings.m_pszCloneIP);
		file.SetIniInt("Clone", "CloneListenPort", direct.m_settings.m_usCloneCommandPort); 

		// Database settings
		file.SetIniInt("Database", "UseBackup", direct.m_settings.m_bUseBackupDB?1:0);	 // make calls to both pri and backup DBs
		file.SetIniString("Database", "PrimaryDSN", direct.m_settings.m_pszPrimaryDSN);						// DSN
		file.SetIniString("Database", "PrimaryDBUser", direct.m_settings.m_pszPrimaryDBUser);				// user
		file.SetIniString("Database", "PrimaryDBPassword", direct.m_settings.m_pszPrimaryDBPassword);		// pw
		file.SetIniString("Database", "BackupDSN", direct.m_settings.m_pszBackupDSN);						// DSN
		file.SetIniString("Database", "BackupDBUser", direct.m_settings.m_pszBackupDBUser);					// user
		file.SetIniString("Database", "BackupDBPassword", direct.m_settings.m_pszBackupDBPassword);			// pw
		file.SetIniString("Database", "DestinationsTable", direct.m_settings.m_pszTableDest);		// destination table name - info about graphics boxes and mapping
		file.SetIniString("Database", "MetadataTable", direct.m_settings.m_pszTableMeta);		// metadata table name - info about graphics
		file.SetIniString("Database", "ExchangeTable", direct.m_settings.m_pszTableExchange);		// exchange flag table - application info
		file.SetIniString("Database", "ChannelsTable", direct.m_settings.m_pszTableChannels);		// channels table -  info about channel options
		file.SetIniString("Database", "EventsTable", direct.m_settings.m_pszTableEvents);		// events table name - info about event status
		file.SetIniString("Database", "MessagesTable", direct.m_settings.m_pszTableMessages);		// errors/messages table name

		// Automation settings
		file.SetIniInt("Automation", "NumServers", direct.m_settings.m_usNumColossus);   // number of Colossus Adaptors

		if(pszParams) {	free(pszParams);	pszParams=NULL;	}

		if(	direct.m_settings.m_usNumColossus == 0)
		{
			// write out an invalid sample just to have a key there to copy and manipulate

			i=999;
			sprintf(buffer, "Server_%03d_Address", i);
			file.SetIniString("Automation", buffer, "0.0.0.0");
			sprintf(buffer, "Server_%03d_Description", i);
			file.SetIniString("Automation", buffer, "Sample server entry for key value formats");
			sprintf(buffer, "Server_%03d_DebugFilename", i);
			file.SetIniString("Automation", buffer, "sample_debug_file.log");
			sprintf(buffer, "Server_%03d_CommLogFilename", i);
			file.SetIniString("Automation", buffer, "sample_comm_log_file.log");

		}
		else
		{
			i=0;
			while(i<direct.m_settings.m_usNumColossus)
			{
				if((direct.m_settings.m_ppszServerAddress)&&(direct.m_settings.m_ppszServerAddress[i]))
				{
					sprintf(buffer, "Server_%03d_Address", i);
					file.SetIniString("Automation", buffer, direct.m_settings.m_ppszServerAddress[i]);
				}

				if((direct.m_settings.m_ppszServerName)&&(direct.m_settings.m_ppszServerName[i]))
				{
					sprintf(buffer, "Server_%03d_Description", i);
					file.SetIniString("Automation", buffer, direct.m_settings.m_ppszServerName[i]);
				}

				if((direct.m_settings.m_ppszDebugFile)&&(direct.m_settings.m_ppszDebugFile[i]))
				{
					sprintf(buffer, "Server_%03d_DebugFilename", i);
					file.SetIniString("Automation", buffer, direct.m_settings.m_ppszDebugFile[i]);
				}

				if((direct.m_settings.m_ppszCommFile)&&(direct.m_settings.m_ppszCommFile[i]))
				{
					sprintf(buffer, "Server_%03d_CommLogFilename", i);
					file.SetIniString("Automation", buffer, direct.m_settings.m_ppszCommFile[i]);
				}

				i++;
			}
		}

	//imagestore specific 
		file.SetIniString("Miranda_Imagestore", "NullFile", direct.m_settings.m_pszNullFile);		// the filename of the nullfile - must be in the system folder
		file.SetIniInt("Miranda_Imagestore", "UseNullFile", direct.m_settings.m_bUseNullFile?1:0);

		// App settings (process specific)
		file.SetIniInt("Application", "ProcessIntervalMS",		direct.m_settings.m_ulProcessIntervalMS);		// interval on which to run the process, in milliseconds
		file.SetIniInt("Application", "ClonePingIntervalMS",	direct.m_settings.m_ulClonePingIntervalMS);  // interval on which the clone pings, or which to expect the clone to ping, in milliseconds
		file.SetIniInt("Application", "WatchIntervalMS",			direct.m_settings.m_ulWatchIntervalMS);			// interval on which to check the watch folder, in milliseconds
		file.SetIniInt("Application", "WarnPercentageDiskFull", direct.m_settings.m_ucWarnPercentageDiskFull);   // send a warning when this % of storage is exceeded
		file.SetIniInt("Application", "DeletePercentageDiskFull", direct.m_settings.m_ucDeletePercentageDiskFull);   // start deleting files if this % is exceeded

	// watchfolder settings.
		file.SetIniString("WatchFolder", "WatchPath", direct.m_settings.m_pszWatchFolderPath?direct.m_settings.m_pszWatchFolderPath:"X:\\watchfolder");									// the path of the watchfolder // a mapped drive
		file.SetIniString("WatchFolder", "PrimaryDestinationPath", direct.m_settings.m_pszPrimaryDestinationFolderPath?direct.m_settings.m_pszPrimaryDestinationFolderPath:"C:\\assets");		// the path of the primary destination folder // local
		file.SetIniString("WatchFolder", "BackupDestinationPath", direct.m_settings.m_pszBackupDestinationFolderPath?direct.m_settings.m_pszBackupDestinationFolderPath:"D:\\assets");			// the path of the backup destination folder // usually, a mapped drive.

		
// system folder
		file.SetIniString("SystemFolder", "SystemFolderPath", direct.m_settings.m_pszSystemFolderPath?direct.m_settings.m_pszSystemFolderPath:"C:\\system");	// the path of the folder used for parse files etc.


		file.SetSettings(pszFilename, false);  // have to have correct filename
	

		if(pszParams) 
		{
			free(pszParams);
			pszParams=NULL;
		}
	}


//init command and status listeners.

	if(direct.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = direct.m_settings.m_usCommandPort;
//		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.
		// this one is going to use non -persistent connection.
		pServer->m_lpfnHandler = DirectServerHandlerThread;
		pServer->m_lpObject = &direct;
		pServer->m_lpMsgObj = &direct.m_net; // any will do,

		
		if(direct.m_net.StartServer(pServer, &direct.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
//			if(pwndStatus) pwndStatus->SetWindowText(errorstring);
//			StatusMessage(plist, errorstring);
			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct", errorstring);  //(Dispatch message)
		}
		else
		{
//			StatusMessage(plist, "Command server listening on %d",direct.m_settings.m_usCommandPort);
			direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", "Command server listening on %d",direct.m_settings.m_usCommandPort);  //(Dispatch message)
		}
	}

//	if(pwndStatus) pwndStatus->SetWindowText("initializing status server...");
//	StatusMessage(plist, "Initializing status server on %d",direct.m_settings.m_usStatusPort);

	if(direct.m_settings.m_usStatusPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = direct.m_settings.m_usStatusPort;
//		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.
		// this one is going to use non -persistent connection.
		pServer->m_lpfnHandler = DirectServerHandlerThread;
		pServer->m_lpObject = &direct;
		pServer->m_lpMsgObj = &direct.m_net; // any will do,

		if(direct.m_net.StartServer(pServer, &direct.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
//			if(pwndStatus) pwndStatus->SetWindowText(errorstring);
//			StatusMessage(plist, errorstring);
			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct", errorstring);  //(Dispatch message)
		}
		else
		{
//			StatusMessage(plist, "Status server listening on %d",direct.m_settings.m_usStatusPort);
			direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", "Status server listening on %d",direct.m_settings.m_usStatusPort);  //(Dispatch message)
		}
	}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "starting loop");  //(Dispatch message)


	_timeb timestamp;
	_timeb watchtime;
	_timeb clonetime;
	_timeb maintime;
	_ftime( &timestamp );
	_ftime( &watchtime );
	_ftime( &clonetime );
	_ftime( &maintime );

	clonetime.time = timestamp.time + 120; // lets give it two minutes before it ever starts looking for a clone. 
	// we want to make sure the clone has time to initialize

	char dberrorstring[DB_ERRORSTRING_LEN];
//	char neterrorstring[NET_ERRORSTRING_LEN];
	char szSQL[DB_SQLSTRING_MAXLEN];

	while(!g_bKillThread)
	{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "start loop");  //(Dispatch message)
//Sleep(200);
		// main working loop.
		_ftime( &timestamp );
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "after timestamp");  //(Dispatch message)
//Sleep(200);

		//////////////////////////////////////////////////////////////
		// The general playout process (not used if a clone)
		//////////////////////////////////////////////////////////////
		// If not connected to the database, do so.
		// Check DB flag for BACKUP.  If there, the other one has it, just go into clone mode.
		// If not connected to Omnibus Colossus Adaptor (sic), do so.
		// Get all the lists on all the connections, if init, build the event lists.
		// if not init, update lists on changes.
		// Get the list of destination IP addresses form db
		// On an interval, ping each one in some way and report failures
		// Make sure all the media that is coming up in the playlists are on the boxes
		// If not, check drive space on each box, check file attribs from the file server
		// if the file is not there, report.
		// if there's enough space (with % margin), transfer it over.
		// if not, check metadata in DB for most appropriate file to delete from box (usage statistics)
		// repeat process until there is enough drive space and transfer.
		// Get a ping from a clone, if set up to do so, reply and clear any DB flags.
		// If no ping, but all else is OK, set up a DB flag that says MAIN.
		// Repeat whole process on an interval.
		
		//////////////////////////////////////////////////////////////
		// The general clone process 
		//////////////////////////////////////////////////////////////
		// Ping the other Direct process every interval (settable)
		// if there, fine, just listen for any commands and if a client
		// tries to connect, send it a redirect command to the other process.
		// if it's not there, retry 3 times.
		// if still not there, connect to DB and check flag.  If flag is not there,
		// we assume main process died.  We set DB flag to BACKUP.
		// start general playout process as above.
		
		//////////////////////////////////////////////////////////////
		// The general watchfolder process
		//////////////////////////////////////////////////////////////
		// check queue for any changes
		// if there's an addition, grab the file path in the inbox, and create
		// the same folder if necessary, copy the file there, delete out of the inbox
		// (dont do a move, in case theres a prob transferring)
		// once the file is on the file server, update the metadata in the database
		

		// ok, here we go:
		//////////////////////////////////////////////////////////////
		// The general playout process (not used if a clone)
		//////////////////////////////////////////////////////////////

// we need to monitor any live events in the queue for sudden changes in omnibus status.
// we only need to bother with changes if there is a difference.
		CDBRecord* pdbrLiveEventData=NULL;
		unsigned long ulNumLiveEventRecords=0;
		int nLiveEventTableIndex = -1;

		if(direct.m_omni.m_usNumConn>0)
		{
			unsigned short i=0;
			unsigned long ulStatusCounter=0;
			while (i<direct.m_omni.m_usNumConn)
			{
				if((direct.m_omni.m_ppConn)&&(direct.m_omni.m_ppConn[i]))
				{
EnterCriticalSection(&direct.m_omni.m_ppConn[i]->m_crit);

					ulStatusCounter+=direct.m_omni.m_ppConn[i]->m_ulCounter;

					unsigned long ulLocalCounter=direct.m_omni.m_ppConn[i]->m_ulCounter;

					if( direct.m_omni.m_ppConn[i]->m_ulRefTick != ulLocalCounter)
					{
						// something has changed.
						// we have to go thru the omni items to check to
						// see if something has played or has finished playing.
						// if so, we must update the DBs.

						unsigned short j=0;

						while(j<direct.m_omni.m_ppConn[i]->GetListCount())
						{
							CCAList* pList = direct.m_omni.m_ppConn[i]->GetList(j);
							if(pList)
							{

								unsigned long ulListCounter=pList->m_ulCounter;

								if(pList->m_ulRefTick!=ulListCounter)
								{
									// there has been a change.
									unsigned short usChannelID = pList->m_usListID;
									pdbrLiveEventData=NULL;
									ulNumLiveEventRecords=0;

									// save up the IP-specific fields.
									char szIP[20];
									strcpy(szIP,"");
									unsigned long ulExpireDate = 0;


									if(direct.m_data.m_pdbConnPrimary)
									{
										nLiveEventTableIndex = direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableEvents);
										if(nLiveEventTableIndex>=0)
										{
											CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

											// get them all
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
												"SELECT * FROM %s WHERE channel = %d ORDER BY on_air_time", 
												direct.m_settings.m_pszTableEvents,
												usChannelID
												);

											//Sleep(500);
											CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
											if(prs == NULL) 
											{
												direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetLiveEvents", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
											}
											else
											{
												// check the items.
						//				AfxMessageBox("here");
												int x = direct.m_db.Convert(prs, pdbcCriteria, &pdbrLiveEventData, &ulNumLiveEventRecords, dberrorstring);
						//				AfxMessageBox("here2");
												if(x != DB_SUCCESS) 
												{
													if(prs)
													{
														prs->Close();
														delete prs;
													}

													direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetLiveEvents", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
												}
											}
											if(pdbcCriteria) delete pdbcCriteria;
										}
										// go by db events, they are time sorted.
										if((pdbrLiveEventData)&&(ulNumLiveEventRecords))
										{
											unsigned long ulCheckingIndex = 0;
											while(ulCheckingIndex<ulNumLiveEventRecords)
											{
												if((pdbrLiveEventData[ulCheckingIndex].m_ppszData)
													&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID])
													&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS]))
												{
													int n = pList->GetEventIndex(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID]);
													if(n>=0)
													{
														CCAEvent* pEvent = pList->GetEvent(n);
														if((pEvent)&&(pEvent->m_usStatus >= C_ON_AIR))
														{
															// check the DB status.
															unsigned long ulStatus = atoi(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS]);
															if(!(ulStatus&DIRECT_LESTATUS_PLAYED))
															{
																// we have to update this record, and also the record for the file.

																ulStatus|=DIRECT_LESTATUS_PLAYED;

																// save the values for the ip specific
																if(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_IP]) 
																	strcpy(szIP, pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_IP]);

/*
								// this record
								if(nLiveEventTableIndex>=0)
								{
									CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);
									CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

									// load up the data.

									int i=0;
									for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
									{
										pdbcCriteria->m_pdbField[i].m_pszData = pdbrLiveEventData[ulCheckingIndex].m_ppszData[i];
										// we are just adding the XFER thing.
										if(i==DIRECT_LEFIELD_STATUS) // field 4 is status
										{
											pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
											if(pdbcValues->m_pdbField[i].m_pszData)
												sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d", ulStatus );
										}
										else
										{
											pdbcValues->m_pdbField[i].m_pszData = pdbrLiveEventData[ulCheckingIndex].m_ppszData[i];
										}
									}

									if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
									// null out the fields so as not to free the records
									for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
									{
										pdbcCriteria->m_pdbField[i].m_pszData = NULL;
										if(i!=DIRECT_LEFIELD_STATUS) // field 4 is status, dont NULL it, it will be destroyed on delete.
										{
											pdbcValues->m_pdbField[i].m_pszData = NULL;
										}
									}
									if(pdbcValues) delete pdbcValues;
									if(pdbcCriteria) delete pdbcCriteria;
								}  //if(nLiveEventTableIndex>=0)


*/
								if(nLiveEventTableIndex>=0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d, ip ='%s' WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										szIP,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
/*
									CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);
									CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

									// load up the data.
									int i=0;
									for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
									{
										pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
										if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
										{
											pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
											if(pdbcCriteria->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
											pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
										}
										// we are just adding the xfer thing.
										if(i==DIRECT_LEFIELD_STATUS) // field 4 is status
										{
											pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
											if(pdbcValues->m_pdbField[i].m_pszData)
												sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d",ulStatus );
										}
										else
										{
											pdbcValues->m_pdbField[i].m_ucType=DB_OP_IGNORE;
											if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
											{
												pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
												if(pdbcValues->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
												pdbcValues->m_pdbField[i].m_ucType=DB_OP_EQUAL;
											}
										}
									}

									if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
									if(pdbcValues) delete pdbcValues;
									if(pdbcCriteria) delete pdbcCriteria;
									*/
								}

								// now the file record(s).
								//we can find the list of files for this event in the DIRECT_LEFIELD_FILES field

								if(
									  (pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_FILES])
									&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_FILES]))
									)
								{
									CSafeBufferUtil sbu;
									char* pchFile = NULL;
									pchFile	= sbu.Token(
																			pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_FILES],
																			strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_FILES]), 
																			"|", 
																			MODE_MULTIDELIM
																			);
									while(pchFile!=NULL)
									{
										// deal with filename

										// find this file in the Db and update the record.
//	char szSQL[DB_SQLSTRING_MAXLEN];

										if(strlen(pchFile))
										{
											if(direct.m_data.m_pdbConnPrimary)
											{
												CDBRecord* pdbrData=NULL;
												unsigned long ulNumRecords=0;

												int nMetadataTableIndex 
													= direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMeta);
												if(nMetadataTableIndex>=0)
												{
													CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nMetadataTableIndex);

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE filename LIKE '%s'", 
														direct.m_settings.m_pszTableMeta,
														pchFile
														);

													//Sleep(500);
													CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
													if(prs == NULL) 
													{
														direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:FindFile", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
													}
													else
													{
														// check the items.
									//				AfxMessageBox("here");
														int x = direct.m_db.Convert(prs, pdbcCriteria, &pdbrData, &ulNumRecords, dberrorstring);
									//				AfxMessageBox("here2");
														if(x != DB_SUCCESS) 
														{
															if(prs)
															{
																prs->Close();
																delete prs;
															}

															direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:FindFile", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
														}
														

													}
													if(pdbcCriteria) delete pdbcCriteria;
													pdbcCriteria = NULL;


													if((pdbrData)&&(ulNumRecords>0))
													{
														// now update the record.
														CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nMetadataTableIndex);
														pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nMetadataTableIndex);

														// load up the data.

														if(pdbrData[0].m_ppszData[DIRECT_MFIELD_EXPIRE]) 
														ulExpireDate = atol(pdbrData[0].m_ppszData[DIRECT_MFIELD_EXPIRE]);

														unsigned long i=0;
														for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nMetadataTableIndex].m_usNumFields; i++)
														{
															pdbcCriteria->m_pdbField[i].m_pszData = pdbrData[0].m_ppszData[i];
															// we are just adding the XFER thing.
															if(i==DIRECT_MFIELD_SYS_LAST) // field 10 is last used
															{
																pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
																if(pdbcValues->m_pdbField[i].m_pszData)
																{
																	_timeb timestamp;
																	_ftime( &timestamp );
																	
																	sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d", (unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) )); //local
																}
															}
															else
															if(i==DIRECT_MFIELD_SYS_TIMES) // field 11 is number of times used
															{
																pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
																if(pdbcValues->m_pdbField[i].m_pszData)
																	sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d", atol(pdbrData[0].m_ppszData[i])+1 );
															}
															else
															{
																pdbcValues->m_pdbField[i].m_pszData = pdbrData[0].m_ppszData[i];
															}
														}

														if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
														{
															direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateMetadata", "Update returned an error.\n%s", dberrorstring);  //(Dispatch message)
														}
														// null out the fields so as not to free the records
														for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nMetadataTableIndex].m_usNumFields; i++)
														{
															pdbcCriteria->m_pdbField[i].m_pszData = NULL;
															if(
																  (i!=DIRECT_MFIELD_SYS_LAST)
																||(i!=DIRECT_MFIELD_SYS_TIMES)
																)// dont NULL it, it will be destroyed on delete.
															{
																pdbcValues->m_pdbField[i].m_pszData = NULL;
															}
														}
														if(pdbcCriteria) delete pdbcCriteria;
														if(pdbcValues) delete pdbcValues;

														// now free the retrieved data
														i=0;
														while(i<ulNumRecords)
														{
															pdbrData[i].Free();  // must free explicitly, per record
															i++;
														}
														delete [] pdbrData;
														pdbrData = NULL;


///// last used and num used and such must be updated on the IP-specific table.
														// but only once.
														// and only if there is an IP
//														pEvent = pList->GetEvent(n);
														if((pEvent)&&(pEvent->m_usStatus == C_ON_AIR)&&(strlen(szIP)))
														{
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [device_%s] WHERE filename LIKE '%s'", 
																szIP,
																pchFile
																);

															//Sleep(500);
															// this retrieve will fail if there is no record or table
															CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
															if(prs != NULL) 
															{
																// update.
																if(!prs->IsEOF())
																{
																	
																	// there is a record.

																	prs->Close();
																	delete prs;
																	prs = NULL;


																	CString szValue;
																	prs->GetFieldValue("sys_times_used", szValue);
																	unsigned long ulTimesUsed = atol(szValue);
																	ulTimesUsed++;

																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
																		"UPDATE [device_%s] SET sys_times_used = %d, sys_last_used = %d, expires_after = %d WHERE filename LIKE '%s'", 
																		szIP,
																		ulTimesUsed,
																		(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)),
																		ulExpireDate,
																		pchFile
																		);
																	if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
																	{
															//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
																	}
																}
																if(prs)
																{
																	prs->Close();
																	delete prs;
																}
															}
														}
													}
												}//  if(nMetadataTableIndex>=0)




											}// if(direct.m_data.m_pdbConnPrimary)
										}//if(strlen(pchFile))



										pchFile	= sbu.Token(
																				NULL,NULL,
																				"|", 
																				MODE_MULTIDELIM
																				);

									}
								}

															}
														}
													}
												}

												// have to free
												pdbrLiveEventData[ulCheckingIndex].Free();  // must free explicitly, per record
												ulCheckingIndex++;
											}
											delete [] pdbrLiveEventData;
											pdbrLiveEventData = NULL;
										}
									}
									pList->m_ulRefTick = ulListCounter;
								}
							}
							j++;

						}

						direct.m_omni.m_ppConn[i]->m_ulRefTick = ulLocalCounter; // not the counter AFTER the parsing, have to make sure we get all changes.
LeaveCriticalSection(&direct.m_omni.m_ppConn[i]->m_crit);
					}
				}
				i++;
			}
			
			if(direct.m_data.m_ulStatusCounter!=ulStatusCounter)
			{
				direct.m_data.m_ulStatusCounter = ulStatusCounter;

				// update the exchange database
				if(direct.m_data.m_pdbConnPrimary)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s SET mod = %d WHERE criterion LIKE 'status_live'", 
						direct.m_settings.m_pszTableExchange,
						ulStatusCounter
						);
					if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
					{
			//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
					}
				}
			}
		}


		if( //(0)&&
				(!direct.m_data.m_bCloneMode)
			&&(
					(timestamp.time > maintime.time)
				||((timestamp.time == maintime.time)&&(timestamp.millitm >= maintime.millitm))
				)
			)
		{
			maintime.time = timestamp.time + direct.m_settings.m_ulProcessIntervalMS/1000; 
			maintime.millitm = timestamp.millitm + (unsigned short)(direct.m_settings.m_ulProcessIntervalMS%1000); // fractional second updates
			if(maintime.millitm>999)
			{
				maintime.time++;
				maintime.millitm%=1000;
			}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "process loop");  //(Dispatch message)
//Sleep(200);

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "connecting to db");  //(Dispatch message)
			// If not connected to the database, do so.
			if(direct.m_data.m_pdbConnPrimary==NULL)
			{
//	AfxMessageBox("X");
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "direct.m_db.CreateNewConnection");  //(Dispatch message)

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "connecting to [%s] [%s] [%s]", direct.m_settings.m_pszPrimaryDSN, direct.m_settings.m_pszPrimaryDBUser, direct.m_settings.m_pszPrimaryDBPassword);  //(Dispatch message)
				direct.m_msgr.DM(MSG_ICONHAND, NULL, "Direct", "Connecting to [%s]", direct.m_settings.m_pszPrimaryDSN);  //(Dispatch message)

				direct.m_data.m_pdbConnPrimary = direct.m_db.CreateNewConnection(
																			direct.m_settings.m_pszPrimaryDSN, 
																			direct.m_settings.m_pszPrimaryDBUser, 
																			direct.m_settings.m_pszPrimaryDBPassword
																		);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "after CreateNewConnection to [%s] [%s] [%s]",direct.m_settings.m_pszPrimaryDSN, direct.m_settings.m_pszPrimaryDBUser, direct.m_settings.m_pszPrimaryDBPassword);  //(Dispatch message)
				if(direct.m_data.m_pdbConnPrimary)
				{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "not null connection");  //(Dispatch message)
					if(direct.m_db.ConnectDatabase(direct.m_data.m_pdbConnPrimary, dberrorstring) == DB_SUCCESS)
					{
						direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", "Connected to %s (primary database)", direct.m_settings.m_pszPrimaryDSN);  //(Dispatch message)

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "adding first table");  //(Dispatch message)
						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableDest); // add destination table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableDest)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableDest);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableDest, 
								direct.m_data.m_pdbConnPrimary->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableDest)].m_usNumFields);  //(Dispatch message)

						}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "adding second table");  //(Dispatch message)
						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMeta); // add metadata table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMeta)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMeta);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableMeta, 
								direct.m_data.m_pdbConnPrimary->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMeta)].m_usNumFields);  //(Dispatch message)

						}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "adding third table");  //(Dispatch message)
						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableExchange); // add exchange table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableExchange)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableExchange);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableExchange, 
								direct.m_data.m_pdbConnPrimary->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableExchange)].m_usNumFields);  //(Dispatch message)

						}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "adding fourth table");  //(Dispatch message)
						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableChannels); // add channels table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableChannels)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableChannels);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableChannels, 
								direct.m_data.m_pdbConnPrimary->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableChannels)].m_usNumFields);  //(Dispatch message)

						}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "adding fifth table");  //(Dispatch message)
						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableEvents); // add events table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableEvents)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableEvents);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableEvents, 
								direct.m_data.m_pdbConnPrimary->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableEvents)].m_usNumFields);  //(Dispatch message)

						}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "adding sixth table");  //(Dispatch message)
						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMessages); // add messages table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMessages)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMessages);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableMessages, 
								direct.m_data.m_pdbConnPrimary->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMessages)].m_usNumFields);  //(Dispatch message)

						}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "finished with db");  //(Dispatch message)
/*
// we will just set up the DBs manually.  for now anyway.
						else
						{
							// create the table for the first time.

							CDBfield* pfld = new CDBfield[DB_NUMFIELDS];
							pfld[0].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_SYM)+1);
							if(pfld[0].m_pszData) strcpy(pfld[0].m_pszData, DB_FIELDNAME_SYM);
							pfld[0].m_ucType = DB_TYPE_VSTR;
							pfld[0].m_usSize = DB_FIELDSIZE_SYM;
							
							pfld[1].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_NAME)+1);
							if(pfld[1].m_pszData) strcpy(pfld[1].m_pszData, DB_FIELDNAME_NAME);
							pfld[1].m_ucType = DB_TYPE_VSTR;
							pfld[1].m_usSize = DB_FIELDSIZE_NAME;

							pfld[2].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_CAT)+1);
							if(pfld[2].m_pszData) strcpy(pfld[2].m_pszData, DB_FIELDNAME_CAT);
							pfld[2].m_ucType = DB_TYPE_VSTR;
							pfld[2].m_usSize = DB_FIELDSIZE_CAT;

							pfld[3].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_EXCH)+1);
							if(pfld[3].m_pszData) strcpy(pfld[3].m_pszData, DB_FIELDNAME_EXCH);
							pfld[3].m_ucType = DB_TYPE_BYTE;
							pfld[3].m_usSize = 1;

							pfld[4].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_TYPE)+1);
							if(pfld[4].m_pszData) strcpy(pfld[4].m_pszData, DB_FIELDNAME_TYPE);
							pfld[4].m_ucType = DB_TYPE_BYTE;
							pfld[4].m_usSize = 1;


							char table[DB_ERRORSTRING_LEN];

							int x = g_db.SetTableInfo(pConn, g_db.GetTableIndex(pConn, pchTable), pchTable, pfld, DB_NUMFIELDS);
							if(x!=DB_SUCCESS)
							{
								sprintf(table, "Table [%s] info not set. %d", pchTable, x);
								AfxMessageBox(table);	
							}	

						
		//				sprintf(errorstring, "numtables %d", pConn->m_usNumTables);
		//				AfxMessageBox(errorstring);	

							g_db.CreateTable(pConn, g_db.GetTableIndex(pConn, pchTable), errorstring);
							if(x!=DB_SUCCESS)
							{
								sprintf(table, "CreateTable returned %d\n%s", x, errorstring);
								AfxMessageBox(table);	
							}	
						}
						*/
					}
					else
					{
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct", "Could not connect to %s (primary database)", direct.m_settings.m_pszPrimaryDSN?direct.m_settings.m_pszPrimaryDSN:"");  //(Dispatch message)
					}
				}
			}

			// If not connected to the backup database, do so.
			if((direct.m_data.m_pdbConnBackup==NULL)&&(direct.m_settings.m_bUseBackupDB))
			{
				Sleep(250); //a pause
				direct.m_msgr.DM(MSG_ICONHAND, NULL, "Direct", "Connecting to [%s]", direct.m_settings.m_pszBackupDSN);  //(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "connecting to backup db");  //(Dispatch message)
				direct.m_data.m_pdbConnBackup	= direct.m_db.CreateNewConnection(
																			direct.m_settings.m_pszBackupDSN, 
																			direct.m_settings.m_pszBackupDBUser, 
																			direct.m_settings.m_pszBackupDBPassword
																		);
				if(direct.m_data.m_pdbConnBackup)
				{
					if(direct.m_db.ConnectDatabase(direct.m_data.m_pdbConnBackup, dberrorstring) == DB_SUCCESS)
					{
						direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", "Connected to %s (backup database)", direct.m_settings.m_pszBackupDSN);  //(Dispatch message)

						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableDest); // add destination table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableDest)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableDest);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableDest, 
								direct.m_data.m_pdbConnBackup->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableDest)].m_usNumFields);  //(Dispatch message)

						}

						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMeta); // add metadata table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMeta)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMeta);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableMeta, 
								direct.m_data.m_pdbConnBackup->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMeta)].m_usNumFields);  //(Dispatch message)

						}

						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableExchange); // add exchange table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableExchange)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableExchange);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableExchange, 
								direct.m_data.m_pdbConnBackup->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableExchange)].m_usNumFields);  //(Dispatch message)

						}

						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableChannels); // add channels table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableChannels)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableChannels);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableChannels, 
								direct.m_data.m_pdbConnBackup->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableChannels)].m_usNumFields);  //(Dispatch message)

						}

						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableEvents); // add events table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableEvents)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableEvents);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableEvents, 
								direct.m_data.m_pdbConnBackup->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableEvents)].m_usNumFields);  //(Dispatch message)

						}

						// create tables if they dont exist.
						direct.m_db.AddTable(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMessages); // add errors table to schema
						if(direct.m_db.TableExists(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMessages)==DB_EXISTS)
						{
							// get the schema
							direct.m_db.GetTableInfo(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMessages);
							direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
								"Table [%s] exists with %d fields", 
								direct.m_settings.m_pszTableMessages, 
								direct.m_data.m_pdbConnBackup->m_pdbTable[direct.m_db.GetTableIndex(direct.m_data.m_pdbConnBackup, direct.m_settings.m_pszTableMessages)].m_usNumFields);  //(Dispatch message)

						}



/*
// we will just set up the DBs manually.  for now anyway.
						else
						{
							// create the table for the first time.

							CDBfield* pfld = new CDBfield[DB_NUMFIELDS];
							pfld[0].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_SYM)+1);
							if(pfld[0].m_pszData) strcpy(pfld[0].m_pszData, DB_FIELDNAME_SYM);
							pfld[0].m_ucType = DB_TYPE_VSTR;
							pfld[0].m_usSize = DB_FIELDSIZE_SYM;
							
							pfld[1].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_NAME)+1);
							if(pfld[1].m_pszData) strcpy(pfld[1].m_pszData, DB_FIELDNAME_NAME);
							pfld[1].m_ucType = DB_TYPE_VSTR;
							pfld[1].m_usSize = DB_FIELDSIZE_NAME;

							pfld[2].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_CAT)+1);
							if(pfld[2].m_pszData) strcpy(pfld[2].m_pszData, DB_FIELDNAME_CAT);
							pfld[2].m_ucType = DB_TYPE_VSTR;
							pfld[2].m_usSize = DB_FIELDSIZE_CAT;

							pfld[3].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_EXCH)+1);
							if(pfld[3].m_pszData) strcpy(pfld[3].m_pszData, DB_FIELDNAME_EXCH);
							pfld[3].m_ucType = DB_TYPE_BYTE;
							pfld[3].m_usSize = 1;

							pfld[4].m_pszData = (char*)malloc(strlen(DB_FIELDNAME_TYPE)+1);
							if(pfld[4].m_pszData) strcpy(pfld[4].m_pszData, DB_FIELDNAME_TYPE);
							pfld[4].m_ucType = DB_TYPE_BYTE;
							pfld[4].m_usSize = 1;


							char table[DB_ERRORSTRING_LEN];

							int x = g_db.SetTableInfo(pConn, g_db.GetTableIndex(pConn, pchTable), pchTable, pfld, DB_NUMFIELDS);
							if(x!=DB_SUCCESS)
							{
								sprintf(table, "Table [%s] info not set. %d", pchTable, x);
								AfxMessageBox(table);	
							}	

						
		//				sprintf(errorstring, "numtables %d", pConn->m_usNumTables);
		//				AfxMessageBox(errorstring);	

							g_db.CreateTable(pConn, g_db.GetTableIndex(pConn, pchTable), errorstring);
							if(x!=DB_SUCCESS)
							{
								sprintf(table, "CreateTable returned %d\n%s", x, errorstring);
								AfxMessageBox(table);	
							}	
						}
						*/
					}
					else
					{
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct", "Could not connect to %s (backup database)", direct.m_settings.m_pszBackupDSN?direct.m_settings.m_pszBackupDSN:"");  //(Dispatch message)
					}
				}
			}

			// Check DB flag for BACKUP.  If there, the other one has it, just go into clone mode.

//TODO

//AfxMessageBox("x");
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before get colossus connect");  //(Dispatch message)
//Sleep(200);

			// If not connected to Omnibus Colossus Adaptor (sic), do so.
			if(direct.m_omni.m_usNumConn<direct.m_settings.m_usNumColossus)
			{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "the number of connections %d differs from the number of connections expected %d",
//								 direct.m_omni.m_usNumConn,direct.m_settings.m_usNumColossus);  //(Dispatch message)
//Sleep(200);
				unsigned short i=0;
				while (i<direct.m_settings.m_usNumColossus)
				{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "connection %d < settings %d", i, direct.m_settings.m_usNumColossus);  //(Dispatch message)
//Sleep(200);
					if((direct.m_settings.m_ppszServerAddress)&&(direct.m_settings.m_ppszServerAddress[i]))			// array of Colossus Adaptor IP (port is 10540)
					{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking if a connection exists already");  //(Dispatch message)
//Sleep(200);
						if(direct.m_omni.ConnectionExists(direct.m_settings.m_ppszServerAddress[i])==NULL)
						{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "no previous connection, calling connect");  //(Dispatch message)
//Sleep(200);
							// connect
							CCAConn*	pomniConn = direct.m_omni.ConnectServer(direct.m_settings.m_ppszServerAddress[i], 
								10540,
								((direct.m_settings.m_ppszServerName)&&(direct.m_settings.m_ppszServerName[i]))?direct.m_settings.m_ppszServerName[i]:NULL
								);
							if(pomniConn!=NULL)
							{
								if((direct.m_settings.m_ppszDebugFile)&&(direct.m_settings.m_ppszDebugFile[i]))
								{
									pomniConn->m_pszDebugFile = (char*) malloc(strlen(direct.m_settings.m_ppszDebugFile[i])+1);		// debug file
									if(pomniConn->m_pszDebugFile) strcpy(pomniConn->m_pszDebugFile, direct.m_settings.m_ppszDebugFile[i]);

								}
								if((direct.m_settings.m_ppszCommFile)&&(direct.m_settings.m_ppszCommFile[i]))	
								{
									pomniConn->m_pszCommFile = (char*) malloc(strlen(direct.m_settings.m_ppszCommFile[i])+1);		// comm log file
									if(pomniConn->m_pszCommFile) strcpy(pomniConn->m_pszCommFile, direct.m_settings.m_ppszCommFile[i]);

								}
								direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", 
									"Connected to Colossus Adaptor [%s] on %s", direct.m_settings.m_ppszServerName[i], direct.m_settings.m_ppszServerAddress[i]);
							}
							else
							{
								direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct", 
									"Error connecting to Colossus Adaptor [%s] on %s", direct.m_settings.m_ppszServerName[i], direct.m_settings.m_ppszServerAddress[i]);

							}
						}
					}

					i++;
				}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "sleeping for 10 seconds");  //(Dispatch message)
				for(int ds=0;ds<100;ds++)
				{
//					direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "sleeping... %d milliseconds so far", ds*100);			
					Sleep(100);  // give some time for the connections to fill up with event data.
				}
			}


			// Get all the lists on all the connections, if init, build the event lists.
			// connecting, above, does this.


			// if not init, update lists on changes.
			// maintaining the connection, in the main thread of the omnibus object, does this.
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to get destinations");  //(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before get destinations");  //(Dispatch message)
//Sleep(200);

			// Get the list of destination IP addresses from db 
			// we do this every time, in case there is a change.
			if(direct.m_data.m_pdbConnPrimary!=NULL)
			{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "get destinations 01");  //(Dispatch message)
//Sleep(200);
				int nindex = direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableDest);
				if(nindex>=0)
				{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "get destinations 02");  //(Dispatch message)
//Sleep(200);
					CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nindex);

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ", direct.m_settings.m_pszTableDest);

					//Sleep(500);
					CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
					if(prs == NULL) 
					{
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetDestinations", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
					}
					else
					{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "get destinations 03");  //(Dispatch message)
//Sleep(200);
						// check the items.
						CDBRecord* pdbrData=NULL;
						unsigned long ulNumRecords=0;

//				AfxMessageBox("here");
						int x = direct.m_db.Convert(prs, pdbcCriteria, &pdbrData, &ulNumRecords, dberrorstring);
//				AfxMessageBox("here2");
						if(x != DB_SUCCESS) 
						{
							if(prs)
							{
								prs->Close();
								delete prs;
							}

							direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetDestinations", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
						}
						else
						{
							if((direct.m_data.m_ppdbDest==NULL)||(ulNumRecords!=direct.m_data.m_ulNumDest))
							{
								direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:GetDestinations", "%d items retrieved from %s", ulNumRecords, direct.m_settings.m_pszTableDest);  //(Dispatch message)

//								Sleep(500);
//								AfxMessageBox("X");
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d items retrieved from %s", ulNumRecords, direct.m_settings.m_pszTableDest);  //(Dispatch message)
//Sleep(200);
								if((ulNumRecords>0)&&(pdbrData))
								{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d items", ulNumRecords);  //(Dispatch message)
//Sleep(200);
									db_dest_t** ppdbDest = new db_dest_t*[ulNumRecords];
									if(ppdbDest)
									{
										int nrec=0;
										int nNumRecords = (int)ulNumRecords;
										while(nrec<nNumRecords)
										{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
//Sleep(100);
											ppdbDest[nrec] = NULL;
											if(pdbrData[nrec].m_ppszData)
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "there's data");  //(Dispatch message)
//Sleep(200);
												db_dest_t* pDest = new db_dest_t;
												if(pDest)
												{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "new object");  //(Dispatch message)
//Sleep(200);
													pDest->pszIP = NULL;
													pDest->pszExtensions = NULL;
													pDest->usChannelID = NULL;
													pDest->ulKeyLayers = NULL;
													pDest->ucDiskPercentage = 70;
													pDest->ucChecksum = 0;
													pDest->ulKBFree = 0;  //KB free on device
													pDest->ulKBTotal = 0;  //KB total on device
													pDest->bDiskChecked = false;

													unsigned short i=0;
													while((pdbrData[nrec].m_ppszData)&&(pdbrData[nrec].m_ppszData[i]!=NULL))
													{
//AfxMessageBox("record");   

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "processing field [%s]", pdbrData[nrec].m_ppszData[i]);  //(Dispatch message)
//Sleep(20);
														unsigned long ulLen = strlen(pdbrData[nrec].m_ppszData[i]);
														pDest->ucChecksum += direct.m_net.Checksum((unsigned char*) pdbrData[nrec].m_ppszData[i], ulLen);

														switch(i)
														{
														case 0:// ip address
															{
																pDest->pszIP = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																if(pDest->pszIP) strcpy(pDest->pszIP, pdbrData[nrec].m_ppszData[i]);
															} break;
														case 1: // keys
															{ 
															} break;
														case 2:// criteria
															{
																//parse the criteria string.
																// first get channel Id
																char* pch = strstr(pdbrData[nrec].m_ppszData[i], "<roID>");
																if(pch)
																{
																	pch+=strlen("<roID>nd0==");  //pretty specific, but we can make general later.
																	pDest->usChannelID = (unsigned short)atoi(pch);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Channel ID in db was %d (from atoi(%s))", pDest->usChannelID, pch);  //(Dispatch message)
//Sleep(100);
																}

																pch = strstr(pdbrData[nrec].m_ppszData[i], "<data>");
																if(pch)
																{
																	unsigned short ucKey = 0;
																	pch+=strlen("<data>d|2d:1");  //pretty specific, but we can make general later.
																	if(*pch == '<')  //less than or equal to
																	{
																		pch+=2;
																		ucKey = atoi(pch);

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Keyer layer in db was <= %d (from atoi(%s))", ucKey, pch);  //(Dispatch message)
//Sleep(100);
																		while(ucKey>0)
																		{
																			pDest->ulKeyLayers |= (1<<(ucKey-1));
																			ucKey--;
																		}

																	}
																	else //greater than or equal to
																	{
																		pch+=2;
																		ucKey = atoi(pch);

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Keyer layer in db was >= %d (from atoi(%s))", ucKey, pch);  //(Dispatch message)
//Sleep(100);
																		while(ucKey<=32)
																		{
																			pDest->ulKeyLayers |= (1<<(ucKey-1));
																			ucKey++;
																		}
																	}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Keyer layers 0x%08x", pDest->ulKeyLayers);  //(Dispatch message)
//Sleep(100);

																}
															} break;
														case 3: // diskspace
															{ 
																unsigned char ucDisk = (unsigned char)atoi(pdbrData[nrec].m_ppszData[i]);
																if((ucDisk>0)&&(ucDisk<100))
																	pDest->ucDiskPercentage = ucDisk;
															} break;
														case 4: // extensions
															{ 
																pDest->pszExtensions = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																if(pDest->pszExtensions) strcpy(pDest->pszExtensions, pdbrData[nrec].m_ppszData[i]);
															} break;
														case 5: // type
															{ 
																pDest->ucType = (unsigned char)atoi(pdbrData[nrec].m_ppszData[i]);
															} break;
														case 6: // desc
															{ 
															} break;
														case 7: // kb free
															{ 
																pDest->ulKBFree = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);
															} break;
														case 8: // kb total
															{ 
																pDest->ulKBTotal = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);
															} break;
														} // switch

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "field %d: %s",i, pdbrData[nrec].m_ppszData[i]);  //(Dispatch message)
//AfxMessageBox(pDest->pszIP);

														i++;
													} // while
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "freeing");  //(Dispatch message)
//AfxMessageBox("freeing");

													pdbrData[nrec].Free();  // must free explicitly, per record
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "freed");  //(Dispatch message)
//AfxMessageBox("freed");
												} // if pDest
												ppdbDest[nrec] = pDest; //even if NULL
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "assigned");  //(Dispatch message)
//	AfxMessageBox("assigned");
											}

											nrec++;  // next record
										} // while
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "finished with data");  //(Dispatch message)
//Sleep(200);

										if(pdbrData) delete [] pdbrData;
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "deleted data");  //(Dispatch message)
//Sleep(200);

										if(direct.m_data.m_ppdbDest)
										{
											unsigned long i=0;
											while (i<direct.m_data.m_ulNumDest)
											{
												if(direct.m_data.m_ppdbDest[i])
												{
													if(direct.m_data.m_ppdbDest[i]->pszIP) free(direct.m_data.m_ppdbDest[i]->pszIP);
													if(direct.m_data.m_ppdbDest[i]->pszExtensions) free(direct.m_data.m_ppdbDest[i]->pszExtensions);
													delete direct.m_data.m_ppdbDest[i];
												}
												i++;
											}

											delete [] direct.m_data.m_ppdbDest;
										}
										
										direct.m_data.m_ppdbDest = ppdbDest;  // reassign
										direct.m_data.m_ulNumDest = ulNumRecords;

									} // if allocated space for array

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "allocated");  //(Dispatch message)
//Sleep(200);

								} // if there are records to parse
							}
							else
							{
								// we already have a canon, must check for changes.
								if((ulNumRecords>0)&&(pdbrData))
								{
									int nrec=0;
									int nNumRecords = (int)ulNumRecords;
									while(nrec<nNumRecords)
									{
										if(pdbrData[nrec].m_ppszData)
										{
											unsigned short i=0;
											unsigned char ucChecksum=0;
											while(pdbrData[nrec].m_ppszData[i]!=NULL)
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checksum for record %d:%d of %d", nrec+1, i, ulNumRecords);  //(Dispatch message)
//			Sleep(50);
												unsigned long ulLen = strlen(pdbrData[nrec].m_ppszData[i]);
												ucChecksum += direct.m_net.Checksum((unsigned char*) pdbrData[nrec].m_ppszData[i], ulLen);
												i++;
											}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking checksum, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
//Sleep(100);
											bool bNew = false;
											if((direct.m_data.m_ppdbDest)&&(direct.m_data.m_ppdbDest[nrec]))
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking checksum, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
												if(direct.m_data.m_ppdbDest[nrec])
												{
													if(direct.m_data.m_ppdbDest[nrec]->ucChecksum!=ucChecksum)
														// create a new one
														bNew = true;
												}
												else
												{
													// create a new one
													bNew = true;
												}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checked, %d", bNew);  //(Dispatch message)

												if(bNew)
												{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "difference found, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
													db_dest_t* pDest = new db_dest_t;
													if(pDest)
													{
														pDest->pszIP = NULL;
														pDest->usChannelID = NULL;
														pDest->ulKeyLayers = NULL;
														pDest->ucDiskPercentage = 70;
														pDest->ucChecksum = ucChecksum;
														pDest->ulKBFree = 0;  //KB free on device
														pDest->ulKBTotal = 0;  //KB total on device
														pDest->bDiskChecked = false;
														i=0;
														while(pdbrData[nrec].m_ppszData[i]!=NULL)
														{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "difference found, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
															switch(i)
															{
															case 0:// ip address
																{
																	pDest->pszIP = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																	if(pDest->pszIP) strcpy(pDest->pszIP, pdbrData[nrec].m_ppszData[i]);
																} break;
															case 1: // keys
																{ 
																} break;
															case 2:// criteria
																{
																	//parse the criteria string.
																	// first get channel Id
																	char* pch = strstr(pdbrData[nrec].m_ppszData[i], "<roID>");
																	if(pch)
																	{
																		pch+=strlen("<roID>nd0==");  //pretty specific, but we can make general later.
																		pDest->usChannelID = (unsigned short)atoi(pch);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Channel ID in db was %d (from atoi(%s))", pDest->usChannelID, pch);  //(Dispatch message)
//Sleep(100);
																	}

																	pch = strstr(pdbrData[nrec].m_ppszData[i], "<data>");
																	if(pch)
																	{
																		unsigned short ucKey = 0;
																		pch+=strlen("<data>d|2d:1");  //pretty specific, but we can make general later.
																		if(*pch == '<')  //less than or equal to
																		{
																			pch+=2;
																			ucKey = atoi(pch);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Keyer layer in db was <= %d (from atoi(%s))", ucKey, pch);  //(Dispatch message)
//Sleep(100);

																			while(ucKey>0)
																			{
																				pDest->ulKeyLayers |= (1<<(ucKey-1));
																				ucKey--;
																			}

																		}
																		else //greater than or equal to
																		{
																			pch+=2;
																			ucKey = atoi(pch);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Keyer layer in db was >= %d (from atoi(%s))", ucKey, pch);  //(Dispatch message)
//Sleep(100);

																			while(ucKey<=32)
																			{
																				pDest->ulKeyLayers |= (1<<(ucKey-1));
																				ucKey++;
																			}
																		}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Keyer layers 0x%08x", pDest->ulKeyLayers);  //(Dispatch message)
//Sleep(100);
																	}
																} break;
															case 3: // diskspace
																{ 
																	unsigned char ucDisk = (unsigned char)atoi(pdbrData[nrec].m_ppszData[i]);
																	if((ucDisk>0)&&(ucDisk<100))
																		pDest->ucDiskPercentage = ucDisk;

																} break;
															case 4: // extensions
																{ 
																	pDest->pszExtensions = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																	if(pDest->pszExtensions) strcpy(pDest->pszExtensions, pdbrData[nrec].m_ppszData[i]);
																} break;
															case 5: // type
																{ 
																	pDest->ucType = (unsigned char)atoi(pdbrData[nrec].m_ppszData[i]);
																} break;

															case 6: // desc
																{ 
																} break;
															case 7: // kb free
																{ 
																	pDest->ulKBFree = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);
																} break;
															case 8: // kb total
																{ 
																	pDest->ulKBTotal = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);
																} break;
															} //switch

															i++;
														}  // while
														if(direct.m_data.m_ppdbDest[nrec])
														{
															if(direct.m_data.m_ppdbDest[nrec]->pszIP) free(direct.m_data.m_ppdbDest[nrec]->pszIP);
															if(direct.m_data.m_ppdbDest[nrec]->pszExtensions) free(direct.m_data.m_ppdbDest[nrec]->pszExtensions);
															delete (direct.m_data.m_ppdbDest[nrec]);
														}
														direct.m_data.m_ppdbDest[nrec] = pDest; //even if NULL

													}  // dest
												} //new
											}  // if((direct.m_data.m_ppdbDest)&&(direct.m_data.m_ppdbDest[nrec]))
										} // if there's data.

										pdbrData[nrec].Free();  // must free explicitly, per record

										nrec++;
									}  // while (nrec<ulNumRecords)

									if(pdbrData) delete [] pdbrData;

									direct.m_data.m_ulNumDest = ulNumRecords;
								}
							}
						}  // convert was successful
					} //prs was not null
					if(pdbcCriteria) delete pdbcCriteria;
				} // valid table index
			}

			// Get the list of filetypes
			// we do this every time, in case there is a change.
			if(direct.m_data.m_pdbConnPrimary!=NULL)
			{
				int nindex = direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableExchange);
				if(nindex>=0)
				{
					CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nindex);

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY mod", direct.m_settings.m_pszTableExchange);

					//Sleep(500);
					CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
					if(prs == NULL) 
					{
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetFileTypes", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
					}
					else
					{
						// check the items.
						CDBRecord* pdbrData=NULL;
						unsigned long ulNumRecords=0;

						int x = direct.m_db.Convert(prs, pdbcCriteria, &pdbrData, &ulNumRecords, dberrorstring);
						if(x != DB_SUCCESS) 
						{
							direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetFileTypes", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
							if(prs)
							{
								prs->Close();
								delete prs;
							}
						}
						else
						{
							if((direct.m_data.m_ppdbCriteria==NULL)||(ulNumRecords!=direct.m_data.m_ulNumCriteria))
							{
								direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:GetFileTypes", "%d items retrieved from %s", ulNumRecords, direct.m_settings.m_pszTableExchange);  //(Dispatch message)

//								Sleep(500);
//								AfxMessageBox("X");
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d items retrieved from %s", ulNumRecords, direct.m_settings.m_pszTableExchange);  //(Dispatch message)
//Sleep(200);
								if((ulNumRecords>0)&&(pdbrData))
								{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d items", ulNumRecords);  //(Dispatch message)
//Sleep(200);
									db_exchange_t** ppdbCriteria = new db_exchange_t*[ulNumRecords];
									if(ppdbCriteria)
									{
										int nrec=0;
										int nNumRecords = (int)ulNumRecords;
										while(nrec<nNumRecords)
										{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
//Sleep(100);
											ppdbCriteria[nrec] = NULL;
											if(pdbrData[nrec].m_ppszData)
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "there's data");  //(Dispatch message)
//Sleep(200);
												db_exchange_t* pExch = new db_exchange_t;
												if(pExch)
												{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "new object");  //(Dispatch message)
//Sleep(200);
													pExch->pszCriterion = NULL;
													pExch->pszFlag = NULL;
													pExch->ulMod = 0;
													pExch->ucChecksum = 0;

													unsigned short i=0;
													while((pdbrData[nrec].m_ppszData)&&(pdbrData[nrec].m_ppszData[i]!=NULL))
													{
//AfxMessageBox("record");   

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "processing field [%s]", pdbrData[nrec].m_ppszData[i]);  //(Dispatch message)
//Sleep(20);
														unsigned long ulLen = strlen(pdbrData[nrec].m_ppszData[i]);
														pExch->ucChecksum += direct.m_net.Checksum((unsigned char*) pdbrData[nrec].m_ppszData[i], ulLen);

														switch(i)
														{
														case 0:// criterion
															{
																pExch->pszCriterion = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																if(pExch->pszCriterion) strcpy(pExch->pszCriterion, pdbrData[nrec].m_ppszData[i]);
															} break;
														case 1: // flag
															{ 
																pExch->pszFlag = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																if(pExch->pszFlag) strcpy(pExch->pszFlag, pdbrData[nrec].m_ppszData[i]);
															} break;
														case 2:// mod
															{ 
																pExch->ulMod = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);
															} break;
														} // switch

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "field %d: %s",i, pdbrData[nrec].m_ppszData[i]);  //(Dispatch message)
//AfxMessageBox(pDest->pszIP);

														i++;
													} // while
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "freeing");  //(Dispatch message)
//AfxMessageBox("freeing");

													pdbrData[nrec].Free();  // must free explicitly, per record
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "freed");  //(Dispatch message)
//AfxMessageBox("freed");
												} // if pDest
												ppdbCriteria[nrec] = pExch; //even if NULL
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "assigned");  //(Dispatch message)
//	AfxMessageBox("assigned");
											}
											nrec++;  // next record
										} // while
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "finished with data");  //(Dispatch message)
//Sleep(200);

										if(pdbrData) delete [] pdbrData;
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "deleted data");  //(Dispatch message)
//Sleep(200);

										if(direct.m_data.m_ppdbCriteria)
										{
											unsigned long i=0;
											while (i<direct.m_data.m_ulNumCriteria)
											{
												if(direct.m_data.m_ppdbCriteria[i])
												{
													if(direct.m_data.m_ppdbCriteria[i]->pszCriterion) free(direct.m_data.m_ppdbCriteria[i]->pszCriterion);
													if(direct.m_data.m_ppdbCriteria[i]->pszFlag) free(direct.m_data.m_ppdbCriteria[i]->pszFlag);
													delete direct.m_data.m_ppdbCriteria[i];
												}
												i++;
											}

											delete [] direct.m_data.m_ppdbCriteria;
										}
										
										direct.m_data.m_ppdbCriteria = ppdbCriteria;  // reassign
										direct.m_data.m_ulNumCriteria = ulNumRecords;

									} // if allocated space for array

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "allocated");  //(Dispatch message)
//Sleep(200);

								} // if there are records to parse
							}
							else
							{
								// we already have a canon, must check for changes.
								if((ulNumRecords>0)&&(pdbrData))
								{
									int nrec=0;
									int nNumRecords = (int)ulNumRecords;
									while(nrec<nNumRecords)
									{
										if(pdbrData[nrec].m_ppszData)
										{
											unsigned short i=0;
											unsigned char ucChecksum=0;
											while(pdbrData[nrec].m_ppszData[i]!=NULL)
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checksum for record %d:%d of %d", nrec+1, i, ulNumRecords);  //(Dispatch message)
//			Sleep(50);
												unsigned long ulLen = strlen(pdbrData[nrec].m_ppszData[i]);
												ucChecksum += direct.m_net.Checksum((unsigned char*) pdbrData[nrec].m_ppszData[i], ulLen);
												i++;
											}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking checksum, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
//Sleep(100);
											bool bNew = false;
											if((direct.m_data.m_ppdbCriteria)&&(direct.m_data.m_ppdbCriteria[nrec]))
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking checksum, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
												if(direct.m_data.m_ppdbCriteria[nrec])
												{
													if(direct.m_data.m_ppdbCriteria[nrec]->ucChecksum!=ucChecksum)
														// create a new one
														bNew = true;
												}
												else
												{
													// create a new one
													bNew = true;
												}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checked, %d", bNew);  //(Dispatch message)

												if(bNew)
												{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "difference found, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
													db_exchange_t* pExch = new db_exchange_t;
													if(pExch)
													{
														pExch->pszCriterion = NULL;
														pExch->pszFlag = NULL;
														pExch->ulMod = 0;
														pExch->ucChecksum = 0;
														i=0;
														while(pdbrData[nrec].m_ppszData[i]!=NULL)
														{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "difference found, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
															switch(i)
															{
															case 0:// criterion
																{
																	pExch->pszCriterion = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																	if(pExch->pszCriterion) strcpy(pExch->pszCriterion, pdbrData[nrec].m_ppszData[i]);
																} break;
															case 1: // flag
																{ 
																	pExch->pszFlag = (char*)malloc(strlen(pdbrData[nrec].m_ppszData[i])+1);
																	if(pExch->pszFlag) strcpy(pExch->pszFlag, pdbrData[nrec].m_ppszData[i]);
																} break;
															case 2:// mod
																{ 
																	pExch->ulMod = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);
																} break;
															} // switch

															i++;
														}  // while
														if(direct.m_data.m_ppdbCriteria[nrec])
														{
															if(direct.m_data.m_ppdbCriteria[nrec]->pszCriterion) free(direct.m_data.m_ppdbCriteria[nrec]->pszCriterion);
															if(direct.m_data.m_ppdbCriteria[nrec]->pszFlag) free(direct.m_data.m_ppdbCriteria[nrec]->pszFlag);
															delete (direct.m_data.m_ppdbCriteria[nrec]);
														}
														direct.m_data.m_ppdbCriteria[nrec] = pExch; //even if NULL

													}  // dest
												} //new
											}  // if((direct.m_data.m_ppdbCriteria)&&(direct.m_data.m_ppdbCriteria[nrec]))
										} // if there's data.

										pdbrData[nrec].Free();  // must free explicitly, per record

										nrec++;
									}  // while (nrec<ulNumRecords)

									if(pdbrData) delete [] pdbrData;

									direct.m_data.m_ulNumCriteria = ulNumRecords;
								}
							}
						}  // convert was successful

					} //prs was not null
					if(pdbcCriteria) delete pdbcCriteria;
				} // valid table index
			}

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before get channels");  //(Dispatch message)
Sleep(200);
			// Get the list of channel IDs from db 
			// we do this every time, in case there is a change.
			if(direct.m_data.m_pdbConnPrimary!=NULL)
			{
				int nindex = direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableChannels);
				if(nindex>=0)
				{
					CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nindex);

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ", direct.m_settings.m_pszTableChannels);

					//Sleep(500);
					CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
					if(prs == NULL) 
					{
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetChannels", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
					}
					else
					{
						// check the items.
						CDBRecord* pdbrData=NULL;
						unsigned long ulNumRecords=0;

//				AfxMessageBox("here");
						int x = direct.m_db.Convert(prs, pdbcCriteria, &pdbrData, &ulNumRecords, dberrorstring);
//				AfxMessageBox("here2");
						if(x != DB_SUCCESS) 
						{
							direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetChannels", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
							if(prs)
							{
								prs->Close();
								delete prs;
							}
						}
						else
						{
							if((direct.m_data.m_ppdbChannels==NULL)||(ulNumRecords!=direct.m_data.m_ulNumChannels))
							{
								direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:GetChannels", "%d items retrieved from %s", ulNumRecords, direct.m_settings.m_pszTableChannels);  //(Dispatch message)

//								Sleep(500);
//								AfxMessageBox("X");
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d items retrieved from %s", ulNumRecords, direct.m_settings.m_pszTableChannels);  //(Dispatch message)
//Sleep(100);
								if((ulNumRecords>0)&&(pdbrData))
								{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d items", ulNumRecords);  //(Dispatch message)
									db_channel_t** ppdbChannels = new db_channel_t*[ulNumRecords];
									if(ppdbChannels)
									{
										int nrec=0;
										int nNumRecords = (int)ulNumRecords;
										while(nrec<nNumRecords)
										{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
											ppdbChannels[nrec] = NULL;
											if(pdbrData[nrec].m_ppszData)
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "there's data");  //(Dispatch message)
												db_channel_t* pChannel = new db_channel_t;
												if(pChannel)
												{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "new object");  //(Dispatch message)
													pChannel->usChannelID = NULL;
													pChannel->ulFlags = 0x00000000;
													pChannel->ucChecksum = 0;
													unsigned short i=0;
													while((pdbrData[nrec].m_ppszData)&&(pdbrData[nrec].m_ppszData[i]!=NULL))
													{
														if(strlen(pdbrData[nrec].m_ppszData[i]))
														{

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "processing field %d: [%s]", i, pdbrData[nrec].m_ppszData[i]);  //(Dispatch message)
Sleep(10);
															unsigned long ulLen = strlen(pdbrData[nrec].m_ppszData[i]);
															pChannel->ucChecksum += direct.m_net.Checksum((unsigned char*) pdbrData[nrec].m_ppszData[i], ulLen);

															switch(i)
															{
															case 0:// ID
																{
																	pChannel->usChannelID = (unsigned short)atoi(pdbrData[nrec].m_ppszData[i]);
																} break;
															case 1: // flags
																{ 
																	pChannel->ulFlags = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);
	
																	// find the channel ID in the omni list and set the frame rate
																	unsigned short o=0;
																	while (o<direct.m_settings.m_usNumColossus)
																	{
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "o = %d, m_usNumColossus = %d", o, direct.m_settings.m_usNumColossus);  //(Dispatch message)
//Sleep(100);
																		if((o<direct.m_omni.m_usNumConn)&&(direct.m_omni.m_ppConn)&&(direct.m_omni.m_ppConn[o]))
																		{
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "direct.m_omni.m_usNumConn = %d, direct.m_omni.m_ppConn = %d, [%d]=%d", direct.m_omni.m_usNumConn, direct.m_omni.m_ppConn, o, direct.m_omni.m_ppConn[o]);  //(Dispatch message)
//Sleep(100);

EnterCriticalSection(&direct.m_omni.m_ppConn[o]->m_crit);

																			if(!(direct.m_omni.m_ppConn[o]->m_ulFlags&OMNI_FRAMEBASISSET))
																			{
//its not list based, but connection based.
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "flags = %d, channel = %d", pChannel->ulFlags, pChannel->usChannelID);  //(Dispatch message)
//Sleep(100);

//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking list = %d", pChannel->usChannelID);  //(Dispatch message)
		
//																				EnterCriticalSection (&direct.m_omni.m_ppConn[o]->m_critRLI);
																				int nList = direct.m_omni.m_ppConn[o]->ReturnListIndex(pChannel->usChannelID);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "found list = %d", nList);  //(Dispatch message)
//Sleep(100);
//																				LeaveCriticalSection (&direct.m_omni.m_ppConn[o]->m_critRLI);
																				if(nList>=0)
																				{
																					if((pChannel->ulFlags&DIRECT_CHANNEL_TYPEMASK)==DIRECT_CHANNEL_NDF)
																					{
																						direct.m_omni.m_ppConn[o]->m_ulFlags&= ~OMNI_FRAMEBASISMASK;
																						direct.m_omni.m_ppConn[o]->m_ulFlags |= (OMNI_NTSCNDF|OMNI_FRAMEBASISSET);
																					}
																					else
																					if((pChannel->ulFlags&DIRECT_CHANNEL_TYPEMASK)==DIRECT_CHANNEL_PAL)
																					{
																						direct.m_omni.m_ppConn[o]->m_ulFlags&= ~OMNI_FRAMEBASISMASK;
																						direct.m_omni.m_ppConn[o]->m_ulFlags |= (OMNI_PAL|OMNI_FRAMEBASISSET);
																					}
																					else
																					{
																						direct.m_omni.m_ppConn[o]->m_ulFlags&= ~OMNI_FRAMEBASISMASK;
																						direct.m_omni.m_ppConn[o]->m_ulFlags |= OMNI_FRAMEBASISSET;
																					}
																					break; // found the right connection. dont loop thru all
																				}
																			}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "after framebasis set");  //(Dispatch message)
//Sleep(100);
LeaveCriticalSection(&direct.m_omni.m_ppConn[o]->m_crit);

																		}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "incrementing o");  //(Dispatch message)
//Sleep(100);
																		o++;
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "incremented o");  //(Dispatch message)
//Sleep(100);
																	}
																} break;
															default:// desc and unknown
																{
																} break;
															} // switch
														} // if datalen

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "field %d: %s",i, pdbrData[nrec].m_ppszData[i]);  //(Dispatch message)
//AfxMessageBox(pDest->pszIP);

														i++;
													} // while
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "freeing");  //(Dispatch message)
//AfxMessageBox("freeing");

													pdbrData[nrec].Free();  // must free explicitly, per record
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "freed");  //(Dispatch message)
//AfxMessageBox("freed");
												} // if pDest
												ppdbChannels[nrec] = pChannel; //even if NULL
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "assigned");  //(Dispatch message)
//	AfxMessageBox("assigned");
											}

											nrec++;  // next record
										}
										if(pdbrData) delete [] pdbrData;

										if(direct.m_data.m_ppdbChannels)
										{
											unsigned long i=0;
											while (i<direct.m_data.m_ulNumChannels)
											{
												if(direct.m_data.m_ppdbChannels[i])
												{
													delete direct.m_data.m_ppdbChannels[i];
												}
												i++;
											}

											delete [] direct.m_data.m_ppdbChannels;
										}
										
										direct.m_data.m_ppdbChannels = ppdbChannels;  // reassign
										direct.m_data.m_ulNumChannels = ulNumRecords;

									} // if allocated space for array
								} // if there are records to parse
							}
							else
							{
								// we already have a canon, must check for changes.
								if((ulNumRecords>0)&&(pdbrData))
								{
									int nrec=0;
									int nNumRecords = (int)ulNumRecords;
									while(nrec<nNumRecords)
									{
										if(pdbrData[nrec].m_ppszData)
										{
											unsigned short i=0;
											unsigned char ucChecksum=0;
											while(pdbrData[nrec].m_ppszData[i]!=NULL)
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checksum for record %d:%d of %d", nrec+1, i, ulNumRecords);  //(Dispatch message)
	Sleep(10);
												unsigned long ulLen = strlen(pdbrData[nrec].m_ppszData[i]);
												if(ulLen)	ucChecksum += direct.m_net.Checksum((unsigned char*) pdbrData[nrec].m_ppszData[i], ulLen);
												i++;
											}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking checksum, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
//Sleep(100);
											bool bNew = false;
											if((direct.m_data.m_ppdbChannels)&&(direct.m_data.m_ppdbChannels[nrec]))
											{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking checksum, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
												if(direct.m_data.m_ppdbChannels[nrec])
												{
													if(direct.m_data.m_ppdbChannels[nrec]->ucChecksum!=ucChecksum)
														// create a new one
														bNew = true;
												}
												else
												{
													// create a new one
													bNew = true;
												}
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checked, %d", bNew);  //(Dispatch message)

												if(bNew)
												{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "difference found, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
													db_channel_t* pChannel = new db_channel_t;
													if(pChannel)
													{
														pChannel->usChannelID = NULL;
														pChannel->ulFlags = 0x00000000;
														pChannel->ucChecksum = ucChecksum;
														i=0;
														while(pdbrData[nrec].m_ppszData[i]!=NULL)
														{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "difference found, record %d of %d", nrec+1, ulNumRecords);  //(Dispatch message)
															if(strlen(pdbrData[nrec].m_ppszData[i]))
															{
																switch(i)
																{
																case 0:// ID
																	{
																		pChannel->usChannelID = (unsigned short)atoi(pdbrData[nrec].m_ppszData[i]);
																	} break;
																case 1: // flags
																	{ 
																		pChannel->ulFlags = (unsigned long)atol(pdbrData[nrec].m_ppszData[i]);

																		// find the channel ID in the omni list and set the frame rate
																		unsigned short o=0;
																		while (o<direct.m_settings.m_usNumColossus)
																		{
	//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "o = %d, m_usNumColossus = %d", o, direct.m_settings.m_usNumColossus);  //(Dispatch message)
	//Sleep(100);
																			if((o<direct.m_omni.m_usNumConn)&&(direct.m_omni.m_ppConn)&&(direct.m_omni.m_ppConn[o]))
																			{
	//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "direct.m_omni.m_usNumConn = %d, direct.m_omni.m_ppConn = %d, [%d]=%d", direct.m_omni.m_usNumConn, direct.m_omni.m_ppConn, o, direct.m_omni.m_ppConn[o]);  //(Dispatch message)
	//Sleep(100);
EnterCriticalSection(&direct.m_omni.m_ppConn[o]->m_crit);
																				if(!(direct.m_omni.m_ppConn[o]->m_ulFlags&OMNI_FRAMEBASISSET))
																				{
	//its not list based, but connection based.
	//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "flags = %d, channel = %d", pChannel->ulFlags, pChannel->usChannelID);  //(Dispatch message)
	//Sleep(100);

	//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking list = %d", pChannel->usChannelID);  //(Dispatch message)
			
//																					EnterCriticalSection (&direct.m_omni.m_ppConn[o]->m_critRLI);
																					int nList = direct.m_omni.m_ppConn[o]->ReturnListIndex(pChannel->usChannelID);
	//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "found list = %d", nList);  //(Dispatch message)
	//Sleep(100);
//																					LeaveCriticalSection (&direct.m_omni.m_ppConn[o]->m_critRLI);
																					if(nList>=0)
																					{
																						if((pChannel->ulFlags&DIRECT_CHANNEL_TYPEMASK)==DIRECT_CHANNEL_NDF)
																						{
																							direct.m_omni.m_ppConn[o]->m_ulFlags&= ~OMNI_FRAMEBASISMASK;
																							direct.m_omni.m_ppConn[o]->m_ulFlags |= (OMNI_NTSCNDF|OMNI_FRAMEBASISSET);
																						}
																						else
																						if((pChannel->ulFlags&DIRECT_CHANNEL_TYPEMASK)==DIRECT_CHANNEL_PAL)
																						{
																							direct.m_omni.m_ppConn[o]->m_ulFlags&= ~OMNI_FRAMEBASISMASK;
																							direct.m_omni.m_ppConn[o]->m_ulFlags |= (OMNI_PAL|OMNI_FRAMEBASISSET);
																						}
																						else
																						{
																							direct.m_omni.m_ppConn[o]->m_ulFlags&= ~OMNI_FRAMEBASISMASK;
																							direct.m_omni.m_ppConn[o]->m_ulFlags |= OMNI_FRAMEBASISSET;
																						}
																						break; // found the right connection. dont loop thru all
																					}
																				}
	//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "after framebasis set");  //(Dispatch message)
	//Sleep(100);
LeaveCriticalSection(&direct.m_omni.m_ppConn[o]->m_crit);

																			}
	//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "incrementing o");  //(Dispatch message)
	//Sleep(100);
																			o++;
	//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "incremented o");  //(Dispatch message)
	//Sleep(100);
																		}

																	} break;
																default:// desc and unknown
																	{
																	} break;
																} // switch

															} // if datalen

															i++;
														}  // while
														if(direct.m_data.m_ppdbChannels[nrec])
														{
															delete (direct.m_data.m_ppdbChannels[nrec]);
														}
														direct.m_data.m_ppdbChannels[nrec] = pChannel; //even if NULL

													}  // dest
												} //new
											}  // if((direct.m_data.m_ppdbChannels)&&(direct.m_data.m_ppdbChannels[nrec]))
										} // if there's data.

										pdbrData[nrec].Free();  // must free explicitly, per record

										nrec++;
									}  // while (nrec<ulNumRecords)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "done with channels");  //(Dispatch message)
//Sleep(200);

									if(pdbrData) delete [] pdbrData;

									direct.m_data.m_ulNumChannels = ulNumRecords;
								}
							}
						}  // convert was successful
					} //prs was not null
					if(pdbcCriteria) delete pdbcCriteria;
				} // valid table index
			}

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to OxSoxLoad");  //(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before device check");  //(Dispatch message)
//Sleep(200);

			// On an interval, ping each one in some way and report failures
			// load the oxsox dll if not already loaded
			if(direct.m_ox.m_hinstDLL==NULL)
			{
				//load
				int nReturn = direct.m_ox.OxSoxLoad();
				if(nReturn<OX_SUCCESS)
					direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:OxSoxLoad", "OxSoxLoad returned %d", nReturn);  //(Dispatch message)
				//else
					//direct.m_ox.OxSoxInit();
			}

			if(
					(direct.m_ox.m_hinstDLL)
//				&&(direct.m_ox.m_bDLLWinsockInit)  // dont init, requires a ping.
				&&(!direct.m_ox.m_bTransferring)
				&&(direct.m_data.m_ppdbDest)
				&&(direct.m_data.m_ulNumDest>0)
				// we may also want to put in a thing to map channel ID to not ping these hosts.
				&&(direct.m_data.m_ppdbChannels)
				&&(direct.m_data.m_ulNumChannels>0)
				)
			{
				int nrec = (int)direct.m_data.m_ulNextPing;
				if(nrec>=(int)direct.m_data.m_ulNumDest) nrec = 0; //loop!
				int nNumDest = (int)direct.m_data.m_ulNumDest;
				while(nrec<nNumDest)
				{
				// we may also want to put in a thing to map channel ID to not ping these hosts.
					if(direct.m_data.GetChannelFlag(direct.m_data.m_ppdbDest[nrec]->usChannelID)&DIRECT_CHANNEL_USE)
					{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to set host %s",direct.m_data.m_ppdbDest[nrec]->pszIP);  //(Dispatch message)
						if((direct.m_data.m_ppdbDest[nrec]->pszIP)&&(strlen(direct.m_data.m_ppdbDest[nrec]->pszIP))&&(!direct.m_ox.m_bTransferring))
						{
							char szIP[20];
							_snprintf(szIP,16,"%s",direct.m_data.m_ppdbDest[nrec]->pszIP);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "setting host %s",szIP);  Sleep(100); //(Dispatch message)
							int nReturn = direct.m_ox.SetHost(szIP);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "set host %s",szIP); Sleep(100);  //(Dispatch message)
							if(nReturn<OX_SUCCESS)
							{
								direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:SetHost", "SetHost returned %d for %s", nReturn, szIP?szIP:"(null)");  //(Dispatch message)
							}
							else
							{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pinging host %s", szIP);  Sleep(100);//(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "really pinging host %s", szIP);  Sleep(100);//(Dispatch message)
								nReturn = direct.m_ox.OxSoxPing();  // ping inits if not init'ed
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pinged host %s, got %d",szIP, nReturn);   Sleep(100);//(Dispatch message)
								if((nReturn<OX_SUCCESS)||(nReturn==OX_NOPING))
								{
									direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:OxSoxPing", "OxSoxPing returned %d for %s - host not found!", nReturn, szIP?szIP:"(null)");  //(Dispatch message)
//Sleep(100);
									direct.SendError("Direct:OxSoxPing", "OxSoxPing returned %d for %s - host not found!", nReturn, szIP?szIP:"(null)");  //(Dispatch message)
									// TODO:  something more with the error here.
			EnterCriticalSection(&theApp.m_crit);
								theApp.m_nAppState=ICON_RED;
									theApp.SetAppStateText( "oxsox error pinging host %s.", szIP?szIP:"(null)");
			LeaveCriticalSection(&theApp.m_crit);

									direct.m_data.m_ulNextPing = nrec+1;
									break; // on a non-comm, we timed out, so we dont want to mess with the process timing.. just break out.
								}
								else
								{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "ping returned TRUE for host %s",szIP); Sleep(100); //(Dispatch message)
									// its there so lets get the disk space and update the DB
									if(!direct.m_data.m_ppdbDest[nrec]->bDiskChecked)
									{
										//check it.
										DiskInfo_ disk_info;
										disk_info.KBytes_Free = 0;
										disk_info.KBytes_Total = 0;
										// multi partition, we are just going to check audio and video
										if(direct.m_data.m_ppdbDest[nrec]->ucType == DIRECT_DESTTYPE_IS2)
										{
											direct.m_data.m_ppdbDest[nrec]->bDiskChecked = true;
											DiskInfo_ disk_info_is2;
											if(direct.m_ox.OxSoxGetDriveInfo(VIDEO_DIRECTORY, &disk_info_is2) == OX_SUCCESS)
											{
												disk_info.KBytes_Free += disk_info_is2.KBytes_Free;	// size of free space in KiB
												disk_info.KBytes_Total += disk_info_is2.KBytes_Total;	// size of free space in KiB
											}
											else
											{
												direct.m_data.m_ppdbDest[nrec]->bDiskChecked = false;
											}
											if(direct.m_ox.OxSoxGetDriveInfo(AUDIO_DIRECTORY, &disk_info_is2) == OX_SUCCESS)
											{
												disk_info.KBytes_Free += disk_info_is2.KBytes_Free;	// size of free space in KiB
												disk_info.KBytes_Total += disk_info_is2.KBytes_Total;	// size of free space in KiB
											}
											else
											{
												direct.m_data.m_ppdbDest[nrec]->bDiskChecked = false;
											}
										}
										else // one partition
										if(
												(direct.m_data.m_ppdbDest[nrec]->ucType == DIRECT_DESTTYPE_INT)
											||(direct.m_data.m_ppdbDest[nrec]->ucType == DIRECT_DESTTYPE_ISHD)
											)
										{
											if(direct.m_ox.OxSoxGetDriveInfo(VIDEO_DIRECTORY, &disk_info) == OX_SUCCESS)
											{
												direct.m_data.m_ppdbDest[nrec]->bDiskChecked = true;
											}
										}
										else
										{
											direct.m_data.m_ppdbDest[nrec]->bDiskChecked = true;
										}
										if(direct.m_data.m_ppdbDest[nrec]->bDiskChecked)
										{
											// update DB

											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
												"UPDATE %s SET kb_free = %d, kb_total = %d WHERE IP LIKE '%s'", 
												direct.m_settings.m_pszTableDest,
												disk_info.KBytes_Free,
												disk_info.KBytes_Total,
												szIP
												);
											if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
											{
									//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
											}
											else
											{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "update disk space for host %s: kb_free = %d, kb_total = %d",
								 szIP, disk_info.KBytes_Free,	disk_info.KBytes_Total); Sleep(100); //(Dispatch message)

											}
										}
									}
								}
							}
						}
					}
					nrec++;
					direct.m_data.m_ulNextPing = nrec;
				}
			}

			direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct", "Performing media check on registered channels.");  //(Dispatch message)
Sleep(100);
			// Make sure all the media that is coming up in the playlists are on the boxes
			// have to parse data for every channel!

			// we are going to check every event on every channel.  
			// we are going to add new ones to the database, delete from the db ones we dont find (they are done or removed)
			// if they exist already, we leave the record alone
			// later, we check every db record sorted by time, so we can get first ones first.

			// we can only do one transfer at a time, so we can abort (just wait for now) if there is any transfer in progress.
			pdbrLiveEventData=NULL;
			ulNumLiveEventRecords=0;

			if(direct.m_data.m_pdbConnPrimary)
			{
				nLiveEventTableIndex = direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableEvents);
				if(nLiveEventTableIndex>=0)
				{
					CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

					// get them all
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", direct.m_settings.m_pszTableEvents);

					//Sleep(500);
					CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
					if(prs == NULL) 
					{
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetLiveEvents", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
					}
					else
					{
						// check the items.
//				AfxMessageBox("here");
						int x = direct.m_db.Convert(prs, pdbcCriteria, &pdbrLiveEventData, &ulNumLiveEventRecords, dberrorstring);
//				AfxMessageBox("here2");
						if(x != DB_SUCCESS) 
						{
							if(prs)
							{
								prs->Close();
								delete prs;
							}
							direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetLiveEvents", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
						}
					}
					if(pdbcCriteria) delete pdbcCriteria;

				}
			}


			// now, we have loaded up all the event information in pdbrLiveEventData;
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "ulNumLiveEventRecords = %d", ulNumLiveEventRecords); Sleep(100); //(Dispatch message)


			int nConnectionIndex=0;
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before media check with %d conns", direct.m_omni.m_usNumConn);  //(Dispatch message)
			while(//	(0)&&
						 ((direct.m_data.m_pdbConnPrimary!=NULL)||(direct.m_data.m_pdbConnBackup!=NULL))
						 &&(nConnectionIndex<direct.m_omni.m_usNumConn)
						 &&(direct.m_omni.m_ppConn)
					 )
			{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "while %d conn < %d conns", nConnectionIndex, direct.m_omni.m_usNumConn);  //(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before check with %d and []%d on conn %d, %d lists", 
//								 direct.m_omni.m_ppConn,direct.m_omni.m_ppConn[nConnectionIndex], nConnectionIndex, direct.m_omni.m_ppConn[nConnectionIndex]->GetListCount());  //(Dispatch message)
//Sleep(200);
//  /*

EnterCriticalSection(&direct.m_omni.m_ppConn[nConnectionIndex]->m_crit);

				int nListIndex=0;
///////////  start dbg
/*				
				while(//	(0)&&
								(direct.m_omni.CheckConnectionValid(nConnectionIndex))
							&&(nListIndex < direct.m_omni.m_ppConn[nConnectionIndex]->GetListCount()) 
							)
				{
// /*
					CCAList* pList = direct.m_omni.m_ppConn[nConnectionIndex]->GetList(nListIndex);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "while %d (list index) < %d (list count) ( %d of %d conns)", nListIndex, direct.m_omni.m_ppConn[nConnectionIndex]->GetListCount(), nConnectionIndex, direct.m_omni.m_usNumConn);  //(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "check list %d not null: %d", nListIndex, direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]?1:0);  //(Dispatch message)
//					unsigned char h,m,s,f, dh, dm, ds, df;
//					EnterCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_critRLI);
//					EnterCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_critUL);
//Sleep(100);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: direct.m_omni.m_ppConn=%d", direct.m_omni.m_ppConn);
//Sleep(100);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: direct.m_omni.m_ppConn[nConnectionIndex]=%d",	direct.m_omni.m_ppConn[nConnectionIndex]);
//Sleep(100);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList=%d",
//											direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList);
//Sleep(100);
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]=%d",
											pList);
Sleep(100);
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_pszListName = %s",
											pList->m_pszListName?pList->m_pszListName:"(null)");
Sleep(100);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents=%d",
//											direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents);
//Sleep(100);
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_usEventCount=%d",
											pList->GetEventCount()
										 );
Sleep(100);  //dbg
					nListIndex++;
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "incremented nListIndex to %d", nListIndex);  //(Dispatch message)
Sleep(100);  //dbg
				
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking nListIndex = %d", nListIndex);  //(Dispatch message)
Sleep(100);  //dbg
				
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking again nListIndex = %d", nListIndex);  //(Dispatch message)
Sleep(100);  //dbg
				
				}

				Sleep(500);  //dbg
				nListIndex=0;  //dbg
////// end dbg
*/
				while(//	(0)&&  //dbg
								(direct.m_omni.CheckConnectionValid(nConnectionIndex))
							&&(nListIndex < direct.m_omni.m_ppConn[nConnectionIndex]->GetListCount()) 
							)
				{
// /*

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "while %d (list index) < %d (list count) ( %d of %d conns)", nListIndex, direct.m_omni.m_ppConn[nConnectionIndex]->GetListCount(), nConnectionIndex, direct.m_omni.m_usNumConn);  //(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "check list %d not null: %d", nListIndex, pList?1:0);  //(Dispatch message)
					int nEventIndex=0;
					CCAList* pList = direct.m_omni.m_ppConn[nConnectionIndex]->GetList(nListIndex);
//					unsigned char h,m,s,f, dh, dm, ds, df;
//					EnterCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_critRLI);
//					EnterCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_critUL);
/*
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: %d,[nConnectionIndex]=%d, %d,[nListIndex]=%d, [%s] m_ppEvents=%d numevts=%d",
											direct.m_omni.m_ppConn,
											direct.m_omni.m_ppConn[nConnectionIndex],
											pList,
											pList,
											pList->m_pszListName?pList->m_pszListName:"(null)",
//											pList->m_ppEvents,
											pList->GetEventCount(),
											pList->GetEventCount()
										 );

Sleep(200);
*/
					if(//(0)&&
						  (pList)
						)
					{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to check channel flag");  //(Dispatch message)
//Sleep(200);
						if(direct.m_data.GetChannelFlag(pList->m_usListID)&DIRECT_CHANNEL_USE)
						{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "DIRECT_CHANNEL_USE, %d events on channel", pList->m_usEventCount);  //(Dispatch message)
//Sleep(200);
// /*
							unsigned short usEventCount = pList->GetEventCount();
							while(nEventIndex < usEventCount)
							{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "while %d event index < %d events", nEventIndex , pList->m_usEventCount);  //(Dispatch message)
//Sleep(200);
//								EnterCriticalSection (&pList->m_critGEI);
//								EnterCriticalSection (&pList->m_critUE);

								CCAEvent* pEvent = pList->GetEvent(nEventIndex);

								if(pEvent)
								{
/*
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "event pointers: %d,[nConnectionIndex]=%d, %d,[nListIndex]=%d, %d[nEventIndex]=%d",
											direct.m_omni.m_ppConn,
											direct.m_omni.m_ppConn[nConnectionIndex],
											pList,
											pList,
//											pList->m_ppEvents,
//											pList->m_ppEvents[nEventIndex]
pEvent,
pEvent
										 );
Sleep(200);
	*/
	/*
									m_omni.m_ppConn[nConnectionIndex]->ConvertMillisecondsToHMSF(m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_ulOnAirTimeMS, 
										&h,&m,&s,&f);
									m_omni.m_ppConn[nConnectionIndex]->ConvertMillisecondsToHMSF(m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_ulDurationMS, 
										&dh, &dm, &ds, &df);

									szRecord.Format(" event %03d: [%s] [%s] [%s] [%s] t%d, tm%d, s%d, o%02d:%02d:%02d.%02d, d%02d:%02d:%02d.%02d\r\n",
										nEventIndex, 
										m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszReconcileKey,
										m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszID,
										m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszTitle,
										m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszData,
										m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_usType,
										m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_usControl,
										m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_usStatus,
										h,m,s,f, dh, dm, ds, df
			//							m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_ulOnAirTimeMS,
			//							m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_ulDurationMS

										);

										*/


									//here we go!
									// lets check in the db, if there, add the exists mark only.

									bool bFoundLiveEvent = false;
									unsigned long ulCheckingIndex=0;

									unsigned short usChannelID = pList->m_usListID;
									if((pdbrLiveEventData)&&(ulNumLiveEventRecords))
									{
										while((!bFoundLiveEvent)&&(ulCheckingIndex<ulNumLiveEventRecords))
										{
											if(
													(pdbrLiveEventData[ulCheckingIndex].m_ppszData)
												&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID])  
												&&(pEvent->m_pszReconcileKey)
												)
											{
												// field 0 is the ID.
												if(strcmp(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID], 
													pEvent->m_pszReconcileKey)==0)
												{
													// found a possible event, make sure its on the right channel.
													if(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL])
													{
														//field 1 is the channel ID
														if(atoi(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL])==usChannelID)
														{
															// this is the one.
															bFoundLiveEvent = true;
															// update the DB record.
															if(direct.m_data.m_pdbConnPrimary)
															{
																// Update!
																// we can 
/*
																if(nLiveEventTableIndex>=0)
																{
																	CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);
																	CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

																	// load up the data.
																	int i=0;
																	for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
																	{
																		pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
																		if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
																		{
																			pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
																			if(pdbcCriteria->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
																			pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
																		}
																		// we are just adding an "exists" thing.
																		if(i==DIRECT_LEFIELD_STATUS) // field 4 is status
																		{
																			unsigned long ulStatus = atol(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
																			ulStatus|=DIRECT_LESTATUS_EXISTS;
																			pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
																			if(pdbcValues->m_pdbField[i].m_pszData)
																				sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d",ulStatus );
																		}
																		else
																		{
																			pdbcValues->m_pdbField[i].m_ucType=DB_OP_IGNORE;
																			if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
																			{
																				pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
																				if(pdbcValues->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
																				pdbcValues->m_pdbField[i].m_ucType=DB_OP_EQUAL;
																			}
																		}
																	}

																	if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
																	{
																		direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
																	}
																	if(pdbcValues) delete pdbcValues;
																	if(pdbcCriteria) delete pdbcCriteria;
																}
															
*/

								if(nLiveEventTableIndex>=0)
								{
									unsigned long ulStatus = atol(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS]);
									ulStatus|=DIRECT_LESTATUS_EXISTS;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}

/*
									CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);
									CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

									// load up the data.
									int i=0;
									for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
									{
										pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
										if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
										{
											pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
											if(pdbcCriteria->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
											pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
										}
										// we are just adding an exists thing.
										if(i==DIRECT_LEFIELD_STATUS) // field 4 is status
										{
											unsigned long ulStatus = atol(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
											ulStatus|=DIRECT_LESTATUS_EXISTS;
											pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
											if(pdbcValues->m_pdbField[i].m_pszData)
												sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d",ulStatus );
										}
										else
										{
											pdbcValues->m_pdbField[i].m_ucType=DB_OP_IGNORE;
											if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
											{
												pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
												if(pdbcValues->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
												pdbcValues->m_pdbField[i].m_ucType=DB_OP_EQUAL;
											}
										}
									}

									if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
									if(pdbcValues) delete pdbcValues;
									if(pdbcCriteria) delete pdbcCriteria;
*/
								}


															}
														} // if channel match.
													} //if not null channel id
												} // if Id match
											} // if not null(s)
											// we call Free() later
											ulCheckingIndex++;
										}  //while((!bFoundLiveEvent)&&(ulCheckingIndex<ulNumLiveEventRecords))

										// we null out later
									} // if((pdbrLiveEventData)&&(ulNumLiveEventRecords))

									// if we didnt find this event, we have to add it!
									if((!bFoundLiveEvent)&&(nLiveEventTableIndex>=0))
									{
										// add it
										CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

										// load up the values.
										for(int i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
										{
											switch(i)
											{
											case DIRECT_LEFIELD_ID://							0
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(13); // varchar 12
													if((pdbcCriteria->m_pdbField[i].m_pszData)&&(pEvent->m_pszReconcileKey))
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 12, "%s", pEvent->m_pszReconcileKey );
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_CHANNEL://				1
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
													if(pdbcCriteria->m_pdbField[i].m_pszData)
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", usChannelID );
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_DATA://						2
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(65); // varchar 64
													if((pdbcCriteria->m_pdbField[i].m_pszData)&&(pEvent->m_pszData))
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 64, "%s", pEvent->m_pszData );
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_ONAIRTIME://		3
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
													if(pdbcCriteria->m_pdbField[i].m_pszData)
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", 
															// unixtime conversion
																(
																 ((pEvent->m_usOnAirJulianDate-25567)*86400)  // number of seconds in a day
																+(pEvent->m_ulOnAirTimeMS/1000)
																)
															);
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_STATUS://					4
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
													if(pdbcCriteria->m_pdbField[i].m_pszData)
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", DIRECT_LESTATUS_EXISTS );
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_DURATION://				5
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(32); // int
													if(pdbcCriteria->m_pdbField[i].m_pszData)
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 31, "%d", pEvent->m_ulDurationMS );
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_CLIPID://					6
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(33); // varchar 32
													if((pdbcCriteria->m_pdbField[i].m_pszData)
														&&(pEvent->m_pszID))
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 
															32, 
															"%s", pEvent->m_pszID );
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_CLIPTITLE://			7
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(65); // varchar 64
													if((pdbcCriteria->m_pdbField[i].m_pszData)&&(pEvent->m_pszTitle))
													{
														_snprintf(pdbcCriteria->m_pdbField[i].m_pszData, 
															64, 
															"%s", pEvent->m_pszTitle );
													}
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_EQUAL;
												} break;
											case DIRECT_LEFIELD_FILES://			8
												{
													pdbcCriteria->m_pdbField[i].m_pszData = NULL; // we figure this out later
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_IGNORE;
												} break;
											case DIRECT_LEFIELD_IP://			9
												{
													pdbcCriteria->m_pdbField[i].m_pszData = NULL; // we figure this out later
													pdbcCriteria->m_pdbField[i].m_ucType = DB_OP_IGNORE;
												} break;
											} // switch
										}


										if(direct.m_db.Insert(pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
										{
											direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:InsertLiveEvents", "Insert returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
										}

										if(pdbcCriteria) delete pdbcCriteria;
									}


/*
									// now we have direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszData.
									char* pch = direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszData;
									if((pch)&&(strlen(pch)))
									{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pch = [%s]", pch);  //(Dispatch message)
Sleep(200);
										// let's see if this event is for us.
										// first we parse the data.
										CSafeBufferUtil sbu;
										char* pchField;
										pchField = sbu.Token(pch, strlen(pch)+1, "|", MODE_SINGLEDELIM);  // include trailing 0
										if((pchField!=NULL)&&(strlen(pchField)>0))  // valid filename
										{
											char pszFilename[MAX_PATH];
											strcpy(pszFilename, pchField);
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pszFilename = [%s]", pszFilename);  //(Dispatch message)
//	Sleep(200);

											pchField = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);  // skip second field.
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchField = [%s]", pchField);  //(Dispatch message)
//	Sleep(200);
											pchField = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);  // third field is keyer
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchField = [%s]", pchField);  //(Dispatch message)
//	Sleep(200);
											if(pchField)
											{
												char* pchKeyer;
												pchKeyer = sbu.Token(pchField, strlen(pchField), ":", MODE_SINGLEDELIM);  // third field is "KEY:7" format
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchKeyer = [%s]", pchKeyer);  //(Dispatch message)
//	Sleep(200);
												pchKeyer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);  // we just want the number!
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchKeyer = [%s]", pchKeyer);  //(Dispatch message)
//	Sleep(200);
												if(pchKeyer)
												{
													unsigned char ucKeyerLayer = (unsigned char) atoi(pchKeyer);
													unsigned short usChannelID = direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_usListID;

													int nDest = direct.m_data.GetDestination(usChannelID, ucKeyerLayer );
	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "ucKeyerLayer = %d, usChannelID = %d, nDest=%d", ucKeyerLayer, usChannelID,nDest );  //(Dispatch message)
	Sleep(200);
													if(nDest>=0)
													{
														//check it for the filename, but only if not already busy
														if(
																(direct.m_ox.m_hinstDLL)
															&&(direct.m_ox.m_bDLLWinsockInit)
															&&(!direct.m_ox.m_bTransferring)
															&&(direct.m_data.m_ppdbDest)
															&&(direct.m_data.m_ulNumDest>0)
															)
														{

//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "ox setting host to %s", direct.m_data.m_ppdbDest[nDest]->pszIP);  //(Dispatch message)
//	Sleep(200);
															// lets determine type.
															// TODO : get type from data base.  for now, we hard code type
															int nReturn = direct.m_ox.SetHost(direct.m_data.m_ppdbDest[nDest]->pszIP);
															if(nReturn<0)
															{
																direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:SetHost", "SetHost returned %d for %s", nReturn, direct.m_data.m_ppdbDest[nDest]->pszIP?direct.m_data.m_ppdbDest[nDest]->pszIP:"(null)");  //(Dispatch message)
															}
															else
															{
																char pszDirAlias[32];

																if(ucKeyerLayer<=2)  // imagestore
																{
		///   GET the video file first
																	sprintf(pszDirAlias, "$VIDEO");
																	sprintf(pszPath, "%s.oxt", pszFilename); // first we look for the video file

																	direct.DealWithFile(pszPath, pszDirAlias, nDest);
																	

		///   NOW the audio file 
																	sprintf(pszDirAlias, "$AUDIO");
																	sprintf(pszPath, "%s.oxw", pszFilename); // first we look for the video file

																	direct.DealWithFile(pszPath, pszDirAlias, nDest);
																	// end with audio file
																}
																else  //intuition
																{
																	sprintf(pszDirAlias, "$VIDEO");
																	sprintf(pszPath, "%s.tem", pszFilename);

																	char* pchFoundFile = NULL;

																	direct.DealWithFile(pszPath, pszDirAlias, nDest, &pchFoundFile);

																	// but now, lets check all the elements.

																	char** ppszChildren = NULL;
																	unsigned long ulNumChildren=0;

																	// should really put a methodology in place to thread this.
	//																either look for the file locally, or thread a transfer

																	if(pchFoundFile) // it's local.
																	{
																		nReturn = direct.m_ox.UtilParseTem(pchFoundFile, &ppszChildren, &ulNumChildren);
																		if(nReturn<0)
																		{
																			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UtilParseTem", "UtilParseTem returned %d for %s", nReturn, direct.m_data.m_ppdbDest[nDest]->pszIP?direct.m_data.m_ppdbDest[nDest]->pszIP:"(null)");  //(Dispatch message)
																		}
																		else
																		{
																			if((ppszChildren)&&(ulNumChildren))
																			{
																				unsigned long ulChild =0;
																				while((ulChild<ulNumChildren)&&(ppszChildren))
																				{
																					if(ppszChildren[ulChild])
																					{
																						direct.DealWithFile(ppszChildren[ulChild], pszDirAlias, nDest);
																						free(ppszChildren[ulChild]);
																					}
																					ulChild++;
																				}
																				if(ppszChildren) delete [] ppszChildren;
																			}
																		}
																		free(pchFoundFile);
																	} //pchFoundFile
																	else  // its remote.
																	{
																		nReturn = direct.m_ox.UtilParseRemoteTem(pszPath, pszDirAlias, &ppszChildren, &ulNumChildren);
																		if(nReturn<0)
																		{
																			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UtilParseRemoteTem", "UtilParseRemoteTem returned %d for %s", nReturn, direct.m_data.m_ppdbDest[nDest]->pszIP?direct.m_data.m_ppdbDest[nDest]->pszIP:"(null)");  //(Dispatch message)
																		}
																		else
																		{
																			if((ppszChildren)&&(ulNumChildren))
																			{
																				unsigned long ulChild =0;
																				while((ulChild<ulNumChildren)&&(ppszChildren))
																				{
																					if(ppszChildren[ulChild])
																					{
																						direct.DealWithFile(ppszChildren[ulChild], pszDirAlias, nDest);
																						free(ppszChildren[ulChild]);
																					}
																					ulChild++;
																				}
																				if(ppszChildren) delete [] ppszChildren;
																			}
																		}
																	}

																}
															}
														}
													} // GetDestination failed.
													else
													{
														// have to report that something is not right
		//TODO
														direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetDestination", 
															"An event on channel %d (%s) has data which does not correspond to any registered IP address.\nThe data for event [%s][%s] was [%s].", 
															direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_usListID,
															direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_pszListName?direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_pszListName:"channel description not available",
															direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszID,
															direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszTitle,
															direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_ppEvents[nEventIndex]->m_pszData
															);  //(Dispatch message)
													}
												}
											}
										}
									}
*/
								}
//								LeaveCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_critGEI);
//								LeaveCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_ppList[nListIndex]->m_critUE);

								nEventIndex++;
							} //while

//* /
						} // if usable channel
					} // if list not null
//* /

//					LeaveCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_critRLI);
//					LeaveCriticalSection (&direct.m_omni.m_ppConn[nConnectionIndex]->m_critUL);

					nListIndex++;
/*
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "incremented nListIndex to %d", nListIndex);  //(Dispatch message)
Sleep(500);
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "really incremented nListIndex to %d", nListIndex);  //(Dispatch message)
Sleep(500);
*/
				}
// * /

LeaveCriticalSection(&direct.m_omni.m_ppConn[nConnectionIndex]->m_crit);


				nConnectionIndex++;
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "incremented nConnectionIndex to %d", nConnectionIndex);  //(Dispatch message)
//Sleep(200);

			}




direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "clearing");  //(Dispatch message)
Sleep(200);


			// OK now we can clear out this:
			if(pdbrLiveEventData!=NULL)
			{
				unsigned long ulRecordIndex=0;
				while(ulRecordIndex<ulNumLiveEventRecords)
				{
					pdbrLiveEventData[ulRecordIndex].Free();  // must free explicitly, per record
					ulRecordIndex++;
				}
				delete [] pdbrLiveEventData;	
			}
			pdbrLiveEventData = NULL;
			ulNumLiveEventRecords=0;
			// and re-use the vars.

			// here, we go thru all the records that dont exist anymore and delete them,
			// and remove the exist flag from the others.
			pdbrLiveEventData=NULL;
			ulNumLiveEventRecords=0;

			if(direct.m_data.m_pdbConnPrimary)
			{
				nLiveEventTableIndex = direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableEvents);
				if(nLiveEventTableIndex>=0)
				{
					CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

					// get them all, but sort by on air time so we deal with the most critical ones first.
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY status, on_air_time", direct.m_settings.m_pszTableEvents);

					//Sleep(500);
					CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
					if(prs == NULL) 
					{
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetLiveEvents", "Retrieve returned NULL\n%s", dberrorstring);  //(Dispatch message)
					}
					else
					{
						// check the items.
//				AfxMessageBox("here");
						int x = direct.m_db.Convert(prs, pdbcCriteria, &pdbrLiveEventData, &ulNumLiveEventRecords, dberrorstring);
//				AfxMessageBox("here2");
						if(x != DB_SUCCESS) 
						{
							if(prs)
							{
								prs->Close();
								delete prs;
							}
							direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetLiveEvents", "Convert returned %d\n%s", x, dberrorstring);  //(Dispatch message)
						}
					}
					if(pdbcCriteria) delete pdbcCriteria;

				}
			}

			if((pdbrLiveEventData)&&(ulNumLiveEventRecords))
			{
				unsigned long ulCheckingIndex=0;
				bool bTransferInProg = false;

				while(ulCheckingIndex<ulNumLiveEventRecords)
				{
					if(
							(pdbrLiveEventData[ulCheckingIndex].m_ppszData)
						&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS])
						)
					{
						unsigned long ulStatus = atol(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS]);
						if(
							  ((ulStatus&DIRECT_LESTATUS_MASK)==DIRECT_LESTATUS_CHK)
							||((ulStatus&DIRECT_LESTATUS_MASK)==DIRECT_LESTATUS_XFER)
							)
						{
							if(!direct.m_ox.m_bTransferring)
							{//done, so remove tag.

								if(direct.m_data.m_pdbConnPrimary)
								{
									if((ulStatus&DIRECT_LESTATUS_MASK)==DIRECT_LESTATUS_XFER)
									{
										int nDest = direct.m_data.GetDestination(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_IP]);
										if(nDest>=0)
										{
											//check it.
											DiskInfo_ disk_info;
											disk_info.KBytes_Free = 0;
											disk_info.KBytes_Total = 0;
											// multi partition, we are just going to check audio and video
											if(direct.m_data.m_ppdbDest[nDest]->ucType == DIRECT_DESTTYPE_IS2)
											{
												direct.m_data.m_ppdbDest[nDest]->bDiskChecked = true;
												DiskInfo_ disk_info_is2;
												if(direct.m_ox.OxSoxGetDriveInfo(VIDEO_DIRECTORY, &disk_info_is2) == OX_SUCCESS)
												{
													disk_info.KBytes_Free += disk_info_is2.KBytes_Free;	// size of free space in KiB
													disk_info.KBytes_Total += disk_info_is2.KBytes_Total;	// size of free space in KiB
												}
												else
												{
													direct.m_data.m_ppdbDest[nDest]->bDiskChecked = false;
												}
												if(direct.m_ox.OxSoxGetDriveInfo(AUDIO_DIRECTORY, &disk_info_is2) == OX_SUCCESS)
												{
													disk_info.KBytes_Free += disk_info_is2.KBytes_Free;	// size of free space in KiB
													disk_info.KBytes_Total += disk_info_is2.KBytes_Total;	// size of free space in KiB
												}
												else
												{
													direct.m_data.m_ppdbDest[nDest]->bDiskChecked = false;
												}
											}
											else // one partition
											if(
													(direct.m_data.m_ppdbDest[nDest]->ucType == DIRECT_DESTTYPE_INT)
												||(direct.m_data.m_ppdbDest[nDest]->ucType == DIRECT_DESTTYPE_ISHD)
												)
											{
												if(direct.m_ox.OxSoxGetDriveInfo(VIDEO_DIRECTORY, &disk_info) == OX_SUCCESS)
												{
													direct.m_data.m_ppdbDest[nDest]->bDiskChecked = true;
												}
											}
											else
											{
												direct.m_data.m_ppdbDest[nDest]->bDiskChecked = true;
											}
											if(direct.m_data.m_ppdbDest[nDest]->bDiskChecked)
											{
												// update DB

												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
													"UPDATE %s SET kb_free = %d, kb_total = %d WHERE IP LIKE '%s'", 
													direct.m_settings.m_pszTableDest,
													disk_info.KBytes_Free,
													disk_info.KBytes_Total,
													direct.m_data.m_ppdbDest[nDest]->pszIP
													);
												if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
												{
										//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
												}
											}
										}
									}




							// remove the tag.
									if(!(ulStatus&DIRECT_LESTATUS_EXISTS))  // but only if we are keeping this event
									{
										// Update!
/*
										if(nLiveEventTableIndex>=0)
										{
											CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);
											CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

											// load up the data.
											int i=0;
											for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
											{
												pdbcCriteria->m_pdbField[i].m_pszData = pdbrLiveEventData[ulCheckingIndex].m_ppszData[i];
												// we are just adding an "exists" thing.
												if(i==DIRECT_LEFIELD_STATUS) // field 4 is status
												{
													ulStatus&=~DIRECT_LESTATUS_MASK;  // removes all status flags, but not exists err, etc.
													pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
													if(pdbcValues->m_pdbField[i].m_pszData)
														sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d", ulStatus );
												}
												else
												{
													pdbcValues->m_pdbField[i].m_pszData = pdbrLiveEventData[ulCheckingIndex].m_ppszData[i];
												}
											}

											if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
											{
												direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
											}
											// null out the fields so as not to free the records
											for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
											{
												pdbcCriteria->m_pdbField[i].m_pszData = NULL;
												if(i!=DIRECT_LEFIELD_STATUS) // field 4 is status, dont NULL it, it will be destroyed on delete.
												{
													pdbcValues->m_pdbField[i].m_pszData = NULL;
												}
											}
											if(pdbcValues) delete pdbcValues;
											if(pdbcCriteria) delete pdbcCriteria;
										}
*/
										if(nLiveEventTableIndex>=0)
										{

											ulStatus&=~DIRECT_LESTATUS_MASK;  // removes all status flags, but not exists err, etc.
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
												"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
												direct.m_settings.m_pszTableEvents,
												ulStatus,
												pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
												pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
												);

											if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
											{
												direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
											}
/*
											CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);
											CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

											// load up the data.
											int i=0;
											for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
											{
												pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
												if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
												{
													pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
													if(pdbcCriteria->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
													pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
												}
												// we are just adding an "exists" thing.
												if(i==DIRECT_LEFIELD_STATUS) // field 4 is status
												{
													ulStatus&=~DIRECT_LESTATUS_MASK;  // removes all status flags, but not exists err, etc.
													pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
													if(pdbcValues->m_pdbField[i].m_pszData)
														sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d",ulStatus );
												}
												else
												{
													pdbcValues->m_pdbField[i].m_ucType=DB_OP_IGNORE;
													if((pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])&&(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])))
													{
														pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(strlen(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i])+1);
														if(pdbcValues->m_pdbField[i].m_pszData) strcpy(pdbcValues->m_pdbField[i].m_pszData, pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
														pdbcValues->m_pdbField[i].m_ucType=DB_OP_EQUAL;
													}
												}
											}

											if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
											{
												direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
											}
											if(pdbcValues) delete pdbcValues;
											if(pdbcCriteria) delete pdbcCriteria;
*/
										}

									}
								}
							}
							else
							{
								bTransferInProg = true;
							}
						}

						if(ulStatus&DIRECT_LESTATUS_EXISTS)
						{
							// remove the tag.
							if(direct.m_data.m_pdbConnPrimary)
							{
								// Update!
/*
								if(nLiveEventTableIndex>=0)
								{
									CDBCriterion* pdbcValues = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);
									CDBCriterion* pdbcCriteria = new CDBCriterion(direct.m_data.m_pdbConnPrimary, nLiveEventTableIndex);

									// load up the data.

									int i=0;
									for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
									{
										pdbcCriteria->m_pdbField[i].m_pszData = pdbrLiveEventData[ulCheckingIndex].m_ppszData[i];
										// we are just adding an "exists" thing.
										if(i==DIRECT_LEFIELD_STATUS) // field 4 is status
										{
											unsigned long ulStatus = atol(pdbrLiveEventData[ulCheckingIndex].m_ppszData[i]);
											ulStatus&=~DIRECT_LESTATUS_EXISTS;
											pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(32);
											if(pdbcValues->m_pdbField[i].m_pszData)
												sprintf(pdbcValues->m_pdbField[i].m_pszData, "%d", ulStatus );
										}
										else
										{
											pdbcValues->m_pdbField[i].m_pszData = pdbrLiveEventData[ulCheckingIndex].m_ppszData[i];
										}
									}

									if(direct.m_db.Update(pdbcValues, pdbcCriteria, dberrorstring)<DB_SUCCESS)  // update a specific record
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
									// null out the fields so as not to free the records
									for(i=0; i<direct.m_data.m_pdbConnPrimary->m_pdbTable[nLiveEventTableIndex].m_usNumFields; i++)
									{
										pdbcCriteria->m_pdbField[i].m_pszData = NULL;
										if(i!=DIRECT_LEFIELD_STATUS) // field 4 is status, dont NULL it, it will be destroyed on delete.
										{
											pdbcValues->m_pdbField[i].m_pszData = NULL;
										}
									}
									if(pdbcValues) delete pdbcValues;
									if(pdbcCriteria) delete pdbcCriteria;
								}
*/

								if(nLiveEventTableIndex>=0)
								{
									unsigned long ulStatus = atol(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS]);
									ulStatus&=~DIRECT_LESTATUS_EXISTS;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}
							}
						}
						else
						{
							// remove the event.
							if(
								  (pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID])
								&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL])
								)
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE id LIKE '%s' AND channel=%s", 
									direct.m_settings.m_pszTableEvents,
									pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
									pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
									);

								//Sleep(500);
								if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
								{
									direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
								}
							}
						}
					}
					ulCheckingIndex++;
				}

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking records");  //(Dispatch message)
Sleep(200);

				// at this point we have updated the DB, but now we can go do something is there is no xfer in prog.
				// and only if there is data (the watchfolder has been init'ed.
				ulCheckingIndex=0;
				while((!bTransferInProg)&&(ulCheckingIndex<ulNumLiveEventRecords)&&(direct.m_data.m_bWatchfolderInitialized)&&(!g_bKillThread))
				{
					if(
							(pdbrLiveEventData[ulCheckingIndex].m_ppszData)
						&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS])
						&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL])
						
						)
					{
						unsigned long ulStatus = atol(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_STATUS]);

						

						if(
								(!(ulStatus&DIRECT_LESTATUS_UNSUPPORTED))  //we dont support
							&&(!(ulStatus&DIRECT_LESTATUS_PLAYED))  // its already done.
							&&(direct.m_data.GetChannelFlag((unsigned short)(atoi(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL])))&DIRECT_CHANNEL_USE)  // ony bother if we are using this channel
							)

						{
							bool bAlterFiles = true;
							switch(ulStatus&DIRECT_LESTATUS_MASK)
							{
							case DIRECT_LESTATUS_NOFILE://				0x00000400  // error - controller doesnt have the file(s).
								{
									// we can check again for the file(s)
									// just a quick check for existence.  if it is there we can start the xfer.
									// it is pritoritized after assets which may need to be moved first.
									bAlterFiles = false; // dont mess with DB with respect to filenames

/*

									if(!(ulStatus&DIRECT_LESTATUS_ERR)) // if we didnt send the error, send the error.
									{
										ulStatus|=DIRECT_LESTATUS_ERR;
										// have to update.

									}
*/
								} //break;  // just flow thru to the "none" case, which checks the file.  all the no file files will be checked again, but after all the nones
							case DIRECT_LESTATUS_NONE://					0x00000000  // no status - a new event
								{
									// ok we need to parse the data to determine if we support it and if we need to do anything with it.
									// here's the meat right here.

									if(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_DATA])
									{
										char* pch = pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_DATA]; // just to abbreviate.

										// data looks like this for Presmaster events:
										// R603|TYPE:VIDEO|KEY:8|
										// there are other Presmaster events that look like this:
										// |MACRO:14|
										// but we do not support these.

										// so, the criteria can be, we support events if they have 3 pipes and 2 colons,
										// and the first pipe-delim filed has to be of non zero length, (in for good measure)
										// and there is a keyer layer. (also in for good measure)

										bool bSupported = false;
										if(strlen(pch)>6) //three pipes, two colons and a non-zero-len event name, and a non-zero-len keyer num
										{
											CBufferUtil bu;
											if(
												  (bu.CountChar(pch, strlen(pch), '|')==3)
												&&(bu.CountChar(pch, strlen(pch), ':')==2)
												)
											{
												// lets continue parsing.
												CSafeBufferUtil sbu;
												char* pchField;
												pchField = sbu.Token(pch, strlen(pch), "|", MODE_SINGLEDELIM); 
												if((pchField!=NULL)&&(strlen(pchField)>0))  // valid filename
												{
													char pszFilename[MAX_PATH];
													strcpy(pszFilename, pchField);
		//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pszFilename = [%s]", pszFilename);  //(Dispatch message)
		//	Sleep(200);

													pchField = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);  // skip second field.
		//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchField = [%s]", pchField);  //(Dispatch message)
		//	Sleep(200);
													pchField = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);  // third field is keyer
		//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchField = [%s]", pchField);  //(Dispatch message)
		//	Sleep(200);
													if(pchField)
													{
														char* pchKeyer;
														pchKeyer = sbu.Token(pchField, strlen(pchField), ":", MODE_SINGLEDELIM);  // third field is "KEY:7" format
		//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchKeyer = [%s]", pchKeyer);  //(Dispatch message)
		//	Sleep(200);
														pchKeyer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);  // we just want the number!
		//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "pchKeyer = [%s]", pchKeyer);  //(Dispatch message)
		//	Sleep(200);
														if(
															  (pchKeyer)
															&&(strlen(pchKeyer))
															&&(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL])
															)
														{
															bSupported = true;
															unsigned char ucKeyerLayer = (unsigned char) atoi(pchKeyer);
															unsigned short usChannelID = atoi(pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]);

															char pchFiles[DIRECT_LEFIELD_FILESWIDTH+1]; // list of files.
															strcpy(pchFiles, "");

															int nDest = direct.m_data.GetDestination(usChannelID, ucKeyerLayer );
//			direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "ucKeyerLayer = %d, usChannelID = %d, nDest=%d", ucKeyerLayer, usChannelID,nDest );  //(Dispatch message)
//			Sleep(200);
															if(nDest>=0)
															{
																// if the ip specific table doenst exist, make it.

								// now update the IP specfic table.
								if(strlen(direct.m_data.m_ppdbDest[nDest]->pszIP))
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "[device_%s]", direct.m_data.m_ppdbDest[nDest]->pszIP);
									if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, szSQL)!=DB_EXISTS)
									{
										// create the table.
										
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"CREATE TABLE [device_%s] (\"filename\" varchar(256) NOT NULL, \"sys_last_used\" int NOT NULL, \"sys_times_used\" int NOT NULL, \"expires_after\" int NULL, \"transfer_date\" int NULL );", 
											direct.m_data.m_ppdbDest[nDest]->pszIP
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
											direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CreateDeviceTable", "CreateDeviceTable returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}
									}
								}


																
																//check it for the filename, but only if not already busy
																if(
																		(direct.m_ox.m_hinstDLL)
																	&&(direct.m_ox.m_bDLLWinsockInit)
																	&&(!direct.m_ox.m_bTransferring)
																	&&(direct.m_data.m_ppdbDest)
																	&&(direct.m_data.m_ulNumDest>0)
																	&&(!g_bKillThread)
																	)
																{

		//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "ox setting host to %s", direct.m_data.m_ppdbDest[nDest]->pszIP);  //(Dispatch message)
		//	Sleep(200);
																	// lets determine type.
																	// TODO : get type from data base.  for now, we hard code type
																	int nReturn = direct.m_ox.SetHost(direct.m_data.m_ppdbDest[nDest]->pszIP);
																	if(nReturn<0)
																	{
																		direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:SetHost", "SetHost returned %d for %s", nReturn, direct.m_data.m_ppdbDest[nDest]->pszIP?direct.m_data.m_ppdbDest[nDest]->pszIP:"(null)");  //(Dispatch message)
																	}
																	else
																	{
																		char pszDirAlias[32];

																		if(ucKeyerLayer<=2)  // imagestore  (or we could check they type field on destination)
																		{
/////////////////////////////////////////////////////////////////////////////////////////////////////////
////  IMAGESTORE
				///   GET the video file first

// have to check for oxe and oxw.  if exist, but no oxa or oxt, must copy and xfer a null OXT
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "imagestore event [%s]", pch);  //(Dispatch message)
Sleep(200);

																			// here we parse the directory aliases and allowed extensions.

																			if(direct.m_data.m_ppdbDest[nDest]->pszExtensions)
																			{
																				bool bVideoFileFound = false;
																				bool bAudioFileFound = false;
																				bool bStandaloneAudioFileFound = false;
																				char* pchPerPartitionInfo 
																					= sbu.Token(
																											direct.m_data.m_ppdbDest[nDest]->pszExtensions,
																											strlen(direct.m_data.m_ppdbDest[nDest]->pszExtensions), 
																											"|", 
																											MODE_SINGLEDELIM
																											);
																				bool bAllFilesFound = true;
																				while(pchPerPartitionInfo!=NULL)
																				{
																					bool bAssetFileFound = false;
																					char	chPartition = 'V';
																					char* pchVidExt=NULL;
																					char* pchPartition = strchr(pchPerPartitionInfo, ':');

																					if(pchPartition)
																					{
																						if(pchPartition>direct.m_data.m_ppdbDest[nDest]->pszExtensions)
																						{
																							switch(*(pchPartition-1))
																							{
																							case 'V':
																							case 'v':
																								{
																									chPartition = 'V';
																									// video partition
																								} break;
																							case 'A':
																							case 'a':
																								{
																									chPartition = 'A';
																									// audio partition.
																								} break;

																								// actually we should never have the fonts thing susppoerted here.  
																								// lets make it a manual push for fonts.
																							case 'F':
																							case 'f':
																								{
																									chPartition = 'F';
																									// audio partition.
																								} break;
																							}
																						}
																						pchPerPartitionInfo = pchPartition+1;
																					}
																					else
																						pchPerPartitionInfo = direct.m_data.m_ppdbDest[nDest]->pszExtensions;

																					switch(chPartition)
																					{
																					case 'V':
																						{
																							// video partition
																							sprintf(pszDirAlias, VIDEO_DIRECTORY);
																						}  break;
																					case 'A':
																						{
																							// audio partition.
																							sprintf(pszDirAlias, AUDIO_DIRECTORY);
																						}  break;
																					case 'F':
																						{
																							// fonts partition.
																							sprintf(pszDirAlias, FONTS_DIRECTORY);
																						}  break;
																					}

																					CSafeBufferUtil sbulocal;
																					pchVidExt 
																						= sbulocal.Token(
																										pchPerPartitionInfo,
																										strlen(pchPerPartitionInfo), 
																										", .*", 
																										MODE_MULTIDELIM
																										);
																					while(pchVidExt!=NULL)
																					{
																						// look for a thing with this extension.
																						sprintf(pszPath, "%s.%s", pszFilename, pchVidExt); // first we look for the video file
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking imagestore event [%s][%s]", pszPath, pszDirAlias);  //(Dispatch message)
Sleep(200);

																						if(direct.Miranda_CheckFileExists(pszPath, pszDirAlias, nDest)==DIRECT_SUCCESS)
																						{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "found imagestore event [%s][%s]", pszPath, pszDirAlias);  //(Dispatch message)
Sleep(200);
																							bAssetFileFound = true;
																							if(chPartition == 'V') bVideoFileFound = true;
																							if(chPartition == 'A')
																							{
																								// have to do the following, in case there's a wav file,
																								// which we can allow all alone
																								if(
																									  (stricmp(pchVidExt, "oxw")==0)  // imagestore 2
																									||(stricmp(pchVidExt, "oxe")==0)  // imagestore 300
																									)
																								{

																									bAudioFileFound = true;
																								}
																								if(stricmp(pchVidExt, "wav")==0)  // easyplay option.
																									bStandaloneAudioFileFound = true;
																							}

																							// add to list.
																							if((strlen(pchFiles))&&(bAlterFiles))
																							{
																								char pchFilesTemp[DIRECT_LEFIELD_FILESWIDTH+1];
																								strcpy(pchFilesTemp, pchFiles);
																								_snprintf(pchFiles, DIRECT_LEFIELD_FILESWIDTH, "%s|%s", pchFilesTemp, pszPath);
																							}
																							else
																							{
																								strcpy(pchFiles, pszPath);
																							}


																							break; // move on, we just need to find a single one.
																						}

																						pchVidExt 
																						= sbulocal.Token(
																										NULL,
																										NULL, 
																										", .*", 
																										MODE_MULTIDELIM
																										);
																					}

																					if(!bAssetFileFound)
																					{
																						bAllFilesFound = false;
																						// go around again, find the file to push..
																						pchVidExt 
																							= sbulocal.Token(
																											pchPerPartitionInfo,
																											strlen(pchPerPartitionInfo), 
																											", .*", 
																											MODE_MULTIDELIM
																											);
																						while(pchVidExt!=NULL)
																						{
																							// look for a thing with this extension.
																							sprintf(pszPath, "%s.%s", pszFilename, pchVidExt); // first we look for the video file

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling FindFile for imagestore event [%s]", pszPath);  //(Dispatch message)
Sleep(200);
																							char* pchFoundFile = NULL;
																							if( direct.FindFile(
																											pszPath, 
																											direct.m_settings.m_pszPrimaryDestinationFolderPath, 
																											&pchFoundFile )
																									== DIRECT_SUCCESS 
																								)
																							{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "FindFile found imagestore event [%s] at [%s]", pszPath, pchFoundFile);  //(Dispatch message)
Sleep(200);

																								bAssetFileFound = true;
																								// push the file.
																								if( direct.Miranda_PushFile(
																											pchFoundFile, 
																											pszPath, pszDirAlias, nDest) == DIRECT_SUCCESS)
																								{
																								// report errors inside the function.
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Miranda_PushFile pushed [%s]->[%s][%s]",pchFoundFile,  pszPath,pszDirAlias);  //(Dispatch message)
Sleep(200);
																								
																							ulStatus &= ~DIRECT_LESTATUS_MASK;
																							ulStatus|=DIRECT_LESTATUS_XFER;
																								bTransferInProg = true;
																								// have to update the database tables.

								if(nLiveEventTableIndex>=0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}


								}



								// now update the IP specfic table.
								if(strlen(direct.m_data.m_ppdbDest[nDest]->pszIP))
								{
									bool bRecordExists = false;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "[device_%s]", direct.m_data.m_ppdbDest[nDest]->pszIP);
									if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, szSQL)!=DB_EXISTS)
									{
										// create the table.
										
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"CREATE TABLE [device_%s] (\"filename\" varchar(256) NOT NULL, \"sys_last_used\" int NOT NULL, \"sys_times_used\" int NOT NULL, \"expires_after\" int NULL, \"transfer_date\" int NULL );", 
											direct.m_data.m_ppdbDest[nDest]->pszIP
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
											direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CreateDeviceTable", "CreateDeviceTable returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}
									}
									else
									{
										// have to check if it's there already.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [device_%s] WHERE filename LIKE '%s'", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath
											);

										//Sleep(500);
										CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
										if(prs != NULL) 
										{
											// update.
											if(!prs->IsEOF())
											{
												prs->Close();
												delete prs;
												prs = NULL;

												// there is a record.
												bRecordExists = true;
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
													"UPDATE [device_%s] SET transfer_date = %d WHERE filename LIKE '%s'", 
													direct.m_data.m_ppdbDest[nDest]->pszIP,
													(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)),
													pszPath
													);
												if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
												{
										//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
												}
											}
											if(prs)
											{
												prs->Close();
												delete prs;
											}
										}

									}

									if(!bRecordExists)
									{
										//insert one.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"INSERT INTO [device_%s] (filename, sys_last_used, sys_times_used, expires_after, transfer_date) VALUES ('%s', %d, %d, %d, %d);", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath,
											0,0,0,  // expire gets fixed later, after it plays once.  TODO - fix later.
											(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))
											
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}

									}
								}




																									bAssetFileFound = true;
																									if(chPartition == 'V') bVideoFileFound = true;
																									if(chPartition == 'A')
																									{
																										// have to do the following, in case there's a wav file,
																										// which we can allow all alone
																										if(
																												(stricmp(pchVidExt, "oxw")==0)  // imagestore 2
																											||(stricmp(pchVidExt, "oxe")==0)  // imagestore 300
																											)
																										{
																											bAudioFileFound = true;
																										}
																										if(stricmp(pchVidExt, "wav")==0)  // easyplay option.
																											bStandaloneAudioFileFound = true;
																									}

																									break; // move on, we just need to find a single one.
																								}
																							}


																							pchVidExt 
																								= sbulocal.Token(
																												NULL,
																												NULL, 
																												", .*", 
																												MODE_MULTIDELIM
																												);
																						}

																						if(!bAssetFileFound)
																						{
																							if((chPartition == 'A')&&((!bAudioFileFound)||(!bStandaloneAudioFileFound)))
																							{
																								// that's OK, just continue. we dont NEED audio.
																								// simply move on, check for Video.
																								// reset this
																								bAssetFileFound = true;
																								bAllFilesFound = true;
																							}
																							else
																							if((chPartition == 'V')&&(bStandaloneAudioFileFound))
																							{
																								// that's OK, just continue. we dont NEED video in this case.
																								bAssetFileFound = true;
																								bAllFilesFound = true;
																							}
																							else
																							if(
																								  (chPartition == 'V')&&(bAudioFileFound)
																								&&(direct.m_settings.m_bUseNullFile)
																								&&(direct.m_settings.m_pszNullFile)&&(strlen(direct.m_settings.m_pszNullFile))
																								&&(direct.m_settings.m_pszSystemFolderPath)&&(strlen(direct.m_settings.m_pszSystemFolderPath))
																								)
																							{
																								// need to put a video file there, use the null image.
																								// first verify the null file is there
																								// if so, push it with the right filename.
																								// if all successfull, set bAllFilesFound = true;

																								char nullfilepath[MAX_PATH];
																								sprintf(nullfilepath, "%s\\%s",
																									direct.m_settings.m_pszSystemFolderPath,
																									direct.m_settings.m_pszNullFile);

																								sprintf(pszPath, "%s.oxt", pszFilename);
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Miranda_PushFile pushing [%s]->[%s][%s]",nullfilepath,  pszPath,pszDirAlias);  //(Dispatch message)
Sleep(200);

																								if(direct.Miranda_PushFile(
																											nullfilepath, 
																											pszPath, pszDirAlias, nDest)==DIRECT_SUCCESS)
																								{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Miranda_PushFile pushed [%s]->[%s][%s]",nullfilepath,  pszPath,pszDirAlias);  //(Dispatch message)
Sleep(200);


																							ulStatus &= ~DIRECT_LESTATUS_MASK;
																							ulStatus|=DIRECT_LESTATUS_XFER;
																								bTransferInProg = true;
																								// have to update the database tables.

								if(nLiveEventTableIndex>=0)
								{

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}

								}



								// now update the IP specfic table.
								if(strlen(direct.m_data.m_ppdbDest[nDest]->pszIP))
								{
									bool bRecordExists = false;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "[device_%s]", direct.m_data.m_ppdbDest[nDest]->pszIP);
									if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, szSQL)!=DB_EXISTS)
									{
										// create the table.
										
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"CREATE TABLE [device_%s] (\"filename\" varchar(256) NOT NULL, \"sys_last_used\" int NOT NULL, \"sys_times_used\" int NOT NULL, \"expires_after\" int NULL, \"transfer_date\" int NULL );", 
											direct.m_data.m_ppdbDest[nDest]->pszIP
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CreateDeviceTable", "CreateDeviceTable returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}
									}
									else
									{
										// have to check if it's there already.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [device_%s] WHERE filename LIKE '%s'", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath
											);

										//Sleep(500);
										CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
										if(prs != NULL) 
										{
											// update.
											if(!prs->IsEOF())
											{
												prs->Close();
												delete prs;
												prs = NULL;
												// there is a record.
												bRecordExists = true;
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
													"UPDATE [device_%s] SET transfer_date = %d WHERE filename LIKE '%s'", 
													direct.m_data.m_ppdbDest[nDest]->pszIP,
													(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)),
													pszPath
													);
												if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
												{
										//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
												}
											}
											if(prs)
											{
												prs->Close();
												delete prs;
											}
										}
									}

									if(!bRecordExists)
									{
										//insert one.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"INSERT INTO [device_%s] (filename, sys_last_used, sys_times_used, expires_after, transfer_date) VALUES ('%s', %d, %d, %d, %d);", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath,
											0,0,0,  // expire gets fixed later, after it plays once.  TODO - fix later.
											(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))
											
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}

									}
								}

																									bAssetFileFound = true;

																								}// else the push didnt work for some reason
																								else
																								{
																									bAssetFileFound = false;
																								}

																							}

																							if(!bAssetFileFound)
																							 // its really not found.
																							{
																								bAllFilesFound = false;
																								
																							// we didnt find any assets in the store that match this event.
																							// report and update with error


																								// send the error out.
																								if(!(ulStatus&DIRECT_LESTATUS_ERR)) // if we didnt send the error, send the error.
																								{
																									// have to update.
																									direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:FindFile", "No file [%s] was found.", pszPath);  //(Dispatch message)
																									direct.SendError("Direct:FindFile", "No file [%s] was found.",pszPath);
																								}

																								ulStatus|=(DIRECT_LESTATUS_NOFILE|DIRECT_LESTATUS_ERR);
								if(nLiveEventTableIndex>=0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}



																							}// its really not found.
																						}//if(!bAssetFileFound)
																					}//if(!bAssetFileFound)

																					pchPerPartitionInfo = sbu.Token(NULL,	NULL, "|", MODE_SINGLEDELIM	);
																				} // while partitions left.

																				if( bAllFilesFound )
																				{
																					// well then, we are all set!

																					
																					ulStatus &= ~DIRECT_LESTATUS_MASK;
																					ulStatus |= DIRECT_LESTATUS_SET;
	//																				direct.SendMsg("Direct:GetLiveEvents","Retrieve returned NULL\n%s", dberrorstring);

																					// update the DB!

								if(nLiveEventTableIndex>=0)
								{

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}



																				}//if( bAllFilesFound )
																				// else we dealt with the action above.

																			} //if(direct.m_data.m_ppdbDest[nDest]->pszExtensions)
																		}
																		else  //intuition
/////////////////////////////////////////////////////////////////////////////////////////////////////////
////  INTUITION


																		{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "intuition event [%s]", pch);  //(Dispatch message)
Sleep(200);
																			// here we parse the directory aliases and allowed extensions, should only be *.tem on video partition, then elements follow.
																			// we have to check the *.tem file for elements.

																			sprintf(pszDirAlias, VIDEO_DIRECTORY);
																			sprintf(pszPath, "%s.tem", pszFilename);  // this is the trigger file for intuition, so can hardcode.

																			strcpy(pchFiles, pszPath);

																			bool bAllFilesFound = true;
																			bool bAssetFileFound = true;
																			char parsefile[MAX_PATH+1];
																			_snprintf(parsefile, MAX_PATH, "%s\\%s.dpf", direct.m_settings.m_pszSystemFolderPath, pszFilename);  //direct parse file.
																			char localfile[MAX_PATH+1];
																			_snprintf(localfile, MAX_PATH, "%s\\%s.tem", direct.m_settings.m_pszSystemFolderPath, pszFilename);  //get the template file
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling Miranda_CheckFileExists [%s][%s]", pszPath, pszDirAlias);  //(Dispatch message)
Sleep(200);

																			if(direct.Miranda_CheckFileExists(pszPath, pszDirAlias, nDest)==DIRECT_SUCCESS)
																			{
																				// great, now we need to find it locally, if we dont see a parse file in the sys folder already.
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Miranda_CheckFileExists [%s][%s] succeeded, parsing [%s]", pszPath, pszDirAlias, parsefile);  //(Dispatch message)
Sleep(200);
																				FILE* fp = fopen(parsefile, "rb");
																				if(fp!=NULL)
																				{
																					// parse file was was there!
																					fseek(fp, 0, SEEK_END);
																					unsigned long ulFileLen = ftell(fp);

																					char* pchParse = (char*) malloc(ulFileLen+1); // term zero
																					if(pchParse)
																					{
																						fseek(fp, 0, SEEK_SET);
																						fread(pchParse, sizeof(char), ulFileLen, fp);
																						*(pchParse+ulFileLen) = 0; // term zero

																					}

																					fclose(fp);

																					if(pchParse)
																					{
																						//ok, lets parse the buffer.
																						CSafeBufferUtil sbulocal;
																						char* pchChild 
																							= sbulocal.Token(
																											pchParse,
																											strlen(pchParse), 
																											"\r\n", 
																											MODE_MULTIDELIM
																											);
																						while((pchChild)&&(bAllFilesFound))
																						{
																							//pchChild points to an element filename.
																							// have to find proper Dir alias for it.

																							char	chPartition = 'V';
																							char* pchVidExt= strrchr(pchChild, '.'); // find the extension
																							if(pchVidExt)
																							{
																								if(pchVidExt<(pchChild+strlen(pchChild-2))) // the dot, plus at least one char ext
																								{
																									pchVidExt++;

																									if(direct.m_data.m_ppdbDest[nDest]->pszExtensions)
																									{

																										// lets make this all lowercase.
																										int g = 0;
																										while((*(pchVidExt+g) != '\n')&&(*(pchVidExt+g) != '\r'))
																										{
																											char ch = _tolower(*(pchVidExt+g));
																											*(pchVidExt+g) = ch;
																											g++;
																										}

																										char* pchTarget = strstr(direct.m_data.m_ppdbDest[nDest]->pszExtensions, pchVidExt);
																										if(!pchTarget)
																										{
																											// didnt find
																											// so try uppercase.
																											g = 0;
																											while((*(pchVidExt+g) != '\n')&&(*(pchVidExt+g) != '\r'))
																											{
																												char ch = _toupper(*(pchVidExt+g));
																												*(pchVidExt+g) = ch;
																												g++;
																											}

																											pchTarget = strstr(direct.m_data.m_ppdbDest[nDest]->pszExtensions, pchVidExt);
																										}

																										if(pchTarget)
																										{
																											while(pchTarget>direct.m_data.m_ppdbDest[nDest]->pszExtensions)
																											{
																												pchTarget--;
																												if(*(pchTarget+1) == ':')
																												{
																													// we found a partition 
																													switch(*pchTarget)
																													{
																													case 'V':
																													case 'v':
																														{
																															chPartition = 'V';
																															// video partition
																														} break;
																													case 'A':
																													case 'a':
																														{
																															chPartition = 'A';
																															// audio partition.
																														} break;
																													case 'F':
																													case 'f':
																														{
																															chPartition = 'F';
																															// fonts partition.
																														} break;
																													case 'R':
																													case 'r':
																														{
																															chPartition = 'R';
																															// root partition.
																														} break;
																													case 'C':
																													case 'c':
																														{
																															chPartition = 'C';
																															// code partition.
																														} break;
																													case 'P':
																														{
																															chPartition = 'P';
																															// param partition.
																														} break;
																													case 'p':
																														{
																															chPartition = 'p';
																															// panel partition.
																														} break;
																													case 'T':
																													case 't':
																														{
																															chPartition = 'T';
																															// thumbs partition.
																														} break;
																													case 'L':
																														{
																															chPartition = 'L';
																															// playList partition.
																														} break;
																													case 'l':
																														{
																															chPartition = 'l';
																															// LAS partition.
																														} break;
																													} //switch

/*
// these are the possibilities
#define HARD_DISK0_ROOT_NAME   "$ROOT"
#define VIDEO_DIRECTORY        "$VIDEO"
#define AUDIO_DIRECTORY        "$AUDIO"
#define CODE_DIRECTORY         "$CODE"
#define PARAM_DIRECTORY        "$PARAM"
#define LAS_DIRECTORY          "$LAS"
#define PAN_DIRECTORY          "$PANEL"
#define BMP_DIRECTORY          "$THUMBS"
#define PLAYLIST_DIRECTORY     "$PLAYLIST"
#define FONTS_DIRECTORY        "$FONTS"
*/
																												} //if(*(pchTarget+1) == ':')
																											}//while(pchTarget>direct.m_data.m_ppdbDest[nDest]->pszExtensions)
																										}//if(pchTarget)
																									}//if(direct.m_data.m_ppdbDest[nDest]->pszExtensions)
																								}//if(pchVidExt<(pchChild+strlen(pchChild-2)))
																							}//if(pchVidExt)

																							// get the right dir alias.
																							switch(chPartition)
																							{
																							case 'V':
																								{
																									// video partition
																									sprintf(pszDirAlias, VIDEO_DIRECTORY);
																								}  break;
																							case 'A':
																								{
																									// audio partition.
																									sprintf(pszDirAlias, AUDIO_DIRECTORY);
																								}  break;
																							case 'F':
																								{
																									// fonts partition.
																									sprintf(pszDirAlias, FONTS_DIRECTORY);
																								}  break;
																							case 'R':
																								{
																									// root partition.
																									sprintf(pszDirAlias, HARD_DISK0_ROOT_NAME);
																								}  break;
																							case 'C':
																								{
																									// code partition.
																									sprintf(pszDirAlias, CODE_DIRECTORY);
																								}  break;
																							case 'P':
																								{
																									// params partition.
																									sprintf(pszDirAlias, PARAM_DIRECTORY);
																								}  break;
																							case 'p':
																								{
																									// panel partition.
																									sprintf(pszDirAlias, PAN_DIRECTORY);
																								}  break;
																							case 'T':
																								{
																									// thumbs partition.
																									sprintf(pszDirAlias, BMP_DIRECTORY);
																								}  break;
																							case 'L':
																								{
																									// playList partition.
																									sprintf(pszDirAlias, PLAYLIST_DIRECTORY);
																								} break;
																							case 'l':
																								{
																									// LAS partition.
																									sprintf(pszDirAlias, LAS_DIRECTORY);
																								} break;
																							}  //switch
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling Miranda_CheckFileExists [%s][%s]", pchChild, pszDirAlias);  //(Dispatch message)
Sleep(200);

																							if(direct.Miranda_CheckFileExists(pchChild, pszDirAlias, nDest)!=DIRECT_SUCCESS)
																							{
																								bAllFilesFound=false;
																								// have to find it and transfer it.
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling Miranda_CheckFileExists [%s][%s] returned no file", pchChild, pszDirAlias);  //(Dispatch message)
Sleep(200);


																								char* pchFoundFile = NULL;
																								if( direct.FindFile(
																												pchChild, 
																												direct.m_settings.m_pszPrimaryDestinationFolderPath, 
																												&pchFoundFile )
																										== DIRECT_SUCCESS 
																									)
																								{											
																									bAssetFileFound = true;

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "FindFile returned file [%s]", pchFoundFile);  //(Dispatch message)
Sleep(200);
																									// push the file.
																									if(direct.Miranda_PushFile(
																												pchFoundFile, 
																												pszPath, pszDirAlias, nDest)==DIRECT_SUCCESS)
																									{
																									// report errors inside the function.
																									
																									ulStatus &= ~DIRECT_LESTATUS_MASK;
																									ulStatus|=DIRECT_LESTATUS_XFER;
																										bTransferInProg = true;
																									// have to update the database.
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Miranda_PushFile returned true for [%s]->[%s][%s]", pchFoundFile, pszPath, pszDirAlias);  //(Dispatch message)
Sleep(200);

								if(nLiveEventTableIndex>=0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}


								// now update the IP specfic table.
								if(strlen(direct.m_data.m_ppdbDest[nDest]->pszIP))
								{
									bool bRecordExists = false;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "[device_%s]", direct.m_data.m_ppdbDest[nDest]->pszIP);
									if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, szSQL)!=DB_EXISTS)
									{
										// create the table.
										
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"CREATE TABLE [device_%s] (\"filename\" varchar(256) NOT NULL, \"sys_last_used\" int NOT NULL, \"sys_times_used\" int NOT NULL, \"expires_after\" int NULL, \"transfer_date\" int NULL );", 
											direct.m_data.m_ppdbDest[nDest]->pszIP
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
											direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CreateDeviceTable", "CreateDeviceTable returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}
									}
									else
									{
										// have to check if it's there already.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [device_%s] WHERE filename LIKE '%s'", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath
											);

										//Sleep(500);
										CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
										if(prs != NULL) 
										{
											// update.
											if(!prs->IsEOF())
											{
												prs->Close();
												delete prs;
												prs = NULL;
												// there is a record.
												bRecordExists = true;
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
													"UPDATE [device_%s] SET transfer_date = %d WHERE filename LIKE '%s'", 
													direct.m_data.m_ppdbDest[nDest]->pszIP,
													(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)),
													pszPath
													);
												if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
												{
										//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
												}

											}
											if(prs)
											{
												prs->Close();
												delete prs;
											}
										}
									}

									if(!bRecordExists)
									{
										//insert one.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"INSERT INTO [device_%s] (filename, sys_last_used, sys_times_used, expires_after, transfer_date) VALUES ('%s', %d, %d, %d, %d);", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath,
											0,0,0,  // expire gets fixed later, after it plays once.  TODO - fix later.
											(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))
											
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}

									}
								}






																									}// else the push didnt work for some reason
																									else
																									{
																										bAssetFileFound = false;
																									}

																								}
																								else
																								{	
																									bAssetFileFound = false;
																								}

																								if(!bAssetFileFound)
																								{
																									//no file!
																									// send the error out.
																									if(!(ulStatus&DIRECT_LESTATUS_ERR)) // if we didnt send the error, send the error.
																									{
																										// have to update.
																										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:FindFile", "No file [%s] was found.", pszPath);  //(Dispatch message)
																										direct.SendError("Direct:FindFile", "No file [%s] was found.",pszPath);
																									}

																									ulStatus|=(DIRECT_LESTATUS_NOFILE|DIRECT_LESTATUS_ERR);

																									
								if(nLiveEventTableIndex>=0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}


																								}
																							}
																							// else it was on the miranda, great. move on.

																							pchChild 
																								= sbulocal.Token(
																												NULL,
																												NULL, 
																												"\r\n", 
																												MODE_MULTIDELIM
																												);

																						}
																						free(pchParse);
																					}
																					else
																					{
																					 //	memory error, could not allocate buffer
																						//error.
																					}
																				}
																				else // the parse file was not there, so we must create one.
																				{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "no parse file, need to create one");  //(Dispatch message)
Sleep(200);
																					char* pchFoundFile = NULL;
																					if( direct.FindFile(
																									pszPath, 
																									direct.m_settings.m_pszPrimaryDestinationFolderPath, 
																									&pchFoundFile )
																							== DIRECT_SUCCESS 
																						)
																					{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "FindFile returned true for [%s] at [%s]", pszPath, pchFoundFile);  //(Dispatch message)
Sleep(200);

																						// lets parse it out into in the sys folder.
																						char** ppszChildren = NULL;
																						unsigned long ulNumChildren=0;

																						if(pchFoundFile) // it's local.
																						{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling UtilParseTem [%s]", pchFoundFile);  //(Dispatch message)
Sleep(200);

																							nReturn = direct.m_ox.UtilParseTem(pchFoundFile, &ppszChildren, &ulNumChildren);
																							if(nReturn<OX_SUCCESS)
																							{
																								direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UtilParseTem", "UtilParseTem returned %d for %s", nReturn, direct.m_data.m_ppdbDest[nDest]->pszIP?direct.m_data.m_ppdbDest[nDest]->pszIP:"(null)");  //(Dispatch message)
																							}
																							else
																							{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "UtilParseTem [%s] succeeded", pchFoundFile);  //(Dispatch message)
Sleep(200);
																								if((ppszChildren)&&(ulNumChildren))
																								{
																									fp = fopen(parsefile, "wb");
																									if(!fp)
																									{
																										//error
																										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:TemParse", "Could not open template parse file %s", parsefile);  //(Dispatch message)
																									}
																									unsigned long ulChild =0;
																									while((ulChild<ulNumChildren)&&(ppszChildren))
																									{
																										if(ppszChildren[ulChild])
																										{
																											if(fp)
																											{
																												fprintf(fp, "%s\n", ppszChildren[ulChild]);
																												fflush(fp);
																											}
																											// also write it to the DB
																											char pchFilesTemp[DIRECT_LEFIELD_FILESWIDTH+1];
																											strcpy(pchFilesTemp, ppszChildren[ulChild]);
																											_snprintf(pchFiles, DIRECT_LEFIELD_FILESWIDTH, "%s|%s", pchFilesTemp, pszPath);

																											free(ppszChildren[ulChild]);
																										}
																										
																										ulChild++;
																									}
																									if(fp) fclose(fp);

																									if(ppszChildren) delete [] ppszChildren;
																								}
																							}
																							free(pchFoundFile);
																						} //pchFoundFile
																						else  // its remote, we have to get it..
																						{
																							// get the tem file.
																							char localfile[MAX_PATH+1];
																							_snprintf(localfile, MAX_PATH, "%s\\%s.tem", direct.m_settings.m_pszSystemFolderPath, pszFilename);  //get the template file

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling OxSoxGetFile [%s][%s]->[%s] succeeded", pszPath, pszDirAlias,localfile);  //(Dispatch message)
Sleep(200);

																							int nRV = direct.m_ox.OxSoxGetFile(localfile, pszPath, pszDirAlias);
																							if(nRV == OX_SUCCESS)
																							{
																								ulStatus &= ~DIRECT_LESTATUS_MASK;
																								ulStatus|=DIRECT_LESTATUS_XFER;
																								bTransferInProg = true;
																									// have to update the database.

								if(nLiveEventTableIndex>=0)
								{

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}


								// dont update the IP table, that is only for transferring TO the unit


								// now update the IP specfic table.
								if(strlen(direct.m_data.m_ppdbDest[nDest]->pszIP))
								{
									bool bRecordExists = false;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "[device_%s]", direct.m_data.m_ppdbDest[nDest]->pszIP);
									if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, szSQL)!=DB_EXISTS)
									{
										// create the table.
										
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"CREATE TABLE [device_%s] (\"filename\" varchar(256) NOT NULL, \"sys_last_used\" int NOT NULL, \"sys_times_used\" int NOT NULL, \"expires_after\" int NULL, \"transfer_date\" int NULL );", 
											direct.m_data.m_ppdbDest[nDest]->pszIP
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CreateDeviceTable", "CreateDeviceTable returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}
									}
									else
									{
										// have to check if it's there already.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [device_%s] WHERE filename LIKE '%s'", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath
											);

										//Sleep(500);
										CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
										if(prs != NULL) 
										{
											// update.
											if(!prs->IsEOF())
											{
												// there is a record.
												prs->Close();
												delete prs;
												prs = NULL;

												bRecordExists = true;
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
													"UPDATE [device_%s] SET transfer_date = %d WHERE filename LIKE '%s'", 
													direct.m_data.m_ppdbDest[nDest]->pszIP,
													(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)),
													pszPath
													);
												if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
												{
										//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
												}
											}
											if(prs)
											{
												prs->Close();
												delete prs;
											}
										}
									}

									if(!bRecordExists)
									{
										//insert one.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"INSERT INTO [device_%s] (filename, sys_last_used, sys_times_used, expires_after, transfer_date) VALUES ('%s', %d, %d, %d, %d);", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath,
											0,0,0,  // expire gets fixed later, after it plays once.  TODO - fix later.
											(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))
											
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}

									}
								}
																							}  // else not on oxtel, have to error out.
																							else
																							{
																								//?
																							}
																						} // else remote, we are getting
																					}
																					else // could not find it!
																					{ // find file failed in the storage area.
																						// check the system folder
																						FILE* fptem = fopen(localfile, "rb");
																						if(fptem)
																						{
																							fclose(fptem); // its there
																							// lets parse it out into in the sys folder.
																							char** ppszChildren = NULL;
																							unsigned long ulNumChildren=0;

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling UtilParseTem [%s]", localfile);  //(Dispatch message)
Sleep(200);
																							nReturn = direct.m_ox.UtilParseTem(localfile, &ppszChildren, &ulNumChildren);
																							if(nReturn<OX_SUCCESS)
																							{
																								direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UtilParseTem", "UtilParseTem returned %d for %s", nReturn, direct.m_data.m_ppdbDest[nDest]->pszIP?direct.m_data.m_ppdbDest[nDest]->pszIP:"(null)");  //(Dispatch message)
																							}
																							else
																							{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "UtilParseTem [%s], parsing [%s]", localfile, parsefile);  //(Dispatch message)
Sleep(200);

																								FILE* fp = fopen(parsefile, "wb");
																								if(!fp)
																								{
																									//error
																									direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:TemParse", "Could not open template parse file %s", parsefile);  //(Dispatch message)
																								}
																								unsigned long ulChild =0;
																								while((ulChild<ulNumChildren)&&(ppszChildren))
																								{
																									if(ppszChildren[ulChild])
																									{
																										if(fp)
																										{
																											fprintf(fp, "%s\n", ppszChildren[ulChild]);
																											fflush(fp);
																										}

																										// also write it to the DB
																										char pchFilesTemp[DIRECT_LEFIELD_FILESWIDTH+1];
																										strcpy(pchFilesTemp, ppszChildren[ulChild]);
																										_snprintf(pchFiles, DIRECT_LEFIELD_FILESWIDTH, "%s|%s", pchFilesTemp, pszPath);

																										free(ppszChildren[ulChild]);
																									}
																									
																									ulChild++;
																								}
																								if(fp) fclose(fp);

																								if(ppszChildren) delete [] ppszChildren;
																							}

																						}
																						else
																						{
																							// this file is nowhere.
																							// but if we are here, we should have been able to transfer the thing previously.
																							// lets just ignore it, try again next time.
																						}




																						// error
																					} // file find succeeded
																				}//else // the parse file was not there, so we must create one.
																			}
																			else
																			{
																				// the template file wasn't found on the miranda box. now we need to find it locally and push it.
																				bAllFilesFound = false;
																				bool bError = false;
																				char* pchFoundFile = NULL;

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling FindFile [%s]",  pszPath);  //(Dispatch message)
Sleep(200);

																				if( direct.FindFile(
																								pszPath, 
																								direct.m_settings.m_pszPrimaryDestinationFolderPath, 
																								&pchFoundFile )
																						== DIRECT_SUCCESS 
																					)
																				{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "FindFile returned success");  //(Dispatch message)
Sleep(200);
																					if(pchFoundFile) // it's local.
																					{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "FindFile found [%s] at [%s]",  pszPath, pchFoundFile);  //(Dispatch message)
Sleep(200);


																						// push the file.

																						// have to determine dir alias by extension.
																						/*
																						// take following from DB
																						sprintf(pszDirAlias, VIDEO_DIRECTORY); // default.
																						char* pchExt = strrchr(pchFoundFile, '.');
																						if(pchExt)
																						{
																							if(
																								  (stricmp(pchExt,".oxw")==0)
																								||(stricmp(pchExt,".wav")==0)
																								||(stricmp(pchExt,".oxe")==0)
																								)
																							{
																								sprintf(pszDirAlias, AUDIO_DIRECTORY);
																							}
																							else
																							if(stricmp(pchExt,".ttf")==0)
																							{
																									sprintf(pszDirAlias, FONTS_DIRECTORY);
																							}
																							else
																							if(stricmp(pchExt,".bmp")==0)
																							{
																									sprintf(pszDirAlias, BMP_DIRECTORY);
																							}

																						}
																						*/
																						// push the file.
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "calling Miranda_PushFile [%s] -> [%s][%s]",  pchFoundFile, pszPath, pszDirAlias);  //(Dispatch message)
Sleep(200);
																						int nRV = direct.Miranda_PushFile(
																									pchFoundFile, 
																									pszPath, pszDirAlias, nDest);
																						if(nRV==DIRECT_SUCCESS)
																						{
																						// report errors inside the function.
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Miranda_PushFile succeded [%s] -> [%s][%s]",  pchFoundFile, pszPath, pszDirAlias);  //(Dispatch message)
Sleep(200);
																						
																						ulStatus &= ~DIRECT_LESTATUS_MASK;
																						ulStatus|=DIRECT_LESTATUS_XFER;
																						bTransferInProg = true;
																						// have to update the database.

								if(nLiveEventTableIndex>=0)
								{

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}



								// now update the IP specfic table.
								if(strlen(direct.m_data.m_ppdbDest[nDest]->pszIP))
								{
									bool bRecordExists = false;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "[device_%s]", direct.m_data.m_ppdbDest[nDest]->pszIP);
									if(direct.m_db.TableExists(direct.m_data.m_pdbConnPrimary, szSQL)!=DB_EXISTS)
									{
										// create the table.
										
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"CREATE TABLE [device_%s] (\"filename\" varchar(256) NOT NULL, \"sys_last_used\" int NOT NULL, \"sys_times_used\" int NOT NULL, \"expires_after\" int NULL, \"transfer_date\" int NULL );", 
											direct.m_data.m_ppdbDest[nDest]->pszIP
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:CreateDeviceTable", "CreateDeviceTable returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}
									}
									else
									{
										// have to check if it's there already.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [device_%s] WHERE filename LIKE '%s'", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath
											);

										//Sleep(500);
										CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
										if(prs != NULL) 
										{
											// update.
											if(!prs->IsEOF())
											{
												
												prs->Close();
												delete prs;
												prs = NULL;

												// there is a record.
												bRecordExists = true;
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
													"UPDATE [device_%s] SET transfer_date = %d WHERE filename LIKE '%s'", 
													direct.m_data.m_ppdbDest[nDest]->pszIP,
													(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)),
													pszPath
													);
												if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
												{
										//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
												}
											}
											if(prs)
											{
												prs->Close();
												delete prs;
											}
										}
									}

									if(!bRecordExists)
									{
										//insert one.
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"INSERT INTO [device_%s] (filename, sys_last_used, sys_times_used, expires_after, transfer_date) VALUES ('%s', %d, %d, %d, %d);", 
											direct.m_data.m_ppdbDest[nDest]->pszIP,
											pszPath,
											0,0,0,  // expire gets fixed later, after it plays once.  TODO - fix later.
											(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))
											
											);
										if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
										{
								//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
										}

									}
								}



																						}// else
																						else
																						{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Miranda_PushFile failed [%s] -> [%s][%s],  returned %d",  pchFoundFile, pszPath, pszDirAlias, nRV);  //(Dispatch message)
Sleep(200);
																							bError = true;
																						} // push file failed
																						free(pchFoundFile);
																					}
																					// else problem
																					else
																					{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "FindFile returned success but pchFoundFile was null");  //(Dispatch message)
Sleep(200);

																						bError = true;
																					} // pch found file was null
																				}
																				else
																				{// no file
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "FindFile failed");  //(Dispatch message)
Sleep(200);

																					bError = true;
																				}

																				if(bError)
																				{

																					//no file!
																											// send the error out.
																					if(!(ulStatus&DIRECT_LESTATUS_ERR)) // if we didnt send the error, send the error.
																					{
																						// have to update.
																						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:FindFile", "The file [%s] was not found on [%s] nor in the assets store with root folder [%s].", pszPath,direct.m_data.m_ppdbDest[nDest]->pszIP, direct.m_settings.m_pszPrimaryDestinationFolderPath);  //(Dispatch message)
																						direct.SendError("Direct:FindFile", "The file [%s] was not found on [%s] nor in the assets store with root folder [%s].",pszPath, direct.m_data.m_ppdbDest[nDest]->pszIP, direct.m_settings.m_pszPrimaryDestinationFolderPath);
																					}

																					ulStatus|=(DIRECT_LESTATUS_NOFILE|DIRECT_LESTATUS_ERR);

								if(nLiveEventTableIndex>=0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}


																				}


																			}


																			if(bAllFilesFound)
																			{
																				// set it up in DB, and delete the parsefile(s).
																				_unlink(parsefile);
																				_unlink(localfile);

																				// do the DB.
																				// well then, we are all set!

																				
																				ulStatus &= ~DIRECT_LESTATUS_MASK;
																				ulStatus |= DIRECT_LESTATUS_SET;
//																				direct.SendMsg("Direct:GetLiveEvents","Retrieve returned NULL\n%s", dberrorstring);

																				// update the DB!


								if(nLiveEventTableIndex>=0)
								{

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}



																			}//if( bAllFilesFound )
																		}
																	}
																}
															} // GetDestination failed.
															else
															{
																// have to report that something is not right
				//TODO
																direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:GetDestination", 
																	"An event on channel %d has data which does not correspond to any registered IP address.\nThe data for event [%s][%s] was [%s].", 
																	usChannelID,
																	pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CLIPID]?pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CLIPID]:"(null)",
																	pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CLIPTITLE]?pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CLIPTITLE]:"(null)",
																	pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_DATA]?pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_DATA]:"(null)"
																	);  //(Dispatch message)
															}
														}  // if (pchKeyer)  and channel not null
													}//if(pchField)
												} // if((pchField!=NULL)&&(strlen(pchField)>0))  // valid filename
											}  // if chars counted
										} //if(strlen(pch)>6) 
										if(!bSupported)
										{
											// have to update with unsupported status.
											ulStatus|=DIRECT_LESTATUS_UNSUPPORTED;

								if(nLiveEventTableIndex>=0)
								{

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
										"UPDATE %s SET status = %d WHERE id LIKE '%s' AND channel = %s", 
										direct.m_settings.m_pszTableEvents,
										ulStatus,
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_ID],
										pdbrLiveEventData[ulCheckingIndex].m_ppszData[DIRECT_LEFIELD_CHANNEL]
										);

									if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
									{
										direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:UpdateLiveEvents", "Update returned an error. (%d)\n%s", __LINE__, dberrorstring);  //(Dispatch message)
									}
								}



										}
									}
									// otherwise, can't do nuffin, so just skip fer now.
								} break;
							case DIRECT_LESTATUS_CHK://						0x00000100  // checking - if a tem file is needed from machine, means xferring that one back to controller
								{
									// this means an xfer in prog, so should never get here.
									// but just in case...
									bTransferInProg = true; //breaks out of the while
								} break;
							case DIRECT_LESTATUS_XFER://					0x00000200  // xferring a file to a machine (ORed with 0-255 ordinal of which file is being transferred) for IS2, 0=oxa, 1=oxt, 2=oxw.  for INT 0=tem, then enum'ed components as listed by tem.
								{
									// this means an xfer in prog, so should never get here.
									// but just in case...
									bTransferInProg = true;  //breaks out of the while
								} break;
							case DIRECT_LESTATUS_SET://						0x00000300  // all set to go, file is there.
								{
									// ignore, go to next.
								} break;
							}
						}
					}

					pdbrLiveEventData[ulCheckingIndex].Free();  // free the thing!

					ulCheckingIndex++;
				} // while((!bTransferInProg)&&(ulCheckingIndex<ulNumLiveEventRecords)&&(direct.m_data.m_bWatchfolderInitialized))

				if(pdbrLiveEventData) delete [] pdbrLiveEventData;
				ulNumLiveEventRecords=0;

			}




			// If not, check drive space on each box, check file attribs from the file server
			// if the file is not there, report.
			// if there's enough space (with % margin), transfer it over.
			// if not, check metadata in DB for most appropriate file to delete from box (usage statistics)
			// repeat process until there is enough drive space and transfer.

			// the following is in the server listener thread
			// Get a ping from a clone, if set up to do so, reply and clear any DB flags.
			// If no ping, but all else is OK, set up a DB flag that says MAIN.
			// the no ping check is here
			//TODO

			// Repeat whole process on an interval.
			
		}  // NOT clone mode, main process

direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before clone");  //(Dispatch message)
Sleep(200);

		
		if(
//				(direct.m_data.m_bCloneMode)&&  // dont include here.  at the interval, we will either write or read the time.
				(direct.m_settings.m_bUseClone)&&  // only do this if we are using a clone
				(
					(timestamp.time > clonetime.time)
				||((timestamp.time == clonetime.time)&&(timestamp.millitm >= clonetime.millitm))
				)
				&&(!g_bKillThread)
			)
		{
			clonetime.time = timestamp.time + direct.m_settings.m_ulClonePingIntervalMS/1000; 
			clonetime.millitm = timestamp.millitm + (unsigned short)(direct.m_settings.m_ulClonePingIntervalMS%1000); // fractional second updates
			if(clonetime.millitm>999)
			{
				clonetime.time++;
				clonetime.millitm%=1000;
			}
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "in clone");  //(Dispatch message)
Sleep(200);

			//////////////////////////////////////////////////////////////
			// The general clone process
			//////////////////////////////////////////////////////////////
			// Ping the other Direct process every interval (settable)

			// ping on direct.m_settings.m_pszCloneIP, direct.m_settings.m_usCloneCommandPort
			
			// if there, fine, just listen for any commands and if a client
			// tries to connect, send it a redirect command to the other process.
			// if it's not there, retry 3 times.
			// if still not there, connect to DB and check flag.  If flag is not there,
			// we assume main process died.  We set DB flag to BACKUP.
			// start general playout process as above.


			// ok here we go!
			// ping on direct.m_settings.m_pszCloneIP, direct.m_settings.m_usCloneCommandPort
/*
			CNetData data;

			data.m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
			//no subcommand or data
			data.m_ucCmd = DIRECT_CMD_PING; 
			data.m_ucSubCmd = NULL;       // the subcommand byte

			data.m_pucData = NULL;
			data.m_ulDataLen = NULL;

			SOCKET s; // direct hardcode
			bool bOK=false;
			if(direct.m_net.SendData(&data, direct.m_settings.m_pszCloneIP, direct.m_settings.m_usCloneCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
			{
				if(data.m_ucCmd == DIRECT_CMD_ACK) // ack returned
				{
					bOK=true;
				}
			}
			direct.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

			if(!bOK)
			{

			}
	*/
			

			// we dont need to do any pinging via network.
			// we just check the DB.


			if(direct.m_data.m_bCloneMode)
			{
				// read the time
				if(direct.m_data.m_bCloneInitReadSuccess) // we already got the init, we can proceed
				{
					if(direct.m_data.m_pdbConnPrimary)
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE criterion LIKE 'clone'", 
							direct.m_settings.m_pszTableExchange
							);

						CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
						if(prs != NULL) 
						{
							if(!prs->IsEOF())
							{
								CString szValue;
								char description[256];
								prs->GetFieldValue("flag", szValue);
								sprintf( description, "%s",szValue);
								prs->GetFieldValue("mod", szValue);
								unsigned long ulTime = atol(szValue);

								if(stricmp(description, direct.m_settings.m_bInitClone?"MAIN":"BACKUP")==0) // always check the other one
								{
									// otherwise backup has not been restored yet.
									if(direct.m_data.m_ulLastCloneTime == ulTime)
									{
										direct.m_data.m_ulNumCloneTimeSame++;
										if(direct.m_data.m_ulNumCloneTimeSame>3)// allow 3 errors.
										{
											// switch modes.
											direct.m_data.m_bCloneMode = false;
											direct.m_data.m_bCloneInitReadSuccess = false;  // reset this.
										}
									}
								}
								else
								if(stricmp(description, direct.m_settings.m_bInitClone?"BACKUP":"MAIN")==0) // always check the other one
								{
									// then I am the process!
									direct.m_data.m_bCloneMode = false;
									direct.m_data.m_bCloneInitReadSuccess = false;  // reset this.
								}

							}
							prs->Close();
							delete prs;
						}
					}
				}
				else
				{
					// try to read the db for the first time.
					if(direct.m_data.m_pdbConnPrimary)
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE criterion LIKE 'clone'", 
							direct.m_settings.m_pszTableExchange
							);

						CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
						if(prs != NULL) 
						{
							if(!prs->IsEOF())
							{
								CString szValue;
								char description[256];
								prs->GetFieldValue("flag", szValue);
								sprintf( description, "%s",szValue);
								prs->GetFieldValue("mod", szValue);
								unsigned long ulTime = atol(szValue);

								if(stricmp(description, direct.m_settings.m_bInitClone?"MAIN":"BACKUP")==0) // always check the other one
								{
									// otherwise backup has not been restored yet.
									direct.m_data.m_ulLastCloneTime = ulTime;
									direct.m_data.m_ulNumCloneTimeSame = 0;  

									direct.m_data.m_bCloneInitReadSuccess = true;
								}
							}
							prs->Close();
							delete prs;
						}
					}
				}
			}
			else  // normal mode.
			{
				//write the time.
				_ftime(&timestamp);

				// read the DB to see if backup is operating.  if so, we just accept clone mode.

				if(!direct.m_data.m_bCloneInitReadSuccess) // we already got the init, we can proceed
				{
					// try to read the db for the first time.
					if(direct.m_data.m_pdbConnPrimary)
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE criterion LIKE 'clone'", 
							direct.m_settings.m_pszTableExchange
							);

						CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
						if(prs != NULL) 
						{
							if(!prs->IsEOF())
							{
								CString szValue;
								char description[256];
								prs->GetFieldValue("flag", szValue);
								sprintf( description, "%s",szValue);
								prs->GetFieldValue("mod", szValue);
								unsigned long ulTime = atol(szValue);

								if(stricmp(description, direct.m_settings.m_bInitClone?"MAIN":"BACKUP")==0) // always check the other one
								{
									direct.m_data.m_bCloneMode = true;  // be a clone.

									direct.m_data.m_ulLastCloneTime = ulTime;
									direct.m_data.m_ulNumCloneTimeSame = 0;  

									direct.m_data.m_bCloneInitReadSuccess = false;
								}
							}
							prs->Close();
							delete prs;
						}
					}
				}

				if((direct.m_data.m_pdbConnPrimary)&&(!direct.m_data.m_bCloneMode))
				{
					// get it and see if the backup is in operation. if so, just go to clone mode.

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
						"UPDATE %s SET flag = '%s', mod = %d WHERE criterion LIKE 'clone'", 
						direct.m_settings.m_pszTableExchange,
						direct.m_settings.m_bInitClone?"BACKUP":"MAIN",
						(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))
						);

					if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
					{
			//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
					}
				}

			}



			// if there, fine, just listen for any commands and if a client
			// tries to connect, send it a redirect command to the other process.
			// if it's not there, retry 3 times.
			// if still not there, connect to DB and check flag.  If flag is not there,
			// we assume main process died.  We set DB flag to BACKUP.
			// start general playout process as above.

		}// clone process
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before watch");  //(Dispatch message)
Sleep(200);

		if(
				(!direct.m_data.m_bCloneMode) && // actually the clone will look at its own watchfolder, peersync dumps to both machines
			(
					(timestamp.time > watchtime.time)
				||((timestamp.time == watchtime.time)&&(timestamp.millitm >= watchtime.millitm))
				&&(!g_bKillThread)
				)
			)
		{
			watchtime.time = timestamp.time + direct.m_settings.m_ulWatchIntervalMS/1000; 
			watchtime.millitm = timestamp.millitm + (unsigned short)(direct.m_settings.m_ulWatchIntervalMS%1000); // fractional second updates
			if(watchtime.millitm>999)
			{
				watchtime.time++;
				watchtime.millitm%=1000;
			}
			//////////////////////////////////////////////////////////////
			// The general watchfolder process
			//////////////////////////////////////////////////////////////
			// check queue for any changes


			// if we arent watching start it up now.
			if(!direct.m_dir.m_bWatchFolder)
			{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we arent watching yet");  //(Dispatch message)
				int nReturn = direct.m_dir.SetWatchFolder(direct.m_settings.m_pszWatchFolderPath);
				if(nReturn<DIRUTIL_SUCCESS)
				{
					// error
					//TODO report
			EnterCriticalSection(&theApp.m_crit);
					theApp.m_nAppState=ICON_YEL;
					theApp.SetAppStateText( "error setting watchfolder.");
				LeaveCriticalSection(&theApp.m_crit);
					direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, direct.m_settings.m_pszWatchFolderPath?direct.m_settings.m_pszWatchFolderPath:"(null)");  //(Dispatch message)
				}
				else
				{
					nReturn = direct.m_dir.BeginWatch( direct.m_settings.m_ulWatchIntervalMS, true);
					if(nReturn<DIRUTIL_SUCCESS)
					{
						// error
						//TODO report
			EnterCriticalSection(&theApp.m_crit);
						theApp.m_nAppState=ICON_YEL;
						theApp.SetAppStateText( "error beginning watchfolder process.");
				LeaveCriticalSection(&theApp.m_crit);
						direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, direct.m_settings.m_pszWatchFolderPath?direct.m_settings.m_pszWatchFolderPath:"(null)");  //(Dispatch message)
					}
					else
					{
			EnterCriticalSection(&theApp.m_crit);
						theApp.m_nAppState=ICON_GRN;
						theApp.SetAppStateText( "Direct is in progress.");
				LeaveCriticalSection(&theApp.m_crit);
						direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct:SetWatchFolder", "SetWatchFolder: began watch on %s", direct.m_settings.m_pszWatchFolderPath);  //(Dispatch message)

						// subtract half the interval so that the watch folder checks for changes, staggered with us checking the results in the else below.
						watchtime.time -= direct.m_settings.m_ulWatchIntervalMS/2000; // second resolution is fine.
/*						watchtime.millitm += (unsigned short)((direct.m_settings.m_ulWatchIntervalMS%1000)/2); // fractional second updates
						if(watchtime.millitm>999)
						{
							watchtime.time++;
							watchtime.millitm%=1000;
						}
*/
					}
				}
			}
			else // we are watching!
			{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we are watching");  //(Dispatch message)
				int nWatchChanges = direct.m_dir.QueryChanges();
				if(nWatchChanges>0)
				{
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d watchfolder changes found", nWatchChanges);  //(Dispatch message)

					// if there's an addition, grab the file path in the inbox, and create
					// the same folder if necessary, copy the file there, delete out of the inbox
					// (dont do a move, in case theres a prob transferring)
					// once the file is on the file server, update the metadata in the database
					CDirEntry* pDirChange;
					int numchanges = 0; // dbg only
					_timeb diskchecktime;
					_ftime(&diskchecktime);
					diskchecktime.time-=6;  // init so it goes.

					while((direct.m_dir.GetDirChange(&pDirChange)>0)&&(!g_bKillThread))

					{
								numchanges++;  // dbg only
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file: %s (%d changes this round)", pDirChange->m_szFilename, numchanges);  //(Dispatch message)
//Sleep(100);
						switch(pDirChange->m_ulFlags&(~WATCH_CHK)) // ignore the check flag
						{
						case WATCH_INIT://		0x00000000  // was there at beginning	
						case WATCH_ADD: //		0x00000001  // was added	
							{
								// if theres a new thing, we have to do one or more things.
								// if it's a new directory, we have to add it in the storage area to mimic
								// if it's a new file, we have to copy it to the storage area.
								// but first we have to check the storage area to see if it's there already,
								// if so, flag it.
								// if not, we copy it.
								// we leave it there so that the backup can also copy it.
								// we check DB later to make sure we have it both places before deleting.
								if((pDirChange->m_szFilename)&&(pDirChange->m_szFullPath))
								{
									if((strcmp(pDirChange->m_szFilename, "."))&&(strcmp(pDirChange->m_szFilename, ".."))) // only if its a real file or dir
									{

										if(pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_DIRECTORY)
										{
											if(stricmp(direct.m_settings.m_pszWatchFolderPath, direct.m_settings.m_pszPrimaryDestinationFolderPath))  // only do if different
											{
												// mkdir is fine if dir is there already
												sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
												char* pchRoot = NULL;
												if(strnicmp(pszPath, direct.m_settings.m_pszWatchFolderPath, strlen(direct.m_settings.m_pszWatchFolderPath))==0)
												{
													pchRoot = pszPath + strlen(direct.m_settings.m_pszWatchFolderPath);
													char path_buffer[MAX_PATH+1];
													char dir_buffer[MAX_PATH+1];
													sprintf(path_buffer, "%s%s", direct.m_settings.m_pszPrimaryDestinationFolderPath, pchRoot);

													//_mkdir(pszPath);
													pchRoot = strstr(path_buffer, ":\\");  //drive delim.
													int nBegin=0;
													if(pchRoot) nBegin = pchRoot-path_buffer+2;

													// have to make dir.
													// if dir already exists, no problem, _mkdir just returns and we continue
													// so, we can always just call mkdir

//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before mkdir with %s", path_buffer);  //(Dispatch message)
													strcpy(dir_buffer, path_buffer);

													for (int i=nBegin; i<(int)strlen(path_buffer); i++)
													{
														if((path_buffer[i]=='/')||(path_buffer[i]=='\\'))
														{
															dir_buffer[i] = 0;
															if(strlen(dir_buffer)>0)
															{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

																_mkdir(dir_buffer);
															}

															dir_buffer[i] = '\\';
														}
														else
														if(i==(int)strlen(path_buffer)-1)
														{
															if(strlen(dir_buffer)>0)
															{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

																_mkdir(dir_buffer);
															}
														}
														else
															dir_buffer[i] = path_buffer[i];
													}


												} // correct base folder
											} // watch and destination are not the same
										}  // dir
										else  // not a dir
										{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "not a dir");  //(Dispatch message)
//Sleep(100);
											// if it's a new file, we have to copy it to the storage area.
											// but first we have to check the storage area to see if it's there already,
											// if so, flag it.
											// if not, we copy it.
											// we leave it there so that the backup can also copy it. not true: separate synch copies it over both places
											// we check DB later to make sure we have it both places before deleting. not true.

											sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
											char* pchRoot = NULL;
											if(strnicmp(pszPath, direct.m_settings.m_pszWatchFolderPath, strlen(direct.m_settings.m_pszWatchFolderPath))==0)
											{
												pchRoot = pszPath + strlen(direct.m_settings.m_pszWatchFolderPath);
												char path_buffer[MAX_PATH+1];
												sprintf(path_buffer, "%s%s", direct.m_settings.m_pszPrimaryDestinationFolderPath, pchRoot);

												// have to copy file, but check space first.
												int nDiskPercent;

												// we had a prob with this before.
												BOOL bSuccess = FALSE;
												// only check the disk space once every 5 seconds.
												_ftime(&timestamp);

//												if( timestamp.time > diskchecktime.time ) 
												{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to call disk space %d>%d", (unsigned long)timestamp.time, (unsigned long)diskchecktime.time);  //(Dispatch message)
													
													bSuccess = GetDiskFreeSpaceEx( direct.m_settings.m_pszPrimaryDestinationFolderPath, 
																															&direct.m_data.m_uliBytesAvail, 
																															&direct.m_data.m_uliBytesTotal,
																															&direct.m_data.m_uliBytesFree
 																														); 

												}																	
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "called disk space");  //(Dispatch message)
												if(bSuccess) // make this zero to remove the disk check
												{
													diskchecktime.time = (timestamp.time+5);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "assigned %d = %d",  diskchecktime.time, time(NULL)+5);  //(Dispatch message)

													direct.m_data.m_dblAvail = ((double)(direct.m_data.m_uliBytesAvail.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)direct.m_data.m_uliBytesAvail.LowPart;
													direct.m_data.m_dblTotal = ((double)(direct.m_data.m_uliBytesTotal.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)direct.m_data.m_uliBytesTotal.LowPart;
													direct.m_data.m_dblFree	= ((double)(direct.m_data.m_uliBytesFree.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)direct.m_data.m_uliBytesFree.LowPart;
												}

												nDiskPercent = (int)((direct.m_data.m_dblTotal-direct.m_data.m_dblAvail)/(direct.m_data.m_dblTotal/100.0));  // == used space

										/*		direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:DiskSpaceCheck", "Disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																 direct.m_data.m_dblAvail, 
																 direct.m_data.m_dblAvail/(direct.m_data.m_dblTotal/100.0), 
																 direct.m_data.m_dblFree, 
																 direct.m_data.m_dblFree/(direct.m_data.m_dblTotal/100.0), 
																 direct.m_data.m_dblTotal
																 );  //(Dispatch message)
*/
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here"); Sleep(100); //(Dispatch message)

												if(nDiskPercent>(int)direct.m_settings.m_ucDeletePercentageDiskFull)
												{
			EnterCriticalSection(&theApp.m_crit);
					theApp.m_nAppState=ICON_YEL;
					theApp.SetAppStateText("disk near full!");
			LeaveCriticalSection(&theApp.m_crit);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here2"); Sleep(100); //(Dispatch message)
													if(!direct.m_data.m_bDiskDeletionSent)
													{
														//TODO:  send an actual report out
														direct.m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Direct:DiskSpaceCheck", "Disk utilization has reached the auto-deletion level (%d.00%% available free space).  The system must delete older files to continue to function.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																		 (100-direct.m_settings.m_ucDeletePercentageDiskFull), 
																		 direct.m_data.m_dblAvail, 
																		 direct.m_data.m_dblAvail/(direct.m_data.m_dblTotal/100.0), 
																		 direct.m_data.m_dblFree, 
																		 direct.m_data.m_dblFree/(direct.m_data.m_dblTotal/100.0), 
																		 direct.m_data.m_dblTotal
																		 );  //(Dispatch message)
														direct.SendMsg( "Direct:DiskSpaceCheck", "Disk utilization has reached the auto-deletion level (%d.00%% available free space).  The system must delete older files to continue to function.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																		 (100-direct.m_settings.m_ucDeletePercentageDiskFull), 
																		 direct.m_data.m_dblAvail, 
																		 direct.m_data.m_dblAvail/(direct.m_data.m_dblTotal/100.0), 
																		 direct.m_data.m_dblFree, 
																		 direct.m_data.m_dblFree/(direct.m_data.m_dblTotal/100.0), 
																		 direct.m_data.m_dblTotal
																		 );  //(Dispatch message)
														Sleep(100);
														direct.m_data.m_bDiskDeletionSent = true;
													}

													// do auto deletes!

													direct.DoAutoDeletion();

												}
												else
												{
													direct.m_data.m_bDiskDeletionSent = false;  // always reset if the threshold comes back down
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here22"); Sleep(100); //(Dispatch message)

													if(nDiskPercent>(int)direct.m_settings.m_ucWarnPercentageDiskFull)
													{
										
					EnterCriticalSection(&theApp.m_crit);
					theApp.m_nAppState=ICON_YEL;
					theApp.SetAppStateText("disk space warning!");
					LeaveCriticalSection(&theApp.m_crit);
					
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here56"); Sleep(100); //(Dispatch message)
														direct.m_data.m_bDiskWarning = true;
														if(!direct.m_data.m_bDiskWarningSent)
														{
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x"); Sleep(100); //(Dispatch message)
															//TODO:  send an actual report out
															direct.m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Direct:DiskSpaceCheck", "Disk utilization has exceeded the warning level (%d.00%% available free space).  The system must delete older files to continue to function.  Auto-deletion will commence when the available disk space drops below %d.00%%.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																			 (100-direct.m_settings.m_ucWarnPercentageDiskFull), 
																			 (100-direct.m_settings.m_ucDeletePercentageDiskFull), 
																			 direct.m_data.m_dblAvail, 
																			 direct.m_data.m_dblAvail/(direct.m_data.m_dblTotal/100.0), 
																			 direct.m_data.m_dblFree, 
																			 direct.m_data.m_dblFree/(direct.m_data.m_dblTotal/100.0), 
																			 direct.m_data.m_dblTotal
																			 );  //(Dispatch message)
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x1"); Sleep(100); //(Dispatch message)

															direct.SendMsg("Direct:DiskSpaceCheck", "Disk utilization has exceeded the warning level (%d.00%% available free space).  The system must delete older files to continue to function.  Auto-deletion will commence when the available disk space drops below %d.00%%.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																			 (100-direct.m_settings.m_ucWarnPercentageDiskFull),
																			 (100-direct.m_settings.m_ucDeletePercentageDiskFull), 
																			 direct.m_data.m_dblAvail, 
																			 direct.m_data.m_dblAvail/(direct.m_data.m_dblTotal/100.0), 
																			 direct.m_data.m_dblFree, 
																			 direct.m_data.m_dblFree/(direct.m_data.m_dblTotal/100.0), 
																			 direct.m_data.m_dblTotal
																			 );
//															Sleep(100);
//direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x2"); Sleep(100); //(Dispatch message)
															direct.m_data.m_bDiskWarningSent = true;
														}
													}
													else
													{
														direct.m_data.m_bDiskWarningSent = false;  // always reset if the threshold comes back down
														direct.m_data.m_bDiskWarning = false;
													}
												}
												 
// direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here arg"); Sleep(100); //(Dispatch message)

/*
				if()

	m_bDiskWarningSent = false;
	m_bDiskWarning = false;
	m_bDiskDeletionSent = false;
	m_bDiskDeletion = false;
*/

												bool bCopyOK = false;
												if(stricmp(pszPath, path_buffer))  // only do if different
												{
//													direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
								EnterCriticalSection(&direct.m_dir.m_critFileOp);


								// have to check if the target exists and if so set its attribs to not ready only so we can overwrite if nec.
													DWORD dwfa = GetFileAttributes(path_buffer);
													if(dwfa!=0xFFFFFFFF) // then it succeeded, the file was there, etc.
													{
														dwfa&= ~FILE_ATTRIBUTE_READONLY;
														SetFileAttributes(path_buffer, dwfa);
													}

													if(CopyFile( pszPath, path_buffer, FALSE)) // flag for operation if file exists FALSE = overwrite.
													{
														direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "copied file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
//Sleep(500);  // let the disk flush.
														// update DB.
														// TODO db stuff
														bCopyOK = true;
														//db stuff later..




														if(!(pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_READONLY))  // because delete will fail on files that are read only
														{
														// we are just deleting immediately...
															direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "deleting file: %s", pszPath);  //(Dispatch message)
//Sleep(200);
								

//														_unlink(pszPath); // try this!  nope, didnt work.


															if(DeleteFile(pszPath))
															{
 																direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "deleted file: %s", pszPath);  //(Dispatch message)
															}
															else
															{
 																direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "couldn't delete file: %s (%d)", pszPath, GetLastError());  //(Dispatch message)
															}
														}
//Sleep(200);
													}
													else
													{
 														direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "couldn't copy file: %s (%d)", pszPath, GetLastError());  //(Dispatch message)
//Sleep(200);

													}

								
/*
								
													if(MoveFileEx( pszPath, path_buffer, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH)) // flag for operation if file exists FALSE = overwrite.
													{
														direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Moved file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
													}
													else
 														direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Couldn't move file: %s (%d)", pszPath, GetLastError());  //(Dispatch message)
*/								
								
								LeaveCriticalSection(&direct.m_dir.m_critFileOp);
													direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "finished copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
//Sleep(200);
												}
												else
												{
													bCopyOK = true;
												}
												if(bCopyOK)
												{
													// deal with metadata.

													// get the new path.
													pchRoot = pDirChange->m_szFullPath + strlen(direct.m_settings.m_pszWatchFolderPath);
													sprintf(path_buffer, "%s%s", direct.m_settings.m_pszPrimaryDestinationFolderPath, pchRoot);

//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "finished copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
//Sleep(200);

	if(direct.m_data.m_pdbConnPrimary)
	{
		CDBRecord* pdbrData=NULL;
		unsigned long ulNumRecords=0;

		int nMetadataTableIndex = direct.m_db.GetTableIndex(direct.m_data.m_pdbConnPrimary, direct.m_settings.m_pszTableMeta);
		if(nMetadataTableIndex>=0)
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE filename LIKE '%s'", 
				direct.m_settings.m_pszTableMeta,
				pDirChange->m_szFilename
				);
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
//Sleep(200);

			//Sleep(500);
			bool bFoundRecord = false;
			CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
			if(prs != NULL) 
			{
				Sleep(100); // a little delay to not peg the DB server.
				if(!prs->IsEOF())
				{
					prs->Close();  // close before next sql statement
					delete prs;
					prs = NULL;

					bFoundRecord = true;
					// just update flags

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s SET sys_file_flags = %d, filepath = '%s' WHERE filename LIKE '%s'", 
						direct.m_settings.m_pszTableMeta,
						DIRECT_FILEFLAG_FILECOPIED,
						path_buffer,//pDirChange->m_szFullPath, // in case the path got changed.
						pDirChange->m_szFilename
						);
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
//Sleep(200);
					if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
					{
			//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
						Sleep(5000); // a longer delay to try to recover
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
//Sleep(200);
						if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
						{
				//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
							Sleep(5000); // a longer delay to try to recover

						}
					}
//					Sleep(50); // a little delay to not peg the DB server.

				}
				if(prs)
				{
					prs->Close();  // close before next sql statement
					delete prs;
				}
			}

			if(!bFoundRecord)
			{

				char description[65];
				strcpy(description, "unknown file type");
				// determine filetype from extension.
				unsigned long ulType = 0; // unknown!
				char* pchExt = strrchr(pDirChange->m_szFilename, '.');
				if((pchExt)&&(strlen(pchExt)>1))
				{
					if(strlen(pchExt)<=4)  // only 0<x<4 letter extensions allowed
					{
						pchExt++;
					}
					else	pchExt = NULL;
				}	else	pchExt = NULL;


				if(pchExt)
				{
					ulType = direct.m_data.ReturnExchangeType(pchExt);
/*
// go easy on the DB
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE criterion LIKE '%s'", 
						direct.m_settings.m_pszTableExchange,
						pchExt
						);

					CRecordset* prs = direct.m_db.Retrieve(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring);
					if(prs != NULL) 
					{
						if(!prs->IsEOF())
						{
							CString szValue;
							prs->GetFieldValue("flag", szValue);
							sprintf( description, "%s",szValue);
							prs->GetFieldValue("mod", szValue);
							ulType = atol(szValue);
						}
						prs->Close();
						delete prs;
					}
*/
				}
				
				
				_ftime( &timestamp );

				// have to insert a default system record. - just the fields we care about.
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
					"INSERT INTO %s (filename, filepath, description, operator, type, ingest_date) VALUES ('%s', '%s', '%s', '%s', %d, %d);", 
					direct.m_settings.m_pszTableMeta,
					pDirChange->m_szFilename, 
					path_buffer,//pDirChange->m_szFullPath,
					description,
					"sys", ulType,
					(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))
					);
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
//Sleep(200);
				if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
				{
		//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
					Sleep(5000); // a longer delay to try to recover
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
//Sleep(200);
					if(direct.m_db.ExecuteSQL(direct.m_data.m_pdbConnPrimary, szSQL, dberrorstring)<DB_SUCCESS)
					{
			//			direct.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:DeleteLiveEvents", "Delete returned an error.\n%s", dberrorstring);  //(Dispatch message)
						Sleep(5000); // a longer delay to try to recover

					}

				}
//				Sleep(50); // a little delay to not peg the DB server.
			}
		}
	} //if(direct.m_data.m_pdbConnPrimary)

												} //if(bCopyOK)


 											} // correct base folder
										}  // not a dir
									} // valid filename
								}// non-null params

							} break;
						case WATCH_DEL: //		0x00000002  // was del
							break;
						case WATCH_CHG: //		0x00000003  // was chg	
							break;
						default: 
							break;  // unknown
						}


/*
			// debug file
						FILE* fp;
						fp = fopen("_removedlog.log", "at");
						if(fp)
						{
							fprintf(fp, "%s\\%s %s %d %d%d %d%db 0x%08x\r\n", 	
								pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
								pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
								foo,
								pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
								pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
								pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
								pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
								pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
								pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
								);
							fflush(fp);
							fclose(fp);
						}


						Message("%s\\%s %s %d %d%d %d%db 0x%08x", 	
							pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
							pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
							foo,
							pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
							pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
							pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
							);
	*/
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "about to delete pDirChange");  //(Dispatch message)
//Sleep(200);
						if(pDirChange) delete(pDirChange);
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "deleted pDirChange");  //(Dispatch message)
//Sleep(200);
//	direct.m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "really deleted pDirChange");  //(Dispatch message)
//Sleep(200);


					} // while changes
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d changes registered", numchanges);  //(Dispatch message)
Sleep(100);

				}  // there are changes
				direct.m_data.m_bWatchfolderInitialized = true;
			}  // we are watching
		}  // watchfolder process
direct.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "end loop");  //(Dispatch message)
Sleep(200);

		Sleep(5);  //just to not peg processor if nothing is happening.
	} // 	while(!g_bKillThread)

	

//	AfxMessageBox("shutting down command server.");
//	if(pwndStatus) pwndStatus->SetWindowText("shutting down command server.");
	direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "Shutting down command server.");  //(Dispatch message)
	direct.m_net.StopServer(direct.m_settings.m_usCommandPort, 5000, errorstring);
//	AfxMessageBox("shutting down status server.");
//	if(pwndStatus) pwndStatus->SetWindowText("shutting down status server.");
	direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "Shutting down status server.");  //(Dispatch message)
	direct.m_net.StopServer(direct.m_settings.m_usStatusPort, 5000, errorstring);
//	if(pwndStatus) pwndStatus->SetWindowText("exiting...");

/*

	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniInt("FileServer", "ListenPort", direct.m_settings.m_usFilePort);
		file.SetIniInt("CommandServer", "ListenPort", direct.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", direct.m_settings.m_usStatusPort);

		file.SetIniInt("Resources", "MinPort", direct.m_settings.m_usResourcePortMin);
		file.SetIniInt("Resources", "MaxPort", direct.m_settings.m_usResourcePortMax);

		file.SetIniInt("Processes", "MinPort", direct.m_settings.m_usProcessPortMin);
		file.SetIniInt("Processes", "MaxPort", direct.m_settings.m_usResourcePortMin);

		file.SetIniInt("Messager", "UseEmail", direct.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", direct.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "LogRemote", direct.m_settings.m_bUseLogRemote?1:0);
		file.SetIniInt("Messager", "UseClone", direct.m_settings.m_bUseClone?1:0);

		file.SetSettings("direct.csf", false);  // have to have correct filename
	}

*/

// have to disconnect and free stuff.
	// watch folder
	direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "Shutting down watch folder.");  //(Dispatch message)
	direct.m_dir.EndWatch();

	// SQL database
	direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "Shutting down database connections.");  //(Dispatch message)

	unsigned char ucI=0;
	while((direct.m_db.m_ppdbConn)&&(ucI<direct.m_db.m_ucNumConnections))
	{
		direct.m_db.DisconnectDatabase(direct.m_db.m_ppdbConn[ucI]);
		ucI++;
	}
	
	// Omnibus automation

	unsigned short nConnectionIndex=0;
	while (nConnectionIndex<direct.m_settings.m_usNumColossus)
	{
		direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "Shutting down Omnibus Colossus server connection on [%s].",direct.m_settings.m_ppszServerAddress[nConnectionIndex]);  //(Dispatch message)
		if((direct.m_settings.m_ppszServerAddress)&&(direct.m_settings.m_ppszServerAddress[nConnectionIndex]))			// array of Colossus Adaptor IP (port is 10540)
		{
			direct.m_omni.DisconnectServer(direct.m_settings.m_ppszServerAddress[nConnectionIndex]);
		}
		nConnectionIndex++;
	}

	direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "Shutting down Miranda Oxsox communications.");  //(Dispatch message)
	// Oxtel graphics boxes
	direct.m_ox.OxSoxAbortTransfer();
	direct.m_ox.OxSoxFree();




	//exiting
	direct.m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "-------------- Direct %s exit ---------------\n\
--------------------------------------------------\n", DIRECT_CURRENT_VERSION);  //(Dispatch message)
//	pdlg->SetProgress(CXDLG_CLEAR);

	Sleep(500); // small delay at end
//	AfxMessageBox("X");
	g_bThreadStarted = false;
//	AfxMessageBox("Y");

	Sleep(100); // another small delay at end
//	AfxMessageBox("Z");
//	_endthread();  // not sure why, this crashes on program exit

//	AfxMessageBox("z");

}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


void DirectServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	CDirectMain* pDirect = (CDirectMain*) pClient->m_lpObject;
	if(pDirect==NULL) { _endthread(); return; }  // need the object to deal

	bool bSetGlobalKill = false;
	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

// need to put in the "meat" for these commands.


					if(pDirect->m_data.m_bCloneMode)
					{
						// send redirect command
						data.m_ucCmd = DIRECT_CMD_CHGCOMM;
						data.m_ucSubCmd = DIRECT_CMD_HOST|DIRECT_CMD_PORT;
						char repbuf[64];
						_snprintf(repbuf, 63, "%s:%d", pDirect->m_settings.m_pszCloneIP, pDirect->m_settings.m_usCloneCommandPort);
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case
						if(data.m_pucData!=NULL)
						{
							free(data.m_pucData);  //destroy the buffer;
						}
						data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
						if(data.m_pucData)
						{
							memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
						}
						else 
						{
							data.m_ucCmd = NET_CMD_NAK;  // just to differentiate
							data.m_ulDataLen = 0;
						}
		

						// has no data, just ack.
						data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.

					}
					else
					{
						switch(data.m_ucCmd)
						{
						case DIRECT_CMD_GETCHANNELS://  0x20  // get list of channels
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;
						case DIRECT_CMD_GETDESTS://		  0x21  // get list of destinations
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;
						case DIRECT_CMD_GETMETA://		  0x22  // get list of metadata
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;
						case DIRECT_CMD_GETSETTINGS://  0x23  // get list of settings
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;

						case DIRECT_CMD_SETCHANNELS://  0x30  // set list of channels
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;
						case DIRECT_CMD_SETDESTS://		  0x31  // set list of destinations
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;
						case DIRECT_CMD_SETMETA://		  0x32  // set list of metadata
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;
						case DIRECT_CMD_SETSETTINGS://  0x33  // set list of settings
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							} break;
						case DIRECT_CMD_BYE:
							{
		pDirect->m_msgr.DM(MSG_ICONINFO, NULL, "Direct", "The Global Kill command has been received.");  //(Dispatch message)

								bSetGlobalKill = true;
								//just ack
								data.m_ucCmd = NET_CMD_ACK;
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

							} break;
						default:
							{
								//just ack
								data.m_ucCmd = NET_CMD_ACK;

			//					char repbuf[64];
								if(data.m_pucData!=NULL)
								{
			/*
									_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

									for(unsigned long q=0; q<data.m_ulDataLen; q++)
									{
										if(strlen(repbuf)<63)
											strncat(repbuf, (char*)(data.m_pucData+q), 1);
									}
									if(strlen(repbuf)<63)
										strcat(repbuf, "]");
									data.m_ulDataLen = strlen(repbuf);
									repbuf[data.m_ulDataLen] = 0;  // just in case
			*/
									free(data.m_pucData);  //destroy the buffer;
								}
			/*
								else
								{
									_snprintf(repbuf, 63, "data was NULL");
									data.m_ulDataLen = strlen(repbuf); 
								}

								data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
								if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
								else 
								{
									data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
									data.m_ulDataLen = 0;
								}
			*/
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

							} break;
						}
					}


					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.


		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}


	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CDirectHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

	_endthread();
}
