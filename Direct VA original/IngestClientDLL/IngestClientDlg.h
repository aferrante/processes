#if !defined(AFX_CLIENTDLLDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
#define AFX_CLIENTDLLDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IngestClientDlg.h : header file
//
#define ID_LIST			0
#define ID_BNREF		1
#define ID_BNADD		2
#define ID_BNED			3
#define ID_BNDEL		4
#define DLG_NUM_MOVING_CONTROLS 5

/////////////////////////////////////////////////////////////////////////////
// CIngestClientDlg dialog

class CIngestClientDlg : public CDialog
{
// Construction
public:
	CIngestClientDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CIngestClientDlg)
	enum { IDD = IDD_DIALOG_MAIN };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIngestClientDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[DLG_NUM_MOVING_CONTROLS];

public:
	void OnExit();

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIngestClientDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDel();
	afx_msg void OnButtonEdit();
	afx_msg void OnButtonRefresh();
	afx_msg void OnItemclickList1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENTDLLDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
