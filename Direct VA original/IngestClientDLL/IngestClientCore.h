// IngestClientCore.h: interface for the CIngestClientCore class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTDLLCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
#define AFX_CLIENTDLLCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIngestClientCore  
{
public:
	CIngestClientCore();
	virtual ~CIngestClientCore();

};

#endif // !defined(AFX_CLIENTDLLCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
