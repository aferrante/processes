// IngestClientData.cpp: implementation of the CIngestClientData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IngestClient.h"
#include "IngestClientData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIngestClientData::CIngestClientData()
{
	m_bDialogStarted = false;
}

CIngestClientData::~CIngestClientData()
{

}
