#if !defined(AFX_CLIENTDLLSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
#define AFX_CLIENTDLLSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IngestClientSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIngestClientSettings dialog

class CIngestClientSettings : public CDialog
{
// Construction
public:
	CIngestClientSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CIngestClientSettings();   // standard destructor

// Dialog Data
	//{{AFX_DATA(CIngestClientSettings)
	enum { IDD = IDD_DIALOG_SETTINGS };
	CString	m_szDesc;
	//}}AFX_DATA

	// internal
	bool m_bHasDlg;

	//exposed
	char* m_pszAppName;
	char* m_pszAboutText;
	char* m_pszSettingsText;

	int Settings(bool bRead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIngestClientSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIngestClientSettings)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENTDLLSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
