#if !defined(AFX_TESTXCTL_H__B474352D_6E55_41CE_BA2D_AD82D5AFE08F__INCLUDED_)
#define AFX_TESTXCTL_H__B474352D_6E55_41CE_BA2D_AD82D5AFE08F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// TestxCtl.h : Declaration of the CTestxCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl : See TestxCtl.cpp for implementation.

class CTestxCtrl : public COleControl
{
	DECLARE_DYNCREATE(CTestxCtrl)

// Constructor
public:
	CTestxCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestxCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

public:
	BOOL m_bInit;

	CString GetInitData();

// Implementation
protected:
	~CTestxCtrl();

	DECLARE_OLECREATE_EX(CTestxCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CTestxCtrl)      // GetTypeInfo
//	DECLARE_PROPPAGEIDS(CTestxCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CTestxCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CTestxCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CTestxCtrl)
	CString m_szInit;
	afx_msg void OnInitChanged();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

//	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CTestxCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CTestxCtrl)
	dispidInit = 1L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTXCTL_H__B474352D_6E55_41CE_BA2D_AD82D5AFE08F__INCLUDED)
