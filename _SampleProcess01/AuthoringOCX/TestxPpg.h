#if !defined(AFX_TESTXPPG_H__7527A4F1_4105_4B41_8116_866B3ABE5D47__INCLUDED_)
#define AFX_TESTXPPG_H__7527A4F1_4105_4B41_8116_866B3ABE5D47__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// TestxPpg.h : Declaration of the CTestxPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CTestxPropPage : See TestxPpg.cpp.cpp for implementation.

class CTestxPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CTestxPropPage)
	DECLARE_OLECREATE_EX(CTestxPropPage)

// Constructor
public:
	CTestxPropPage();

// Dialog Data
	//{{AFX_DATA(CTestxPropPage)
	enum { IDD = IDD_PROPPAGE_TESTX };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CTestxPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTXPPG_H__7527A4F1_4105_4B41_8116_866B3ABE5D47__INCLUDED)
