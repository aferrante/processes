// TestxDlg.cpp : implementation file
//

#include "stdafx.h"
#include "testx.h"
#include "TestxCtl.h"
#include "TestxDlg.h"
#include "..\..\..\Common\IMG\BMP\CBmpUtil_MFC.h"  //bmp utilities for interface
#include "..\..\..\Common\LAN\Tcpip.h"  //network comm protocol
#include "..\..\..\Common\TXT\BufferUtil.h"  // buffer utilities 


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CTestxCtrl* g_pctrl;

#define DLG_MINSIZEX  150
#define DLG_MINSIZEY  100

/////////////////////////////////////////////////////////////////////////////
// CTestxDlg dialog


CTestxDlg::CTestxDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestxDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestxDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hcurArrow=LoadCursor(NULL,IDC_ARROW);
	m_hcurSize=LoadCursor(NULL, IDC_SIZENWSE);
	m_hcurEW=LoadCursor(NULL,IDC_SIZEWE);
	m_hcurNS=LoadCursor(NULL,IDC_SIZENS);
	m_bSizing=FALSE;
	m_bVis=FALSE;
}


void CTestxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestxDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTestxDlg, CDialog)
	//{{AFX_MSG_MAP(CTestxDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_GETMINMAXINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestxDlg message handlers

BOOL CTestxDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}

void CTestxDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	// prevents closing from escape button
  // CDialog::OnCancel();
}

void CTestxDlg::OnOK() 
{
	// TODO: Add extra validation here
	// intercepts enter key
	//	CDialog::OnOK();
}

void CTestxDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	if((bShow)&&(!m_bVis))
	{
		m_bVis=TRUE;
/*
		if(g_pctrl!=NULL)
		{
			CRect rc;
			GetWindowRect(&rc);
			g_pctrl->SetControlSize(rc.Width(), rc.Height());
		}
*/
	}
}

void CTestxDlg::OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI ) 
{
	lpMMI->ptMinTrackSize.x=DLG_MINSIZEX;
	lpMMI->ptMinTrackSize.y=DLG_MINSIZEY;
}

void CTestxDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	if(((nType==SIZE_MAXIMIZED)||(nType==SIZE_RESTORED))&&(m_sizeDlg!=CSize(0,0))&&(m_bVis))
	{
		// move components around
		CRect rc;
		int dx=m_sizeDlg.cx-cx;
		int dy=m_sizeDlg.cy-cy;

		CWnd* pWnd;
		pWnd = GetDlgItem(IDC_STATIC_PULL);
		if(pWnd)
		{
			pWnd->GetWindowRect(&rc);
			ScreenToClient(&rc);
			pWnd->SetWindowPos(NULL, rc.left-dx, rc.top-dy, 0,0, SWP_NOZORDER|SWP_NOSIZE);
		}

		// reset dlg size
		m_sizeDlg.cx=cx;
		m_sizeDlg.cy=cy;
	}
	
	if(g_pctrl!=NULL)
	{
		g_pctrl->SetControlSize(cx , cy);
	}
}

void CTestxDlg::OnMove(int x, int y) 
{
	// we want to disable the dialog from moving within the control
	//	CDialog::OnMove(x, y);
}

void CTestxDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	CRect rc;
	GetWindowRect(&rc);

	CString foo; foo.Format("dlg:%d, %d\n", rc.Width(), rc.Height() );
//	AfxMessageBox(foo);
	GetDlgItem(IDC_STATIC_X)->SetWindowText(foo);

	if(g_pctrl!=NULL)
	{
		if(g_pctrl->IsModified())
		{
			GetDlgItem(IDC_STATIC_INIT)->SetWindowText(g_pctrl->GetInitData());
		}
	}


	GetDlgItem(IDC_STATIC_PULL)->GetWindowRect(&rc);
	ScreenToClient(&rc);

	if(	(point.x>rc.left)&&(point.y>rc.top)	)
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurSize!=hCur) SetCursor(m_hcurSize);
	}
	else if (point.x>=rc.right)
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurEW!=hCur) SetCursor(m_hcurEW);
	}
	else if (point.y>=rc.bottom)
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurNS!=hCur) SetCursor(m_hcurNS);
	}
	else
	{
		HCURSOR hCur = GetCursor();
		if(m_hcurArrow!=hCur) SetCursor(m_hcurArrow);
	}

	if(m_bSizing)
	{
		GetWindowRect(&rc);
		CSize size(1,1);
		if(rc.Width()+((point.x-m_ptMouseDown.x)*m_sizeMouse.cx)<DLG_MINSIZEX) size.cx=0;
		if(rc.Height()+((point.y-m_ptMouseDown.y)*m_sizeMouse.cy)<DLG_MINSIZEY) size.cy=0;

		SetWindowPos(NULL,0,0,
			rc.Width()+((point.x-m_ptMouseDown.x)*m_sizeMouse.cx*size.cx), rc.Height()+((point.y-m_ptMouseDown.y)*m_sizeMouse.cy*size.cy),
			SWP_NOMOVE|SWP_NOZORDER);
		if(size.cx) m_ptMouseDown.x = point.x; // else leave it the old x
		if(size.cy) m_ptMouseDown.y = point.y; // else leave it the old y
//		m_ptMouseDown = point;
	}

	CDialog::OnMouseMove(nFlags, point);
}

void CTestxDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CRect rc;
	GetDlgItem(IDC_STATIC_PULL)->GetWindowRect(&rc);
	ScreenToClient(&rc);
	if(	(point.x>rc.left)&&(point.y>rc.top)	)
	{
		SetCapture();
		m_bSizing = TRUE;
		m_ptMouseDown = point;
		m_sizeMouse = CSize(1,1);
		HCURSOR hCur = GetCursor();
		if(m_hcurSize!=hCur) SetCursor(m_hcurSize);
	}
	else if (point.x>rc.right)
	{
		SetCapture();
		m_bSizing = TRUE;
		m_ptMouseDown = point;
		m_sizeMouse = CSize(1,0);
		HCURSOR hCur = GetCursor();
		if(m_hcurEW!=hCur) SetCursor(m_hcurEW);
	}
	else if (point.y>rc.bottom)
	{
		SetCapture();
		m_bSizing = TRUE;
		m_ptMouseDown = point;
		m_sizeMouse = CSize(0,1);
		HCURSOR hCur = GetCursor();
		if(m_hcurNS!=hCur) SetCursor(m_hcurNS);
	}
	CDialog::OnLButtonDown(nFlags, point);
}

void CTestxDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	m_bSizing=FALSE;
	ReleaseCapture();
	HCURSOR hCur = GetCursor();
	if(m_hcurArrow!=hCur) SetCursor(m_hcurArrow);
	CDialog::OnLButtonUp(nFlags, point);
}


BOOL CTestxDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CRect rc;
	GetWindowRect(&rc);
	m_sizeDlg.cx = rc.Width();
	m_sizeDlg.cy = rc.Height();

		// Load all bitmaps
	CBmpUtil bu;
	CBitmap bmp;
	HBITMAP hbmp;

  bmp.LoadBitmap(IDB_BITMAP_PULL);
	hbmp = bu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), bu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();

	// provide transparency to the corner pull
	if(hbmp!=NULL) ((CStatic*)GetDlgItem(IDC_STATIC_PULL))->SetBitmap(hbmp);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
