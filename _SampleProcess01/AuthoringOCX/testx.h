#if !defined(AFX_TESTX_H__D6D2AE4A_336B_488B_8FC1_6F16258F52BF__INCLUDED_)
#define AFX_TESTX_H__D6D2AE4A_336B_488B_8FC1_6F16258F52BF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// testx.h : main header file for TESTX.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTestxApp : See testx.cpp for implementation.

class CTestxApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTX_H__D6D2AE4A_336B_488B_8FC1_6F16258F52BF__INCLUDED)
