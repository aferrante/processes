// TestxCtl.cpp : Implementation of the CTestxCtrl ActiveX Control class.

#include "stdafx.h"
#include "testx.h"
#include "TestxDlg.h"
#include "TestxCtl.h"
//#include "TestxPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CTestxCtrl, COleControl)

CTestxDlg g_dlg;
CTestxCtrl* g_pctrl=NULL;


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CTestxCtrl, COleControl)
	//{{AFX_MSG_MAP(CTestxCtrl)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
//	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CTestxCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CTestxCtrl)
	DISP_PROPERTY_NOTIFY(CTestxCtrl, "Init", m_szInit, OnInitChanged, VT_BSTR)
	//}}AFX_DISPATCH_MAP
//	DISP_FUNCTION_ID(CTestxCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CTestxCtrl, COleControl)
	//{{AFX_EVENT_MAP(CTestxCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages
/*
// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CTestxCtrl, 1)
	PROPPAGEID(CTestxPropPage::guid)
END_PROPPAGEIDS(CTestxCtrl)

*/

/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CTestxCtrl, "TESTX.TestxCtrl.1",
	0xe5bb65ef, 0x36fc, 0x4ab7, 0xb2, 0x1, 0x4f, 0x74, 0xee, 0x7a, 0xe8, 0x71)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CTestxCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DTestx =
		{ 0xd5db7efd, 0xea2f, 0x46a5, { 0x9d, 0x31, 0x3d, 0x1b, 0xfd, 0x21, 0x28, 0xa1 } };
const IID BASED_CODE IID_DTestxEvents =
		{ 0x9c4db7df, 0x153, 0x48b0, { 0x9e, 0xf9, 0x2f, 0xce, 0x65, 0x4e, 0xd, 0x19 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwTestxOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CTestxCtrl, IDS_TESTX, _dwTestxOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl::CTestxCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CTestxCtrl

BOOL CTestxCtrl::CTestxCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_TESTX,
			IDB_TESTX,
			afxRegApartmentThreading,
			_dwTestxOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl::CTestxCtrl - Constructor

CTestxCtrl::CTestxCtrl()
{
	InitializeIIDs(&IID_DTestx, &IID_DTestxEvents);
	g_pctrl=this;
	m_bInit=TRUE;

	// TODO: Initialize your control's instance data here.
//5195748396 MK cell or corus ext. 3636 3640
//	SetInitialSize(GetSystemMetrics(SM_CXFULLSCREEN)*3/4, GetSystemMetrics(SM_CYFULLSCREEN)*3/4);
		SetInitialSize(640, 480); // has to be bigger than the dialog initially
}


/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl::~CTestxCtrl - Destructor

CTestxCtrl::~CTestxCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl::OnDraw - Drawing function

void CTestxCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
//	pdc->Ellipse(rcBounds);

	if(m_bInit) // just set the size the first time around
	{
		m_bInit=FALSE;
		CRect rc;
		g_dlg.GetWindowRect(&rc);
		SetControlSize(rc.Width(), rc.Height());
	}

	g_dlg.Invalidate();
}


/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl::DoPropExchange - Persistence support

void CTestxCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
  PX_String(pPX, _T("Init"), m_szInit, _T(""));
}

CString CTestxCtrl::GetInitData()
{
	return m_szInit;
}


/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl::OnResetState - Reset control to default state

void CTestxCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange
	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl::AboutBox - Display an "About" box to the user
/*
void CTestxCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_TESTX);
	dlgAbout.DoModal();
}
*/

/////////////////////////////////////////////////////////////////////////////
// CTestxCtrl message handlers

int CTestxCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;

	g_dlg.Create(this);
	g_dlg.ShowWindow(SW_SHOW); 
	return 0;
}

void CTestxCtrl::OnDestroy() 
{
	COleControl::OnDestroy();
	g_dlg.ShowWindow(SW_HIDE);
	g_dlg.DestroyWindow();
}


void CTestxCtrl::OnInitChanged() 
{
	// TODO: Add notification handler code
	SetModifiedFlag();
}
