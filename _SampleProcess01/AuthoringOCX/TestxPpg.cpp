// TestxPpg.cpp : Implementation of the CTestxPropPage property page class.

#include "stdafx.h"
#include "testx.h"
#include "TestxPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CTestxPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CTestxPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CTestxPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CTestxPropPage, "TESTX.TestxPropPage.1",
	0x37c733fb, 0x39da, 0x427d, 0x8b, 0x7a, 0xba, 0x50, 0x8b, 0x6f, 0x79, 0x57)


/////////////////////////////////////////////////////////////////////////////
// CTestxPropPage::CTestxPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CTestxPropPage

BOOL CTestxPropPage::CTestxPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_TESTX_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CTestxPropPage::CTestxPropPage - Constructor

CTestxPropPage::CTestxPropPage() :
	COlePropertyPage(IDD, IDS_TESTX_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CTestxPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CTestxPropPage::DoDataExchange - Moves data between page and properties

void CTestxPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CTestxPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CTestxPropPage message handlers
