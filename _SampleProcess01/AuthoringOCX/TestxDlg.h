#if !defined(AFX_TESTXDLG_H__79FF4567_D47B_4BA2_985F_6E9C070BF227__INCLUDED_)
#define AFX_TESTXDLG_H__79FF4567_D47B_4BA2_985F_6E9C070BF227__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TestxDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestxDlg dialog

class CTestxDlg : public CDialog
{
// Construction
public:
	CTestxDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTestxDlg)
	enum { IDD = IDD_DIALOG_MAIN };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

public:
	CPoint m_ptMouseDown;
	HCURSOR m_hcurArrow;
	HCURSOR	m_hcurSize;
	HCURSOR	m_hcurNS;
	HCURSOR	m_hcurEW;
	BOOL m_bSizing;
	CSize m_sizeDlg;
	CSize m_sizeMouse;
	BOOL m_bVis;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestxDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTestxDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI );
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTXDLG_H__79FF4567_D47B_4BA2_985F_6E9C070BF227__INCLUDED_)
