//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by testx.rc
//
#define IDS_TESTX                       1
#define IDD_ABOUTBOX_TESTX              1
#define IDB_TESTX                       1
#define IDI_ABOUTDLL                    1
#define IDS_TESTX_PPG                   2
#define IDS_TESTX_PPG_CAPTION           200
#define IDD_PROPPAGE_TESTX              200
#define IDD_DIALOG_MAIN                 201
#define IDC_STATIC_PULL                 201
#define IDB_BITMAP_PULL                 202
#define IDC_STATIC_X                    202
#define IDI_ICON1                       203
#define IDC_STATIC_INIT                 203

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        204
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         204
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
